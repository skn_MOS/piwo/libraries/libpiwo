#!/bin/sh
mkdir -p build-static-windows
cd build-static-windows
cmake \
	-GNinja \
	-DCMAKE_BUILD_TYPE=Release \
	-DLIBSTATIC=YES \
	-DPIWO_ENABLE_LAYERS='all' \
	-DCMAKE_TOOLCHAIN_FILE=cmake/cross-compile/linux_cc_windows.cmake \
	..
ninja

