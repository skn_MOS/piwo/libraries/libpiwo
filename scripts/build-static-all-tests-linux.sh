#!/bin/sh
mkdir -p build-static-all-tests-linux
cd build-static-all-tests-linux
cmake \
	-GNinja \
	-DCMAKE_BUILD_TYPE=Release \
	-DBUILD_TESTS=YES \
	-DLIBSTATIC=YES \
	-DPIWO_ENABLE_LAYERS='all' \
	..
ninja

