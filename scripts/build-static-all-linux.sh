#!/bin/sh
mkdir -p build-static-linux
cd build-static-linux
cmake \
	-GNinja \
	-DCMAKE_BUILD_TYPE=Release \
	-DLIBSTATIC=YES \
	-DPIWO_ENABLE_LAYERS='all' \
	-DINSTALL_TOOLS=ON \
	..
ninja

