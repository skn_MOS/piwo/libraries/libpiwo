if(NOT DEFINED piwo_lib_deps_list)
  set(piwo_lib_deps_list "")
endif()

macro(piwo_add_dep modname depname)
  if(NOT "${${depname}_LIBRARIES}")
    set(${depname}_LIBRARIES ${depname})
  endif()

  message(STATUS "Adding ${${depname}_LIBRARIES} dependencies to module ${modname} [triggered by ${depname}]")

  list(APPEND piwo_lib_deps_list_${modname} ${${depname}_LIBRARIES})
endmacro()

macro(piwo_export_deps modname)
  set(piwo_lib_deps_${modname} "")
  list(JOIN piwo_lib_deps_list_${modname} " -l" piwo_lib_deps_${modname})
  list(LENGTH piwo_lib_deps_list_${modname} deps_count)
  if ("${deps_count}" GREATER "0")
    string(PREPEND piwo_lib_deps_${modname} "-l")
  endif()
  message(STATUS "Exporting dependencies [${piwo_lib_deps_${modname}}] into piwo_lib_deps_${modname}")
endmacro()
