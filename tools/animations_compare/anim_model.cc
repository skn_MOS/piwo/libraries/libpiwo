#include "anim_model.h"

int
anim_model::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid() || !_anim_data)
    return 0;

  return _anim_data->get_height();
}

int
anim_model::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid() || !_anim_data)
    return 0;

  return _anim_data->get_width();
}

QVariant
anim_model::data(const QModelIndex& index, [[maybe_unused]] int role) const
{
  if (!_anim_data)
    return QVariant();

  piwo::color col = _anim_data->get_current_color(index.column(), index.row());
  return QColor{ col.r, col.g, col.b };
}

bool
anim_model::setData([[maybe_unused]] const QModelIndex& index,
                    [[maybe_unused]] const QVariant&    value,
                    [[maybe_unused]] int                role)
{
  // Currently not needed
  return false;
}

Qt::ItemFlags
anim_model::flags(const QModelIndex& index) const
{
  if (!index.isValid())
    return Qt::NoItemFlags;

  return Qt::ItemIsEditable;
}

void
anim_model::new_anim(const QString& anim_path)
{
  if (!_anim_data)
    _anim_data = std::make_unique<anim_display>();

  this->beginResetModel();
  _anim_data->reset_index();
  _anim_data->build_from_file(anim_path.toStdString().c_str());
  this->endResetModel();
}

void
anim_model::next_frame()
{
  if (_anim_data->is_anim_finished())
  {
    emit this->animationFinished();
    return;
  }
  this->beginResetModel();
  _anim_data->increase_index();
  this->endResetModel();
}

void
anim_model::prev_frame()
{
  this->beginResetModel();
  _anim_data->decrease_index();
  this->endResetModel();
}

void
anim_model::restart()
{
  this->beginResetModel();
  _anim_data->reset_index();
  this->endResetModel();
}
