#pragma once
#include "anim_display.h"
#include "color_palette.h"

class palette_display
{
public:
  piwo::color
  get_current_color(anim_display& anim, size_t column, size_t row) const
  {
    return _palette.get_rounded_color(anim.get_current_color(column, row))
      .value();
  }

  void
  clear()
  {
    _palette.clear();
  }

  void
  extend(const anim_display& anim, size_t treshold)
  {
    _palette.extend(anim.get_anim(), treshold);
  }

private:
  piwo::color_palette _palette;
};
