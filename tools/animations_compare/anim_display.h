#pragma once
#include "animation.h"
#include <string>

class anim_display
{
public:
  size_t
  get_width() const
  {
    return _anim.width();
  }

  size_t
  get_height() const
  {
    return _anim.height();
  }

  size_t
  get_index() const
  {
    return _index;
  }

  const piwo::animation&
  get_anim() const
  {
    return _anim;
  }

  piwo::color
  get_current_color(size_t column, size_t row)
  {
    return _anim[_index].at_(column, _anim.height() - row - 1);
  }

  void
  increase_index()
  {
    ++_index;
  }

  void
  decrease_index()
  {
    if (_index > 0)
      --_index;
  }

  void
  reset_index()
  {
    _index = 0;
  }

  void
  build_from_file(const char* filename)
  {
    _anim.build_from_file(filename);
  }

  bool
  is_anim_finished() const
  {
    return _index >= _anim.frame_count() - 1;
  }

private:
  piwo::animation _anim;
  size_t          _index = 0;
};
