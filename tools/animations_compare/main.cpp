#include "anim_model.h"
#include "palette_model.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int
main(int argc, char* argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

  QGuiApplication app(argc, argv);

  app.setOrganizationName("MOS");
  app.setOrganizationDomain("MOS");

  qmlRegisterType<anim_model>("AnimModel", 0, 1, "AnimModel");
  qmlRegisterType<palette_model>("PaletteModel", 0, 1, "PaletteModel");

  QQmlApplicationEngine engine;
  const QUrl            url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(
    &engine,
    &QQmlApplicationEngine::objectCreated,
    &app,
    [url](QObject* obj, const QUrl& objUrl)
    {
      if (!obj && url == objUrl)
        QCoreApplication::exit(-1);
    },
    Qt::QueuedConnection);
  engine.load(url);

  return app.exec();
}
