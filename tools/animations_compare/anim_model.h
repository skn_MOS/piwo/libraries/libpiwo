#pragma once

#include "anim_display.h"
#include "color_palette.h"
#include <QAbstractTableModel>
#include <QColor>
#include <memory>
#include <qqml.h>

class anim_model : public QAbstractTableModel
{
  Q_OBJECT

public:
  int
  rowCount(const QModelIndex& parent = QModelIndex()) const override;

  int
  columnCount(const QModelIndex& parent = QModelIndex()) const override;

  QVariant
  data(const QModelIndex& index, int role) const override;

  bool
  setData(const QModelIndex& index,
          const QVariant&    value,
          int                role = Qt::EditRole) override;

  Qt::ItemFlags
  flags(const QModelIndex& index) const override;

  QHash<int, QByteArray>
  roleNames() const override
  {
    return { { Qt::DisplayRole, "animation" } };
  }

public slots:
  void
  new_anim(const QString& anim_path);

  void
  next_frame();

  void
  prev_frame();

  void
  restart();

  int
  frame_number()
  {
    return _anim_data->get_index();
  }

signals:
  void
  animationFinished();

protected:
  std::unique_ptr<anim_display> _anim_data;
};
