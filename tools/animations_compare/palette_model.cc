#include "palette_model.h"

int
palette_model::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid() || !_anim_data || !_palette_data)
    return 0;

  return _anim_data->get_height();
}

int
palette_model::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid() || !_anim_data || !_palette_data)
    return 0;

  return _anim_data->get_width();
}

QVariant
palette_model::data(const QModelIndex& index, [[maybe_unused]] int role) const
{
  if (!_anim_data || !_palette_data)
    return QVariant();

  piwo::color col;
  col =
    _palette_data->get_current_color(*_anim_data, index.column(), index.row());

  return QColor{ col.r, col.g, col.b };
}

void
palette_model::new_palette(const QString& anim_path, int treshold)
{
  if (!_anim_data || !_palette_data)
  {
    _anim_data    = std::make_unique<anim_display>();
    _palette_data = std::make_unique<palette_display>();
  }

  this->beginResetModel();
  _anim_data->reset_index();
  _anim_data->build_from_file(anim_path.toStdString().c_str());
  _palette_data->clear();
  _palette_data->extend(*_anim_data, treshold);
  this->endResetModel();
}

void
palette_model::change_treshold(int treshold)
{
  this->beginResetModel();
  _palette_data->clear();
  _palette_data->extend(*_anim_data, treshold);
  this->endResetModel();
}
