#pragma once

#include "anim_model.h"
#include "animation.h"
#include "color_palette.h"
#include "palette_display.h"
#include <QColor>
#include <memory>
#include <qqml.h>

class palette_model : public anim_model
{
  Q_OBJECT

public:
  int
  rowCount(const QModelIndex& parent = QModelIndex()) const override;

  int
  columnCount(const QModelIndex& parent = QModelIndex()) const override;

  QVariant
  data(const QModelIndex& index, [[maybe_unused]] int role) const override;

  QHash<int, QByteArray>
  roleNames() const override
  {
    return { { Qt::DisplayRole, "palette" } };
  }

public slots:
  void
  new_palette(const QString& anim_path, int treshold);

  void
  change_treshold(int treshold);

private:
  std::unique_ptr<palette_display> _palette_data;
};
