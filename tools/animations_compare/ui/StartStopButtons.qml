import QtQuick.Controls 2.12
import QtQuick 2.12

Item {
    id: it
    anchors.horizontalCenter: palette_view.horizontalCenter
    anchors.top: palette_view.bottom
    anchors.topMargin: 50
    height: row_interval.height + row_bottom.height + 10
    visible: false

    property alias is_visible: it.visible

    Row {
        id: row_interval
        anchors.horizontalCenter: it.horizontalCenter
        spacing: 10

        Label {
            y: 5
            text: "Interval in milliseconds"
        }

        TextField {
            id: text_mseconds
        }
    }

    Row {
        id: row_bottom
        spacing: 20
        anchors.horizontalCenter: it.horizontalCenter
        anchors.top: row_interval.bottom
        anchors.topMargin: 10

        Button {
            id: stop_button
            text: "Stop"
            onClicked: {
                timer.running = false;
            }
        }

        Button {
            id: start_button
            text: "Start"
            onClicked: {
                var num = parseInt(text_mseconds.text, 10);
                if(!isNaN(num) && num > 0) {
                    timer.interval = num;
                    timer.running = true;
                    timer.repeat = true;
                }
            }
        }
    }
}
