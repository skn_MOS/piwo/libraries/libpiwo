import QtQuick.Controls 2.12
import QtQuick 2.12

Item {
    id: item
    anchors.horizontalCenter: parent.horizontalCenter
    property alias visible_arrows: item.visible
    anchors.top: anim_view.bottom
    visible: false

    Button {
        id: button_next_frame
        anchors.left: item.horizontalCenter
        anchors.leftMargin: 10
        anchors.top: item.top
        anchors.topMargin: 50
        width: 50
        height: 50
        text: ">"
        onClicked: {
            anim_model.next_frame();
            palette_model.next_frame();
        }
    }

    Button {
        id: button_prev_frame
        anchors.right: item.horizontalCenter
        anchors.rightMargin: 10
        anchors.top: item.top
        anchors.topMargin: 50
        width: 50
        height: 50
        text: "<"
        onClicked: {
            anim_model.prev_frame();
            palette_model.prev_frame();
        }
    }
}
