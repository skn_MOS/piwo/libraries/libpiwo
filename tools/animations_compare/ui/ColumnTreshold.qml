import QtQuick.Controls 2.12
import QtQuick 2.12

Column {
    x: 50
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 100
    anchors.left: anim_view.left
    spacing: 10
    visible: false
    readonly property int sliderValue: treshold_slider.value

    Label {
        text: "Treshold"
    }

    Slider {
        id: treshold_slider
        width: 255
        from: 1; to: 255; stepSize: 1
        value: 1
        onMoved: {
            palette_model.change_treshold(value);
        }
    }

    TextField {
        id: text_treshold
        text: treshold_slider.value
        enabled: false
    }
}
