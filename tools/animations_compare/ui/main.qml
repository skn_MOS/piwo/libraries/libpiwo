import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQuick.Dialogs 1.3
import AnimModel 0.1
import PaletteModel 0.1

ApplicationWindow {
    width: 1500
    height: 600
    color: "gray"
    visible: true
    title: "Animations Comparison"
    property var tableViewWidth: 600
    property var tableViewHeight: 300

    MenuBar {
        Menu {
            title: "File"
            Action {
                text: "Open animation"
                onTriggered: fileDialog.open();
            }
        }
    }

    TableView {
        id: anim_view
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 50
        width: tableViewWidth
        height: tableViewHeight
        columnSpacing: 1
        rowSpacing: 1

        model: AnimModel {
            id: anim_model
            onAnimationFinished: timer.running = false;
            onModelReset: {
                buttons_arrows.visible_arrows = true;
                frame_number.text = "Frame number: " + anim_model.frame_number();
            }
        }

        delegate: Rectangle {
            id: rect
            implicitWidth: anim_view.width / anim_model.columnCount() - anim_view.columnSpacing
            implicitHeight: anim_view.height / anim_model.rowCount() - anim_view.rowSpacing
            color: animation
        }
    }

    TableView {
        id: palette_view
        anchors.left: parent.horizontalCenter
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 50
        width: tableViewWidth
        height: tableViewHeight
        columnSpacing: 1
        rowSpacing: 1

        model: PaletteModel {
            id: palette_model
            onAnimationFinished: timer.running = false;
            onModelReset: {
                column_treshold.visible = true;
                start_stop_buttons.is_visible = true;
                restart_button.visible = true;
            }
        }

        delegate: Rectangle {
            implicitWidth: palette_view.width / palette_model.columnCount() - palette_view.columnSpacing
            implicitHeight: palette_view.height / palette_model.rowCount() - palette_view.rowSpacing
            color: palette
        }
    }

    Label {
        id: frame_number
        anchors.left: anim_view.left
        anchors.top: anim_view.bottom
        anchors.topMargin: 10
    }

    ButtonsArrows {
        id: buttons_arrows
    }

    StartStopButtons {
        id: start_stop_buttons
    }

    Button {
        id: restart_button
        anchors.horizontalCenter: start_stop_buttons.horizontalCenter
        anchors.top: start_stop_buttons.bottom
        anchors.topMargin: 10
        text: "Restart"
        visible: false

        onClicked: {
            anim_model.restart();
            palette_model.restart();
        }
    }

    ColumnTreshold {
        id: column_treshold
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        nameFilters: [ "Chose animation file (*.piwo7)" ]
        onAccepted: {
            var path = fileDialog.fileUrl.toString();
            path = path.replace(/^(file:\/{2})/,"");
            var cleanPath = decodeURIComponent(path);

            anim_model.new_anim(cleanPath);
            palette_model.new_palette(cleanPath, parseInt(column_treshold.sliderValue));
        }
        Component.onCompleted: visible = false
    }

    Timer {
        id: timer
        onTriggered: {
            anim_model.next_frame();
            palette_model.next_frame();
        }
    }
}
