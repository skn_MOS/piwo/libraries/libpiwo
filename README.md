# LIBPIWO
### Library provides
  - parsing animation files in PIWO7 format
  - saving animations as PIWO7 files
  - structures and classes for processing and manipulating PIWO7 animations
  - serialization/deserialization of packets used in PIWO communication protocol

### Library overview
libpiwo is crossplatform (windows and linux) library for handling PIWO7 file
format and PIWO communication protocol.
One of the main goals of this library is to provide reference implementation
for parsing and saving PIWO animation files. This way all components that uses
this format in any way doesn't have to bother with incompatibilities.
New version may introduce breaking changes and fixes so It is strongly advised
for each component to specify which version it requires to run.

### Installation
Below you can find description for building and installing this project using
__GCC__ and __GNU Make__, however You can use whatever compiler and build
system you want, as long as cmake supports it or you know what you are doing.

#### Dependencies

Before building binaries manually one must first provide necessary dependecies.
List of required componets are listed below. To install then just follow their installation steps.

1. mipc - MOS Interprocess Communication Library (https://gitlab.com/skn_mos/libraries/mipc)

You should follow installation instruction from the above projects.

#### Windows

1. Install __MinGW g++__ compiler (must support C++17)
2. Install __GNU make__
3. Install __CMake__
4. Add __MinGW__'s bin folder to `PATH` variable (eg. C:\MinGW\bin)
5. Add __GNU make__'s path to `PATH` variable (eg. C:\Program Files\GnuWin32\bin)
6. Add __CMake__'s path to `PATH` variable (eg. C:\Program Files\CMake\bin)

#### Linux

1. Install __g++__ (g++-8 or newer is preferred, also must support C++17)
2. Install __make__
3. Install __CMake__

### Building

#### Windows

1. Open terminal (eg. cmd.exe)
2. Go to location of main directory of this repo
3. Create new directory called __build__ with `mkdir build`
4. Enter new directory with `cd build`
5. Type in `cmake -G "Unix Makefiles" -DPIWO_ENABLE_LAYERS=all ..`
6. Type in `make install` (You may need to run cmd.exe with Administrator priviliges)

#### Linux

1. Open terminal
2. Go to location of main directory of this repo
3. Create new directory called __build__ with `mkdir build`
4. Enter new directory with `cd build`
5. Type in `cmake -DPIWO_ENABLE_LAYERS=all ..`
6. Type in `make install` (You may need to run this step as root, for example via sudo)
