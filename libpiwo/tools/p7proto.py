#!/usr/bin/env python3
import crc8

def generate_test_packets(uid : str, uid_base = 10) -> tuple:
    uid = [int(c, uid_base) for c in uid.split(':')]

    red = [19,17,0]
    red.extend(uid)
    red.extend([255, 0, 0])
    red.append(ord(crc8.crc8(bytearray(red)).digest()))

    green = [19,17,0]
    green.extend(uid)
    green.extend([0, 255, 0])
    green.append(ord(crc8.crc8(bytearray(green)).digest()))

    blue = [19,17,0]
    blue.extend(uid)
    blue.extend([0, 0, 255])
    blue.append(ord(crc8.crc8(bytearray(blue)).digest()))

    return red, green, blue
