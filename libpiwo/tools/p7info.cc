#include "animation.h"

#include <string>
#include <vector>
#include <iostream>

#include <getopt.h>

struct
options_t
{
  // Number of mutually exclusive options with "cat"
  static constexpr const size_t EXOPT_CAT_COUNT = 3;
  static constexpr const char* flag_strs[EXOPT_CAT_COUNT] =
  {
    "fc",
    "res",
    "dump"
  };

  union
  {
    struct
    {
      uint8_t fc             : 1 = 0;
      uint8_t res            : 1 = 0;
      uint8_t dump           : 1 = 0;
      uint8_t cat            : 1 = 0;
      uint8_t help           : 1 = 0;
    };

    uint8_t as_uint8_t;
  };
} static options;

void
show_menu_options()
{
  constexpr const char* menu =
    "  --help               Show this message\n"
    "  --fc                 Show frame count\n"
    "  --res                Show animation resoloution\n"
    "  --dump               Dump frame to stdout (in piwo7 format)\n"
    "  --cat                Concatenate several animations into one\n";

  printf("%s", menu);
}

void
collect_options(int argc, char**argv)
{
  struct
  option long_options[] =
  {
    {"help", no_argument, NULL, 'h'},
    {"fc"  , no_argument, NULL, 'f'},
    {"res" , no_argument, NULL, 'r'},
    {"dump", no_argument, NULL, 'd'},
    {"cat" , no_argument, NULL, 'c'},
    {0, 0, 0, 0}
  };

  for(int i = 0; i < argc; i++)
  {
    auto input = getopt_long(argc, argv, "hfrdc", long_options, nullptr);

    switch(input)
    {
      case -1:
        return;

      case 'h':
        options.help = 1;
        break;

      case 'f':
        options.fc = 1;
        break;

      case 'r':
         options.res = 1;
         break;

      case 'd':
        options.dump = 1;
        break;

      case 'c':
        options.cat = 1;
        break;
    }
  }
}

void
print_bad_options()
{
  static constexpr const char* warn_msg[] =
  {
    "",
    "Ignored option: %s (not available in cat mode)\n"
  };

  for(uint8_t i = 0; i < options_t::EXOPT_CAT_COUNT; i++)
  {
    const uint8_t current_flag = 1 << i;
    const bool is_exlusive_opt_set = current_flag & options.as_uint8_t;

    const auto format_str = warn_msg[is_exlusive_opt_set];
    const auto flag_str = options_t::flag_strs[i];
    fprintf(stderr, format_str, flag_str);
  }
}

int
cat(int argc, char** argv)
{

  const auto all_but_cat = ~(options_t { .cat = 1 }.as_uint8_t);

  if(all_but_cat & options.as_uint8_t)
    print_bad_options();

  std::vector<piwo::animation> anims;
  bool                         res_known = false;
  size_t                       width, height, totalfc = 0;

  for (; optind < argc; optind++)
  {
    const char* filename = argv[optind];

    piwo::animation anim;
    auto            status = anim.build_from_file(filename);

    switch (status.error)
    {
      case piwo::animation::error_e::ok:
        break;
      case piwo::animation::error_e::cant_open_file:
        fprintf(stderr, "Cant open file %s\n", filename);
        return 2;
      case piwo::animation::error_e::bad_signature:
         fprintf(stderr, "File %s has incorrect signature, line number: %lu\n", filename, status.line_number);
        return 3;
      case piwo::animation::error_e::bad_parse:
        fprintf(stderr, "File %s format incorrect, line number: %lu\n", filename, status.line_number);
        return 4;
      default:
        fprintf(stderr, "%s: Unexpected error\n", filename);
        return 5;
    }

    if (res_known)
    {
      if (anim.width() != width || anim.height() != height)
      {

        fprintf(stderr,"Cant concatenate %s - different resolution then "
                     "previous animations.\n",
                     filename);

        fprintf(stderr, "Expected resolution: %lu x %lu, but %s has: %lu x %lu\n",
                     width,
                     height,
                     filename,
                     anim.width(),
                     anim.height());

        return 6;
      }
    }

    res_known = true;
    width     = anim.width();
    height    = anim.height();

    totalfc += anim.frame_count();
    anims.emplace_back(std::move(anim));
  }

  auto out     = piwo::animation::alloc(width, height, totalfc);
  auto out_beg = out.begin(), out_end = out.end();

  for (auto& anim : anims)
  {
    for (auto& frame : anim)
    {
      if (out_beg == out_end)
      {
        fprintf(stderr, "Logic error: Unexpected count of frames!");
        return 7;
      }

      out_beg->copy_from_(frame);
      ++out_beg;
    }
  }

  std::cout << out;
  return 0;
}

int
main(int argc, char** argv)
{
  collect_options(argc, argv);

  if (argc < 3 || options.help)
  {
    show_menu_options();
    return 0;
  }

  if (options.cat)
  {
    return cat(argc, argv);
  }

  const int non_option_arg_count = argc - optind;
  const bool multiple_files = non_option_arg_count > 1;

  for (; optind < argc; optind++)
  {
    const char* filename = argv[optind];

    piwo::animation anim;

    auto status = anim.build_from_file(filename);

    switch (status.error)
    {
      case piwo::animation::error_e::ok:
        break;
      case piwo::animation::error_e::cant_open_file:
        fprintf(stderr, "Cant open file %s\n", filename);
        return 2;
      case piwo::animation::error_e::bad_signature:
       fprintf(stderr, "File %s has incorrect signature, line number: %lu\n", filename, status.line_number);
        return 3;
      case piwo::animation::error_e::bad_parse:
        fprintf(stderr, "File %s format incorrect, line number: %lu\n", filename, status.line_number);
        return 4;
      default:
        fprintf(stderr, "Unexpected error\n");
        return 5;
    }

    if (multiple_files)
      printf("%s:\n", filename);

    if (options.fc)
      printf("Frame count: %zu \n", anim.frame_count());

    if (options.res)
      printf("Resolution: %zu x %zu \n", anim.width(), anim.height());

    if (options.dump)
      std::cout << anim;
  }
  return 0;
}
