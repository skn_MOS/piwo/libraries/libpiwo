#pragma once
#include "frames_intersection.h"

namespace piwo
{
void
frame_assign_op(piwo::frames_intersection fi);

void
frame_add_op(piwo::frames_intersection fi);

void
frame_or_op(piwo::frames_intersection fi);

void
frame_sub_op(piwo::frames_intersection fi);

void
frame_mul_op(piwo::frames_intersection fi);

void
frame_assign_op(piwo::frames_intersection_unsafe fi);

void
frame_add_op(piwo::frames_intersection_unsafe fi);

void
frame_or_op(piwo::frames_intersection_unsafe fi);

void
frame_sub_op(piwo::frames_intersection_unsafe fi);

void
frame_mul_op(piwo::frames_intersection_unsafe fi);

} // namespace piwo
