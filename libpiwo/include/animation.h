#pragma once

#include "color.h"
#include "frame.h"
#include "frame_iterator.h"

#include <memory>
#include <mipc/file.h>
#include <ostream>
#include <string_view>

namespace piwo
{

class animation
{
public:
  static constexpr std::string_view piwo7_signature = "PIWO_7_FILE";

  enum class error_e : int
  {
    ok = 0,
    cant_open_file,
    bad_signature,
    bad_parse,
    bad_option,
    bad_size,
  };

  struct parser_context
  {
    const char* f_pos;
    const char* f_end;

    size_t lines = 1;
   };

  struct error_t
  {
    error_e error;
    size_t line_number;

    error_t() = default;

    error_t(error_e e)
      : error(e)
    {
    }

    error_t(error_e e, size_t number)
      : error(e), line_number(number)
      {
      }

    operator bool() const noexcept { return error == error_e::ok; }

    bool
    operator==(error_e err) const noexcept
    {
      return this->error == err;
    }

    bool
    operator!=(error_e err) const noexcept
    {
      return !(*this == err);
    }
  };

  struct parse_option
  {
    enum class translate_type
    {
      // Dont translte
      raw,

      // Used to convert parsed animation on the fly to contain
      // frames with only 50ms.
      // If current frame has longer delay than 50ms it is inserted
      // multiple times to fill whole time space.
      // The way of handling situation when frame delay is not divisible
      // by 50 ms is described below and depends on flag passed.
      //
      // "*_exact" returns bad_parse it that situation
      ms50_exact,

      // "*_round_down" throws away reminder of division (anim is
      // shorter)
      ms50_round_down,

      // "*_round_up" adds extra frame if reminder of division is not
      // zero
      ms50_round_up,
    } translate;

    parse_option()
      : translate(translate_type::raw)
    {
    }
  };

  using iterator = frame_iterator_t;

  animation()
    : _width(0)
    , _height(0)
    , _frame_count(0)
    , _total_time(0)
  {
  }

  animation(size_t w, size_t h)
    : _width(w)
    , _height(h)
    , _frame_count(0)
    , _total_time(0)
  {
  }

  animation(const animation& other) noexcept
    : _width(other._width)
    , _height(other._height)
    , _frame_count(other._frame_count)
    , _total_time(other._total_time)
    , _frames(std::make_unique<char[]>(other.buffer_size()))
  {
    auto others_data = other._frames.get();
    auto this_data   = this->_frames.get();

    std::copy_n(others_data, other.buffer_size(), this_data);
  }

  animation(animation&& other) noexcept
    : _width(other._width)
    , _height(other._height)
    , _frame_count(other._frame_count)
    , _total_time(other._total_time)
    , _frames(std::move(other._frames))
  {
    other._frame_count = 0;
    other._total_time  = 0;
  }

  animation&
  operator=(const animation& other)
  {
    this->_width       = other._width;
    this->_height      = other._height;
    this->_frame_count = other._frame_count;
    this->_total_time  = other._total_time;
    this->_frames      = std::make_unique<char[]>(other.buffer_size());

    auto others_data = other._frames.get();
    auto this_data   = this->_frames.get();

    std::copy_n(others_data, other.buffer_size(), this_data);

    return *this;
  }

  animation&
  operator=(animation&& other)
  {
    this->_width       = other._width;
    this->_height      = other._height;
    this->_frame_count = other._frame_count;
    this->_total_time  = other._total_time;
    this->_frames      = std::move(other._frames);

    other._frame_count = 0;
    other._total_time  = 0;

    return *this;
  }

  constexpr size_t
  frame_offset() const noexcept
  {
    size_t base_size   = sizeof(frame) - sizeof(frame::colors);
    size_t colors_size = this->_width * this->_height * sizeof(color);
    size_t offset      = base_size + colors_size;

    return offset;
  }

  iterator
  begin() const noexcept
  {
    auto     frame_ptr = reinterpret_cast<frame*>(this->_frames.get());
    iterator it(frame_ptr, this->frame_offset());
    return it;
  }

  iterator
  end() const noexcept
  {
    auto frame_ptr_ = reinterpret_cast<char*>(this->_frames.get());
    frame_ptr_ += this->frame_offset() * this->_frame_count;

    auto     frame_ptr = reinterpret_cast<frame*>(frame_ptr_);
    iterator it(frame_ptr, this->frame_offset());

    return it;
  }

  frame&
  operator[](size_t index) noexcept
  {
    auto frame_ptr = reinterpret_cast<char*>(this->_frames.get());
    frame_ptr += index * this->frame_offset();
    return *reinterpret_cast<frame*>(frame_ptr);
  }

  auto
  width() const noexcept
  {
    return this->_width;
  }

  auto
  height() const noexcept
  {
    return this->_height;
  }

  auto
  frame_count() const noexcept
  {
    return this->_frame_count;
  }

  auto
  total_time() const noexcept
  {
    return this->_total_time;
  }

  char*
  data() const noexcept
  {
    return this->_frames.get();
  }

  size_t
  buffer_size() const noexcept
  {
    return this->_frame_count * this->frame_offset();
  }

  error_t
  build_from_file(const char* filename, parse_option op = {}) noexcept;

  error_t
  build_from_file(mipc::finbuf& file, parse_option op = {}) noexcept
  {
    parser_context parser;
    parser.f_pos = file.data();
    parser.f_end = file.end();

    return build_from_file_(parser, op);
  }

  static animation
  alloc(size_t width, size_t height, size_t frame_count)
  {
    piwo::animation ret(width, height);
    ret._frame_count = frame_count;
    ret._total_time  = 0;
    ret._frames      = std::make_unique<char[]>(ret.buffer_size());

    return ret;
  }

  friend std::ostream&
  operator<<(std::ostream& os, const animation& anim) noexcept;

private:
  error_t
  build_from_file_(parser_context& parser, parse_option op = {}) noexcept;

private:
  size_t                  _width, _height, _frame_count, _total_time;
  std::unique_ptr<char[]> _frames;
};

} // namespace piwo
