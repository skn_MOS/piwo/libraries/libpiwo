#pragma once

#include "frame.h"

#include <fstream>

namespace piwo
{
struct frame_iterator_t;
}

std::ostream&
operator<<(std::ostream& os, const piwo::frame& f);

namespace piwo
{
struct frame_iterator_t
{
  frame_iterator_t(frame* d, size_t offset)
    : _data(d)
    , _offset(offset)
  {
  }

  frame_iterator_t&
  operator+(size_t inc)
  {
    auto data_ = reinterpret_cast<char*>(this->_data);
    data_ += inc * this->_offset;
    this->_data = reinterpret_cast<frame*>(data_);
    return *this;
  }

  frame_iterator_t&
  operator++()
  {
    auto data_ = reinterpret_cast<char*>(this->_data);
    data_ += this->_offset;
    this->_data = reinterpret_cast<frame*>(data_);
    return *this;
  }

  frame_iterator_t
  operator++(int)
  {
    auto retval = *this;
    auto data_  = reinterpret_cast<char*>(this->_data);
    data_ += this->_offset;
    this->_data = reinterpret_cast<frame*>(data_);
    return retval;
  }

  frame_iterator_t&
  operator-(size_t dec)
  {
    auto data_ = reinterpret_cast<char*>(this->_data);
    data_ += dec * this->_offset;
    this->_data = reinterpret_cast<frame*>(data_);
    return *this;
  }

  frame_iterator_t&
  operator--()
  {
    auto data_ = reinterpret_cast<char*>(this->_data);
    data_ -= this->_offset;
    this->_data = reinterpret_cast<frame*>(data_);
    return *this;
  }

  frame_iterator_t
  operator--(int)
  {
    auto retval = *this;
    auto data_  = reinterpret_cast<char*>(this->_data);
    data_ -= this->_offset;
    this->_data = reinterpret_cast<frame*>(data_);
    return retval;
  }

  bool
  operator==(const frame_iterator_t& other) const
  {
    return this->_data == other._data;
  }

  bool
  operator!=(const frame_iterator_t& other) const
  {
    return this->_data != other._data;
  }

  frame&
  operator*() const noexcept
  {
    return *this->_data;
  }

  frame*
  operator->() const noexcept
  {
    return this->_data;
  }

  frame*
  get() const noexcept
  {
    return this->_data;
  };

  friend std::ostream& ::operator<<(std::ostream& os, const piwo::frame& f);

private:
  frame*       _data;
  const size_t _offset;
};
} // namespace piwo
