#pragma once
#include "alloc.h"
#include "color.h"
#include "mark.h"

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <memory>

namespace piwo
{

// CAUTION !!! This class should be used only as a VIEW OVER HEAP MEMORY
// This variable should never appear on the stack. The only valid form of
// existance of this object on stack is in form of pointer or reference.
struct __attribute__((packed, aligned(8))) frame
{
  frame()                   = delete;
  frame(frame&& other)      = delete;
  frame(const frame& other) = delete;

  frame&
  operator=(const frame& other) = delete;

  frame&
  operator=(frame&& other) = delete;

  // Assures that the destination frame has enough space
  // to copy whole color data from source
  UNSAFE void
  copy_from_(const frame& src)
  {
    this->width  = src.width;
    this->height = src.height;
    this->delay  = src.delay;

    size_t color_count = src.width * src.height;
    std::copy_n(src.colors, color_count, this->colors);
  }

  // Assures that the destination frame has enough space
  // to copy whole color data from source
  UNSAFE void
  copy_to_(frame& dest) const
  {
    dest.copy_from_(*this);
  }

  UNSAFE constexpr color&
  at_(size_t x, size_t y)
  {
    return this->at_flat_(y * this->width + x);
  }

  UNSAFE constexpr color&
  at_flat_(size_t flat_offset)
  {
    assert(flat_offset < this->width * this->height);

    return this->colors[flat_offset];
  }

  constexpr size_t
  size() const noexcept
  {
    size_t base_size   = sizeof(frame) - sizeof(frame::colors);
    size_t colors_size = this->width * this->height * sizeof(color);
    size_t offset      = base_size + colors_size;

    return offset;
  }

  constexpr color*
  begin() noexcept
  {
    return &this->colors[0];
  }

  constexpr const color*
  cbegin() const noexcept
  {
    return &this->colors[0];
  }

  constexpr color*
  end() noexcept
  {
    auto colors_count = this->width * this->height;
    return &this->colors[0] + colors_count;
  }

  constexpr const color*
  cend() const noexcept
  {
    auto colors_count = this->width * this->height;
    return &this->colors[0] + colors_count;
  }

  void
  fill(const color& c);

  size_t   width = 0, height = 0;
  uint32_t delay = 0;
  color    colors[0]; // Variable length array
};

struct single_frame_buffer_deleter
{
  template<typename T>
  void
  operator()(T* p) const
  {
    piwo::aligned_free(const_cast<std::remove_const_t<T>*>(p));
  }
};

using single_frame_buffer_t =
  std::unique_ptr<frame, single_frame_buffer_deleter>;

using shared_frame_buffer_t = std::shared_ptr<frame>;

single_frame_buffer_t
alloc_frame(size_t width, size_t height);

shared_frame_buffer_t
alloc_frame_shared(size_t width, size_t height);
} // namespace piwo
