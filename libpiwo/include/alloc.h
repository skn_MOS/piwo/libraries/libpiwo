#pragma once

#include <cstddef>
#include <cstdlib>

namespace piwo
{

inline void*
aligned_alloc(size_t alignment, size_t size)
{
#if defined(__WIN32)
  return _aligned_malloc(size, alignment);
#else
  return ::aligned_alloc(alignment, size);
#endif
}

inline void
aligned_free(void* p)
{
#if defined(__WIN32)
  _aligned_free(p);
#else
  ::free(p);
#endif
}

}
