#pragma once

#include "animation.h"
#include "color.h"
#include <map>
#include <optional>
#include <string>

namespace piwo
{

struct PackedColorComp
{
  bool
  operator()(const piwo::color& lhs, const piwo::color& rhs) const
  {
    return lhs.packed() < rhs.packed();
  }
};

class color_palette
{
public:
  template<typename IteratorType>
  void
  extend(IteratorType begin, IteratorType end, size_t treshold);

  void
  extend(const animation& anim, size_t treshold);

  size_t
  extend(const char* directory_path, size_t treshold);

  size_t
  size() const
  {
    return _palette.size();
  }

  auto
  begin() const
  {
    return _palette.cbegin();
  }

  auto
  end() const
  {
    return _palette.cend();
  }

  void
  clear()
  {
    _palette.clear();
    _rounded_colors.clear();
  }

  std::optional<size_t>
  get_palette_num(const color& col) const;

  std::optional<color>
  get_palette_color(size_t number) const;

  std::optional<color>
  get_rounded_color(const color& col) const;

private:
  color
  round_color(const color& col, size_t treshold) const;

  uint8_t
  round_byte(uint8_t color_byte, size_t treshold) const;

  std::map<color, size_t, PackedColorComp> _palette;
  std::map<color, color, PackedColorComp>  _rounded_colors;
};

template<typename IteratorType>
void
color_palette::extend(IteratorType begin, IteratorType end, size_t treshold)
{
  while (begin != end)
  {
    this->extend(*begin, treshold);
    ++begin;
  }
}

} // namespace piwo
