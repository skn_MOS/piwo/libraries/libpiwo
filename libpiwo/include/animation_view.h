#pragma once

#include "animation.h"

namespace piwo
{

class animation_view
{
public:
  static constexpr std::string_view piwo7_signature = "PIWO_7_FILE";

  using iterator = animation::iterator;

  animation_view()
    : _width(0)
    , _height(0)
    , _frame_count(0)
    , _total_time(0)
    , _frames(nullptr)
  {
  }

  animation_view(const animation_view& other) = default;

  animation_view(const animation& anim)
    : _width(anim.width())
    , _height(anim.height())
    , _frame_count(anim.frame_count())
    , _total_time(anim.total_time())
    , _frames(anim.data())
  {
  }

  animation_view(animation_view&& other) = default;

  animation_view&
  operator=(const animation_view& other) = default;

  animation_view&
  operator=(const animation& anim)
  {
    this->_width       = anim.width();
    this->_height      = anim.height();
    this->_frame_count = anim.frame_count();
    this->_total_time  = anim.total_time();
    this->_frames      = anim.data();

    return *this;
  }

  animation_view&
  operator=(animation_view&& other) = default;

  constexpr size_t
  frame_offset() const noexcept
  {
    size_t base_size   = sizeof(frame) - sizeof(frame::colors);
    size_t colors_size = this->_width * this->_height * sizeof(color);
    size_t offset      = base_size + colors_size;

    return offset;
  }

  iterator
  begin() const noexcept
  {
    auto     frame_ptr = reinterpret_cast<frame*>(this->_frames);
    iterator it(frame_ptr, this->frame_offset());
    return it;
  }

  iterator
  end() const noexcept
  {
    auto frame_ptr_ = reinterpret_cast<char*>(this->_frames);
    frame_ptr_ += this->frame_offset() * this->_frame_count;

    auto     frame_ptr = reinterpret_cast<frame*>(frame_ptr_);
    iterator it(frame_ptr, this->frame_offset());

    return it;
  }

  frame&
  operator[](size_t index) noexcept
  {
    auto frame_ptr = reinterpret_cast<char*>(this->_frames);
    frame_ptr += index * this->frame_offset();
    return *reinterpret_cast<frame*>(frame_ptr);
  }

  auto
  width() const noexcept
  {
    return this->_width;
  }

  auto
  height() const noexcept
  {
    return this->_height;
  }

  auto
  frame_count() const noexcept
  {
    return this->_frame_count;
  }

  auto
  total_time() const noexcept
  {
    return this->_total_time;
  }

  auto
  data() const noexcept
  {
    return this->_frames;
  }

  friend std::ostream&
  operator<<(std::ostream& os, const animation_view& anim) noexcept;

private:
  size_t _width, _height, _frame_count, _total_time;
  char*  _frames;
};

} // namespace piwo
