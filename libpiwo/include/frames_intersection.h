#pragma once
#include <climits>
#include <cmath>
#include <cstdint>
#include <functional>
#include <limits>
#include <memory>

#include "frame.h"

namespace piwo
{
class frames_intersection;

class frames_intersection_unsafe
{
public:
  using merged_pixels = std::pair<piwo::color&, piwo::color&>;
  using frame_t       = piwo::frame;

private:
  class frames_intersection_iterator
  {
  public:
    frames_intersection_iterator(frames_intersection_unsafe& fi)
      : _intersection(fi)
    {
      _pos_x = this->_intersection._lhs_x_beg;
      _pos_y = this->_intersection._lhs_y_beg;
    }

    bool
    operator!=(const frames_intersection_iterator&)
    {
      const size_t lhs_pos =
        this->_pos_y * this->_intersection._lhs->width + this->_pos_x;

      size_t current_pos = lhs_pos - this->_intersection._bias;
      return current_pos != this->_intersection._last_pos;
    }

    frames_intersection_iterator
    operator++(int)
    {
      frames_intersection_iterator temp = *this;
      ++(*this);
      return temp;
    }

    frames_intersection_iterator&
    operator++()
    {
      ++_pos_x;
      if (_pos_x >= _intersection._lhs_x_end)
      {
        ++_pos_y;
        _pos_x = _intersection._lhs_x_beg;
      }

      return *this;
    }

    merged_pixels
    operator*()
    {
      return { this->_intersection._lhs->at_(_pos_x, _pos_y),
               this->_intersection._rhs->at_(_pos_x + _intersection._rhs_x_off,
                                             _pos_y +
                                               _intersection._rhs_y_off) };
    }

  private:
    frames_intersection_unsafe& _intersection;
    size_t                      _pos_x, _pos_y;
  };

  friend class frames_intersection_iterator;
  friend class frames_intersection;

  frames_intersection_unsafe(frame_t& lhs,
                             frame_t& rhs,
                             int64_t  off_x,
                             int64_t  off_y)
    : _lhs(&lhs)
    , _rhs(&rhs)
    , _rhs_x_off(-off_x)
    , _rhs_y_off(-off_y)
  {
    size_t abs_off_x       = saturated_abs(off_x);
    size_t abs_off_y       = saturated_abs(off_y);
    size_t saturated_off_x = std::min(abs_off_x, lhs.width);
    size_t saturated_off_y = std::min(abs_off_y, lhs.height);

    if (off_x < 0)
    {
      _rhs_x_off = saturated_off_x;
      if (abs_off_x <= lhs.width)
      {
        saturated_off_x = 0;
      }
    }
    if (off_y < 0)
    {
      _rhs_y_off = abs_off_y;
      if (abs_off_y <= lhs.height)
      {
        saturated_off_y = 0;
      }
    }

    this->_lhs_x_beg = saturated_off_x;
    this->_lhs_y_beg = saturated_off_y;
    this->_lhs_x_end =
      std::min(static_cast<size_t>(off_x + rhs.width), lhs.width);
    this->_lhs_y_end =
      std::min(static_cast<size_t>(off_y + rhs.height), lhs.height);
    this->_last_pos = (this->_lhs_y_end - 1) * lhs.width + this->_lhs_x_end;
    this->_bias =
      ((this->_lhs_y_end) * lhs.width + this->_lhs_x_beg) - this->_last_pos;
  }

public:
  frames_intersection_iterator
  begin()
  {
    return { *this };
  }

  frames_intersection_iterator
  end()
  {
    return { *this };
  }

  friend void
  unsafe_intersection_apply(piwo::frame& f1,
                            piwo::frame& f2,
                            int64_t      off_x,
                            int64_t      off_y,
                            void (*)(frames_intersection_unsafe));

private:
  frame_t* _lhs;
  frame_t* _rhs;
  // the offsets of rhs frame begining, relative to the lhs begining positions
  size_t _rhs_x_off = 0, _rhs_y_off = 0;
  size_t _lhs_x_beg = 0, _lhs_y_beg = 0;
  size_t _lhs_x_end = 0, _lhs_y_end = 0;
  size_t _last_pos = 0;
  size_t _bias     = 0;

  template<typename T>
  T
  saturated_abs(T val)
  {
    if (val == std::numeric_limits<T>::min())
      return std::numeric_limits<T>::max();
    if (val > 0)
      return val;
    return -val;
  }
};

class frames_intersection
{
public:
  using merged_pixels = std::pair<piwo::color&, piwo::color&>;
  using frame_t       = std::shared_ptr<piwo::frame>;

  using frames_intersection_iterator =
    frames_intersection_unsafe::frames_intersection_iterator;

public:
  frames_intersection(const frame_t& lhs,
                      const frame_t& rhs,
                      int64_t        off_x,
                      int64_t        off_y)
    : _lhs(lhs)
    , _rhs(rhs)
    , unsafe(*lhs, *rhs, off_x, off_y)
  {
  }

  frames_intersection_iterator
  begin()
  {
    return unsafe.begin();
  }

  frames_intersection_iterator
  end()
  {
    return unsafe.end();
  }

private:
  frame_t                    _lhs;
  frame_t                    _rhs;
  frames_intersection_unsafe unsafe;
};

using frames_intersection_operator = void (*)(frames_intersection);
using frames_intersection_operator_unsafe =
  void (*)(frames_intersection_unsafe);

inline void
unsafe_intersection_apply(piwo::frame&                        f1,
                          piwo::frame&                        f2,
                          int64_t                             off_x,
                          int64_t                             off_y,
                          frames_intersection_operator_unsafe fun)
{
  frames_intersection_unsafe intersection(f1, f2, off_x, off_y);
  std::invoke(fun, intersection);
}
} // namespace piwo
