#include "common_intersection_operators.h"

namespace piwo
{

void
frame_add_op(piwo::frames_intersection fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r += p2.r;
    p1.g += p2.g;
    p1.b += p2.b;
  }
};

void
frame_or_op(piwo::frames_intersection fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r |= p2.r;
    p1.g |= p2.g;
    p1.b |= p2.b;
  }
};

void
frame_sub_op(piwo::frames_intersection fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r -= p2.r;
    p1.g -= p2.g;
    p1.b -= p2.b;
  }
};

void
frame_mul_op(piwo::frames_intersection fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r *= p2.r;
    p1.g *= p2.g;
    p1.b *= p2.b;
  }
};

void
frame_assign_op(piwo::frames_intersection fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r = p2.r;
    p1.g = p2.g;
    p1.b = p2.b;
  }
}

void
frame_add_op(piwo::frames_intersection_unsafe fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r += p2.r;
    p1.g += p2.g;
    p1.b += p2.b;
  }
};

void
frame_or_op(piwo::frames_intersection_unsafe fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r |= p2.r;
    p1.g |= p2.g;
    p1.b |= p2.b;
  }
};

void
frame_sub_op(piwo::frames_intersection_unsafe fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r -= p2.r;
    p1.g -= p2.g;
    p1.b -= p2.b;
  }
};

void
frame_mul_op(piwo::frames_intersection_unsafe fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r *= p2.r;
    p1.g *= p2.g;
    p1.b *= p2.b;
  }
};

void
frame_assign_op(piwo::frames_intersection_unsafe fi)
{
  for (auto [p1, p2] : fi)
  {
    p1.r = p2.r;
    p1.g = p2.g;
    p1.b = p2.b;
  }
}
}
