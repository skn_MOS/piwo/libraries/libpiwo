#include "color_palette.h"
#include <algorithm>
#include <filesystem>
#include <limits>

namespace piwo
{

void
color_palette::extend(const animation& anim, size_t treshold)
{
  for (auto& frame : anim)
  {
    for (auto& col : frame)
    {
      if (_rounded_colors.find(col) == _rounded_colors.end())
        _rounded_colors[col] = round_color(col, treshold);
    }
  }

  size_t num = _palette.size();
  for (const auto& [_, col] : _rounded_colors)
  {
    if (_palette.find(col) == _palette.end())
      _palette[col] = num++;
  }
}

size_t
color_palette::extend(const char* directory_path, size_t treshold)
{
  piwo::animation          anim;
  piwo::animation::error_t error_inf;
  size_t                   files_read = 0;

  for (auto& p : std::filesystem::directory_iterator(directory_path))
  {
    // TODO(all) Optimize - on windows it requires conversion wchar[] -> char[]
    error_inf = anim.build_from_file(p.path().string().c_str());
    if (error_inf.error == piwo::animation::error_e::ok)
    {
      ++files_read;
      this->extend(anim, treshold);
    }
  }
  return files_read;
}

std::optional<size_t>
color_palette::get_palette_num(const color& col) const
{
  std::optional<size_t> opt;
  if (_palette.find(col) != _palette.end())
  {
    opt = _palette.at(col);
  }
  return opt;
}

std::optional<color>
color_palette::get_palette_color(size_t number) const
{
  std::optional<color> opt;
  auto it = std::find_if(_palette.begin(), _palette.end(), [=](const auto& i) {
    return i.second == number;
  });
  if (it != _palette.end())
  {
    opt = it->first;
  }
  return opt;
}

std::optional<color>
color_palette::get_rounded_color(const color& col) const
{
  std::optional<color> opt;
  if (_rounded_colors.find(col) != _rounded_colors.end())
  {
    opt = _rounded_colors.at(col);
  }
  return opt;
}

color
color_palette::round_color(const color& col, size_t treshold) const
{
  color new_color;
  new_color.r = this->round_byte(col.r, treshold);
  new_color.g = this->round_byte(col.g, treshold);
  new_color.b = this->round_byte(col.b, treshold);

  return new_color;
}

uint8_t
color_palette::round_byte(uint8_t color_byte, size_t treshold) const
{
  auto    round_lvl = color_byte % treshold;
  uint8_t new_byte  = color_byte - round_lvl;
  if (round_lvl > treshold / 2)
  {
    if (std::numeric_limits<uint8_t>::max() < treshold + new_byte)
    {
      new_byte = std::numeric_limits<uint8_t>::max();
    }
    else
    {
      new_byte += treshold;
    }
  }

  return new_byte;
}

} // namespace piwo
