#include "animation.h"

#include "compiler.h"

#include <algorithm>
#include <ostream>
#include <type_traits>

namespace piwo
{

using parse_option = animation::parse_option;

static bool
verify_signature_(animation::parser_context& parser)
{
  const auto f_beg = parser.f_pos;

  auto isspace       = [](auto c) -> bool { return std::isspace(c); };
  auto signature_end = std::find_if(f_beg, parser.f_end, isspace);

  if (signature_end == parser.f_end)
    return false;

  auto signature_size = signature_end - f_beg;
  parser.f_pos        = signature_end;

  return std::string_view(f_beg, signature_size) == animation::piwo7_signature;
}

animation::error_t
animation::build_from_file(const char* filename, parse_option op) noexcept
{
  mipc::finbuf file(filename);

  if (MOS_UNLIKELY(!file))
    return animation::error_e::cant_open_file;

  return build_from_file(file, op);
}

using parse_result = std::from_chars_result;

#define FETCH_ULL_CHECKED(parser, ull, err)                                    \
  if (!fetch_unsignedT(parser, ull))                                           \
    return err;

template<typename T>
bool
fetch_unsignedT(animation::parser_context& parser, T& value, int base = 10)
{
  static_assert(std::is_unsigned_v<T>);

  while (parser.f_pos != parser.f_end && std::isspace(*parser.f_pos) && *parser.f_pos != '\n')
    ++parser.f_pos;

  auto [ptr, err] = std::from_chars(parser.f_pos, parser.f_end, value, base);

  if (MOS_UNLIKELY(err != std::errc() || value > color::MAX_VALUE))
    return false;

  parser.f_pos = ptr;
  return true;
}

static bool
ignore_non_empty_lines(animation::parser_context& parser, size_t line_count)
{
  bool line_with_character = false;

  while (line_count > 0)
  {
    if (parser.f_pos >= parser.f_end)
      return false;

    if (*parser.f_pos == '\n')
    {
      if(!line_with_character)
        return false;

      line_with_character = false;
      ++parser.lines;
      --line_count;
    }

    if(!std::isspace(*parser.f_pos))
      line_with_character = true;

    ++parser.f_pos;
  }

  return true;
}

static bool
ignore_ws_line(animation::parser_context& parser)
{
  bool line_with_character = false;

  while (parser.f_pos < parser.f_end)
  {
    if (*parser.f_pos == '\n')
    {
      if(line_with_character)
        return false;

      ++parser.lines;
      ++parser.f_pos;
      return true;
    }

    if(!std::isspace(*parser.f_pos))
      line_with_character = true;

    ++parser.f_pos;
  }

  return false;
}

#define IGNORE_WS_TILL_LINE(parser, err)                                   \
  if(!ignore_whitespace_till_line(parser))                                 \
    return err;

static bool
ignore_whitespace_till_line(animation::parser_context& parser)
{
  while (parser.f_pos < parser.f_end)
  {
    auto c = *parser.f_pos;

    if (!std::isspace(c))
      return false;

    if (c == '\n')
    {
      ++parser.lines;
      ++parser.f_pos;
      return true;
    }

    ++parser.f_pos;
  }

  return false;
}

static bool
ends_with_ws(animation::parser_context parser)
{
  while (parser.f_pos < parser.f_end)
  {
    if (!std::isspace(*parser.f_pos))
      return false;

    ++parser.f_pos;
  }

  return true;
}

static bool
ends_with_ws(animation::parser_context parser, size_t& lines)
{
  while(parser.f_pos < parser.f_end)
  {
    if (!std::isspace(*parser.f_pos))
      return false;

    if(*parser.f_pos == '\n')
      ++lines;

    ++parser.f_pos;
  }

  return true;
}

// TODO perhaps return meaningfull error code instead of bool
static bool
count_frames_and_time(animation::parser_context parser,
                      size_t       frame_height,
                      parse_option op,
                      size_t&      frame_count,
                      size_t&      total_time)
{
  frame_count = 0;
  total_time  = 0;

  if (parser.f_pos >= parser.f_end)
    return true;

  for (;;)
  {
    // we allow files to end with arbitrary amount of whitespace
    if (MOS_UNLIKELY(ends_with_ws(parser)))
      break;

    // each frame starts with empty line
    if (!ignore_ws_line(parser))
      return false;

    // then parse frame delay
    size_t delay;
    FETCH_ULL_CHECKED(parser, delay, false);

    if (op.translate == parse_option::translate_type::raw)
    {
      ++frame_count;
    }
    else if (op.translate == parse_option::translate_type::ms50_exact)
    {

      size_t mul = delay / 50;
      size_t rem = delay % 50;

      if (rem)
        return false;

      frame_count += mul;
    }
    else
    {

      // TODO add handling for rest of the flags
      return false;
    }

    total_time += delay;

    IGNORE_WS_TILL_LINE(parser, false);    // delay parsed so we can go further with parser.f_pos

    // skip height
    if (!ignore_non_empty_lines(parser, frame_height))
      return false;
  }

  return true;
}

static bool
parse_frame(animation::parser_context& parser, frame& f)
{
  size_t current_color;
  color* output = f.colors;

  for (auto h = f.height; h > 0; --h)
  {
    for (auto w = 0ull; w < f.width; ++w)
    {
      FETCH_ULL_CHECKED(parser, current_color, false)
      output[(h - 1) * f.width + w] = piwo::color(current_color);
    }

    IGNORE_WS_TILL_LINE(parser, false);
  }

  return true;
}

static bool
parse_frames_raw(animation& self, animation::parser_context& parser)
{
  auto   frame_count = self.frame_count();
  size_t index       = 0;

  while (index < frame_count)
  {
    if(!ignore_ws_line(parser))
      return false;

    uint32_t delay;
    FETCH_ULL_CHECKED(parser, delay, false);
    IGNORE_WS_TILL_LINE(parser, false);

    auto& frame  = self[index];
    frame.delay  = delay;
    frame.width  = self.width();
    frame.height = self.height();

    if(!parse_frame(parser, frame))
      return false;

    ++index;
  }

  return true;
}

static bool
parse_frames_ms50(animation& self, animation::parser_context& parser)
{
  const auto     frame_count = self.frame_count();
  constexpr auto frame_delay = 50;
  size_t         index       = 0;

  while (index < frame_count)
  {
    if(!ignore_ws_line(parser))
      return false;

    uint32_t delay;
    FETCH_ULL_CHECKED(parser, delay, false);
    IGNORE_WS_TILL_LINE(parser, false);

    auto& frame  = self[index];
    frame.delay  = frame_delay;
    frame.width  = self.width();
    frame.height = self.height();

    if(!parse_frame(parser, frame))
      return false;

    ++index;
    delay -= frame_delay;

    while (delay > 0)
    {
      self[index].copy_from_(frame);
      ++index;
      delay -= frame_delay;
    }
  }

  return true;
}

static animation::error_t
parse_frame_error_seeking(animation& self, animation::parser_context& parser, parse_option op)
{
  auto& frame = self[0];
  frame.width = self.width();
  frame.height = self.height();

  for(;;)
  {
    if(!ignore_ws_line(parser))
      return animation::error_t(animation::error_e::bad_parse, parser.lines);

    uint32_t delay;
    FETCH_ULL_CHECKED(parser, delay, animation::error_t(animation::error_e::bad_parse, parser.lines));

    switch (op.translate)
    {
      case parse_option::translate_type::raw: // no need to check raw delay
        break;

      case parse_option::translate_type::ms50_exact:
      {
        size_t rem = delay % 50;

        if (rem)
          return animation::error_t(animation::error_e::bad_parse, parser.lines);
      }
        break;

      default:
        return animation::error_e::bad_option;
    }

   // delay parsed, going further with parser.f_pos 
    IGNORE_WS_TILL_LINE(parser, animation::error_t(animation::error_e::bad_parse, parser.lines));

    if(!parse_frame(parser, frame))
       return animation::error_t(animation::error_e::bad_parse, parser.lines);
  }
}

animation::error_t
animation::build_from_file_(parser_context& parser, parse_option op) noexcept
{
  if (MOS_UNLIKELY(!verify_signature_(parser)))
    return error_t(error_e::bad_signature, parser.lines);

  IGNORE_WS_TILL_LINE(parser, error_t(error_e::bad_parse, parser.lines));
  FETCH_ULL_CHECKED(parser, this->_width, error_t(error_e::bad_parse, parser.lines));
  FETCH_ULL_CHECKED(parser, this->_height, error_t(error_e::bad_parse, parser.lines));
  IGNORE_WS_TILL_LINE(parser, error_t(error_e::bad_parse, parser.lines));

  size_t frame_count, total_time;
  const bool early_success = count_frames_and_time(
        parser, this->_height, op, frame_count, total_time);

  const auto frame_size =
    sizeof(frame) + this->_width * this->_height * sizeof(color);

  if(!early_success)
  {
    this->_frames = std::make_unique<char[]>(frame_size);
  
    return parse_frame_error_seeking(*this, parser, op);
  }

  this->_frame_count = frame_count;
  this->_total_time  = total_time;

  if (frame_count == 0)
   return animation::error_e::ok;

  decltype(frame_count * frame_size) total_size;
  if (MOS_UNLIKELY(
        __builtin_mul_overflow(frame_count, frame_size, &total_size)))
    return animation::error_e::bad_size;

  this->_frames = std::make_unique<char[]>(total_size);

  bool status = false;

  switch (op.translate)
  {
    case parse_option::translate_type::raw:
      status = parse_frames_raw(*this, parser);
      break;

    case parse_option::translate_type::ms50_exact:
      status = parse_frames_ms50(*this, parser);
      break;

    default:
      return animation::error_e::bad_option;
  }

  if (MOS_UNLIKELY(!status))
    return error_t(error_e::bad_parse, parser.lines);

  if (MOS_UNLIKELY(!ends_with_ws(parser, parser.lines)))
    return error_t(error_e::bad_parse, parser.lines);

  return animation::error_e::ok;
}

std::ostream&
operator<<(std::ostream& os, const animation& anim) noexcept
{
  os << animation::piwo7_signature << '\n' << anim._width << ' ' << anim._height << '\n';

  for (auto& frame : anim)
  {
    os << '\n' << frame.delay << '\n';
    for (auto y = anim._height; y > 0; --y)
    {
      for (auto x = 0ull; x < anim._width; ++x)
      {
        os << frame.at_(x, y - 1).convert<size_t>() << ' ';
      }
      os << '\n';
    }
  }

  return os;
}
} // namespace piwo
