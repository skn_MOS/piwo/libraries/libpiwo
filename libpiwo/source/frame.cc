#include <cstdlib>
#include <memory>

#include "alloc.h"
#include "frame.h"

namespace piwo
{

void
frame::fill(const color& c)
{
  std::fill(this->begin(), this->end(), c);
}

template<typename T>
constexpr T
align_size_up(T size, size_t alignment)
{
  if (alignment == 0)
    return size;

  size += alignment - 1;
  size -= size % alignment;

  return size;
}

single_frame_buffer_t
alloc_frame(size_t width, size_t height)
{
  const size_t color_buffer_size = sizeof(color) * width * height;
  const size_t total_frame_size  = sizeof(frame) + color_buffer_size;

  void* frame_buffer =
    aligned_alloc(std::alignment_of_v<frame>,
                  align_size_up(total_frame_size, std::alignment_of_v<frame>));

  if (frame_buffer == nullptr)
    return nullptr;

  single_frame_buffer_t frame_ptr;
  frame_ptr.reset(static_cast<frame*>(frame_buffer));

  frame_ptr->width  = width;
  frame_ptr->height = height;
  frame_ptr->delay  = 0;

  return frame_ptr;
}

shared_frame_buffer_t
alloc_frame_shared(size_t width, size_t height)
{
  const size_t color_buffer_size = sizeof(color) * width * height;
  const size_t total_frame_size  = sizeof(frame) + color_buffer_size;

  void* frame_buffer =
    aligned_alloc(std::alignment_of_v<frame>,
                  align_size_up(total_frame_size, std::alignment_of_v<frame>));

  if (frame_buffer == nullptr)
    return nullptr;

  shared_frame_buffer_t frame_ptr(static_cast<frame*>(frame_buffer),
                                  single_frame_buffer_deleter());

  frame_ptr->width  = width;
  frame_ptr->height = height;
  frame_ptr->delay  = 0;

  return frame_ptr;
}

} // namespace piwo
