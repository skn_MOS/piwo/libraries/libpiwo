#include "animation.h"

#include <mipc/file.h>

#include <gtest/gtest.h>

#include <istream>
#include <sstream>
#include <string_view>

using namespace std;
using namespace testing;
using namespace piwo;
using namespace mipc;

// DO NOT CHANGE !!!
// TESTS DEPENDS ON THIS CONTENT
constexpr std::string_view test_file_template = "PIWO_7_FILE\n"
                                                "3 3\n"
                                                "\n"
                                                "57\n"
                                                "7 8 9 \n"
                                                "4 5 6 \n"
                                                "1 2 3 \n"
                                                "\n"
                                                "82\n"
                                                "16 17 18 \n"
                                                "13 14 15 \n"
                                                "10 11 12 \n";

// Returned buffer in the string_view has to be freed manually
std::string_view
copy_test_file(std::string_view filebuf)
{
  char*  buf     = new char[filebuf.size()];
  size_t bufsize = filebuf.size();

  std::copy(std::begin(filebuf), std::end(filebuf), buf);

  return { buf, bufsize };
}

class CommonTest : public ::testing::Test
{
protected:
  CommonTest()          = default;
  virtual ~CommonTest() = default;

  void
  SetUp() override
  {
    auto   test_file   = copy_test_file(test_file_template);
    finbuf test_finbuf = finbuf_unsafe_init(test_file.size(), test_file.data());

    auto error = anim.build_from_file(test_finbuf);

    if (error.error != animation::error_e::ok)
      FAIL() << "Failed to prepare test animation. Build from file failed.";

    if (anim.frame_count() != 2)
      FAIL() << "Failed to prepare test animation. Incorrect size.";

    if (anim.total_time() != 57 + 82)
      FAIL() << "Failed to prepare test animation. Incorrect size.";

    size_t i = 1;
    for (auto&& frame : anim)
    {
      for (auto it = frame.begin(); it != frame.end(); ++it)
      {
        if (*it != color::convert(i))
          FAIL() << "Failed to prepare test animation. Unexpected color.";
        ++i;
      }
    }
  }

  piwo::animation anim;
};

TEST_F(CommonTest, move_ctor)
{
  auto anim_width       = anim.width();
  auto anim_height      = anim.height();
  auto anim_frame_count = anim.frame_count();
  auto anim_total_time  = anim.total_time();

  piwo::animation anim2(std::move(this->anim));

  ASSERT_EQ(anim.frame_count(), 0);

  ASSERT_EQ(anim2.width(), anim_width);
  ASSERT_EQ(anim2.height(), anim_height);
  ASSERT_EQ(anim2.frame_count(), anim_frame_count);
  ASSERT_EQ(anim2.total_time(), anim_total_time);

  size_t i = 1;
  for (auto&& frame : anim2)
  {
    for (auto it = frame.begin(); it != frame.end(); ++it)
    {
      ASSERT_EQ(*it, color::convert(i));
      ++i;
    }
  }
}

TEST_F(CommonTest, move_assign_operator)
{
  auto anim_width       = anim.width();
  auto anim_height      = anim.height();
  auto anim_frame_count = anim.frame_count();
  auto anim_total_time  = anim.total_time();

  piwo::animation anim2;
  anim2 = std::move(this->anim);

  ASSERT_EQ(anim.frame_count(), 0);

  ASSERT_EQ(anim2.width(), anim_width);
  ASSERT_EQ(anim2.height(), anim_height);
  ASSERT_EQ(anim2.frame_count(), anim_frame_count);
  ASSERT_EQ(anim2.total_time(), anim_total_time);

  size_t i = 1;
  for (auto&& frame : anim2)
  {
    for (auto it = frame.begin(); it != frame.end(); ++it)
    {
      ASSERT_EQ(*it, color::convert(i));
      ++i;
    }
  }
}

TEST_F(CommonTest, copy_ctor)
{
  auto anim_width       = anim.width();
  auto anim_height      = anim.height();
  auto anim_frame_count = anim.frame_count();
  auto anim_total_time  = anim.total_time();

  piwo::animation anim2(this->anim);

  ASSERT_EQ(anim.width(), anim_width);
  ASSERT_EQ(anim.height(), anim_height);
  ASSERT_EQ(anim.frame_count(), anim_frame_count);
  ASSERT_EQ(anim.total_time(), anim_total_time);

  ASSERT_EQ(anim2.width(), anim_width);
  ASSERT_EQ(anim2.height(), anim_height);
  ASSERT_EQ(anim2.frame_count(), anim_frame_count);
  ASSERT_EQ(anim2.total_time(), anim_total_time);

  size_t i = 1;
  for (auto&& frame : anim2)
  {
    for (auto it = frame.begin(); it != frame.end(); ++it)
    {
      ASSERT_EQ(*it, color::convert(i));
      ++i;
    }
  }
}

TEST_F(CommonTest, copy_assign_operator)
{
  auto anim_width       = anim.width();
  auto anim_height      = anim.height();
  auto anim_frame_count = anim.frame_count();
  auto anim_total_time  = anim.total_time();

  piwo::animation anim2 = this->anim;

  ASSERT_EQ(anim.width(), anim_width);
  ASSERT_EQ(anim.height(), anim_height);
  ASSERT_EQ(anim.frame_count(), anim_frame_count);
  ASSERT_EQ(anim.total_time(), anim_total_time);

  ASSERT_EQ(anim2.width(), anim_width);
  ASSERT_EQ(anim2.height(), anim_height);
  ASSERT_EQ(anim2.frame_count(), anim_frame_count);
  ASSERT_EQ(anim2.total_time(), anim_total_time);

  size_t i = 1;
  for (auto&& frame : anim2)
  {
    for (auto it = frame.begin(); it != frame.end(); ++it)
    {
      ASSERT_EQ(*it, color::convert(i));
      ++i;
    }
  }
}

TEST_F(CommonTest, frame_iterator_range_based_for_loop_iterate_over_colors)
{
  piwo::animation anim2 = this->anim;

  size_t i = 1;
  for (auto& frame : anim)
  {
    for (auto& c : frame)
    {
      ASSERT_EQ(c, color::convert(i));
      ++i;
    }
  }
}

TEST_F(CommonTest, frame_iterator_copy_range_based_for_loop_iterate_over_colors)
{
  piwo::animation anim2 = this->anim;

  size_t i = 1;
  for (auto& frame : anim)
  {
    for (auto c : frame)
    {
      ASSERT_EQ(c, color::convert(i));
      ++i;
    }
  }
}

TEST_F(CommonTest, frame_iterator_pre_increment_operator)
{
  piwo::animation anim2 = this->anim;

  auto it      = anim2.begin();
  auto it_copy = it;
  auto it_new  = ++it;

  ASSERT_NE(it, it_copy);
  ASSERT_EQ(it_new->at_(0, 0), it->at_(0, 0));
}

TEST_F(CommonTest, frame_iterator_post_increment_operator)
{
  piwo::animation anim2 = this->anim;

  auto it     = anim2.begin();
  auto it_old = it++;

  ASSERT_EQ(it_old->at_(0, 0), color::convert(1));
  ASSERT_EQ(it->at_(0, 0), color::convert(10));
}
