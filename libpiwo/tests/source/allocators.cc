/*
 * This file test custom memory allocators and allocations helpers / utilities /
 * wrappers. It is strongly advised to run these test with address sanitizer (or
 * other similar tool) turned on.
 */

#include <memory>

#include <gtest/gtest.h>

#include "frame.h"

using namespace testing;
using namespace piwo;

TEST(alloc_frame_test, alloc_frame_doesnt_leak_memory)
{
  constexpr size_t width  = 12;
  constexpr size_t height = 10;

  auto f = alloc_frame(width, height);

  EXPECT_EQ(f->width, width);
  EXPECT_EQ(f->height, height);
}

TEST(alloc_frame_test, alloc_frame_allocates_enough_storage_for_colors)
{
  constexpr size_t width       = 12;
  constexpr size_t height      = 10;
  constexpr size_t color_count = width * height;

  auto f = alloc_frame(width, height);

  EXPECT_EQ(f->width, width);
  EXPECT_EQ(f->height, height);

  for (auto i = 0ull; i < color_count; ++i)
  {
    volatile color* cp = &f->colors[i];

    // Force volatile load, to let sanitizer check access bounds.
    [[maybe_unused]] const auto r = cp->r;
    [[maybe_unused]] const auto g = cp->g;
    [[maybe_unused]] const auto b = cp->b;
  }
}

TEST(alloc_shared_frame_test, alloc_frame_doesnt_leak_memory)
{
  constexpr size_t width  = 12;
  constexpr size_t height = 10;

  auto f = alloc_frame_shared(width, height);

  EXPECT_EQ(f->width, width);
  EXPECT_EQ(f->height, height);
}

TEST(alloc_shared_frame_test, alloc_frame_allocates_enough_storage_for_colors)
{
  constexpr size_t width       = 12;
  constexpr size_t height      = 10;
  constexpr size_t color_count = width * height;

  auto f = alloc_frame_shared(width, height);

  EXPECT_EQ(f->width, width);
  EXPECT_EQ(f->height, height);

  for (auto i = 0ull; i < color_count; ++i)
  {
    volatile color* cp = &f->colors[i];

    // Force volatile load, to let sanitizer check access bounds.
    [[maybe_unused]] const auto r = cp->r;
    [[maybe_unused]] const auto g = cp->g;
    [[maybe_unused]] const auto b = cp->b;
  }
}
