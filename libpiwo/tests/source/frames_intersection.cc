#include <gtest/gtest.h>

#include "frames_intersection.h"

namespace
{
// clang-format off
const piwo::color frame_default_4x6[] = { {0x1,0x1,0x1}, {0x2,0x2,0x2}, {0x3,0x3,0x3}, {0x4,0x4,0x4},
                                          {0x5,0x5,0x5}, {0x6,0x6,0x6}, {0x7,0x7,0x7}, {0x8,0x8,0x8},
                                          {0x9,0x9,0x9}, {0x1,0x1,0x1}, {0x2,0x2,0x2}, {0x3,0x3,0x3},
                                          {0x4,0x4,0x4}, {0x5,0x5,0x5}, {0x6,0x6,0x6}, {0x7,0x7,0x7},
                                          {0x8,0x8,0x8}, {0x9,0x9,0x9}, {0x1,0x1,0x1}, {0x2,0x2,0x2},
                                          {0x3,0x3,0x3}, {0x4,0x4,0x4}, {0x5,0x5,0x5}, {0x6,0x6,0x6} };

const piwo::color frame_default_2x2[] = { {0x5,0x5,0x5}, {0x6,0x6,0x6},
                                          {0x7,0x7,0x7}, {0x8,0x8,0x8}, };
// clang-format on

void
make_iterate_test(const piwo::frames_intersection::frame_t& frame1,
                  const piwo::frames_intersection::frame_t& frame2,
                  int64_t                                   off_x,
                  int64_t                                   off_y,
                  const piwo::color*                        result_lhs,
                  const piwo::color*                        result_rhs,
                  uint8_t                                   result_size)
{
  size_t frame_number = 0;

  piwo::frames_intersection frames(frame1, frame2, off_x, off_y);
  for (auto frame_pair : frames)
  {
    ASSERT_TRUE(frame_number < result_size);
    EXPECT_EQ(frame_pair.first.convert<uint32_t>(),
              result_lhs[frame_number].convert<uint32_t>());
    EXPECT_EQ(frame_pair.second.convert<uint32_t>(),
              result_rhs[frame_number].convert<uint32_t>());
    frame_number++;
  }
  EXPECT_EQ(frame_number, result_size);
}

void
make_iterate_test(const piwo::frames_intersection::frame_t& frame1,
                  const piwo::frames_intersection::frame_t& frame2,
                  int64_t                                   off_x,
                  int64_t                                   off_y,
                  const piwo::color*                        result,
                  uint8_t                                   result_size)
{
  make_iterate_test(frame1, frame2, off_x, off_y, result, result, result_size);
}
} // namespace

// clang-format off
//  ////////////////////////////////////////////////////////////////////////
//  This is an example image frame with offsets: off_x = 0, off_y = 0
//    ____________________
//   | 0x1  0x2  0x3  0x4 |
//   | 0x5  0x6  0x7  0x8 | <- lhs frame
//   | 0x9  0x1  0x2  0x3 |
//   | 0x4  0x5  0x6  0x7 |
//   | 0x8  0x9  0x1  0x2 | <- rhs frame
//   | 0x3  0x4  0x5  0x6 |
//   |____________________|
//
// The intersection is the place we are intresterd in
// clang-format on

TEST(FrameIntersections, create_correct_iteration_range_without_frame_offset)
{

  constexpr size_t width  = 4;
  constexpr size_t height = 6;

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;

  const uint8_t default_frame_size = std::size(frame_default_4x6);

  ASSERT_TRUE(default_frame_size >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size >= frame2->width * frame2->height);

  for (int i = 0; i < default_frame_size; i++)
  {
    frame1->begin()[i] = frame_default_4x6[i];
    frame2->begin()[i] = frame_default_4x6[i];
  }

  int64_t off_x = 0;
  int64_t off_y = 0;
  make_iterate_test(
    frame1, frame2, off_x, off_y, frame_default_4x6, default_frame_size);
}

// clang-format off
//  ////////////////////////////////////////////////////////////////////////
//  This is an example image frame with negative offsets: off_x 2, off_y =3
//    ____________________
//   | 0x1  0x2  0x3  0x4 |
//   | 0x5  0x6  0x7  0x8 | <- lhs frame
//   | 0x9  0x1  0x2  0x3 |
//               ____________________
//   | 0x4  0x5 |6/1  7/2 | 0x3  0x4|   6/1 <- means that value 6 belongs to lhs frame and 1 to rhs frame
//   | 0x8  0x9 |1/5  2/6 | 0x7  0x8| <- rhs frame
//   | 0x3  0x4 |5/9  6/1 | 0x2  0x3|
//   |__________|_________|         |
//              |0x4  0x5   0x6  0x7|
//              |0x8  0x9   0x1  0x2|
//              |0x3  0x4   0x5  0x6|
//              |___________________|
//
// The intersection is the place we are intresterd in
// clang-format on

TEST(FrameIntersections,
     create_correct_iteration_range_with_rhs_frame_positive_offset)
{
  constexpr size_t width  = 4;
  constexpr size_t height = 6;

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;
  const uint8_t default_frame_size = std::size(frame_default_4x6);

  ASSERT_TRUE(default_frame_size >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size >= frame2->width * frame2->height);
  for (int i = 0; i < default_frame_size; i++)
  {
    frame1->begin()[i] = frame_default_4x6[i];
    frame2->begin()[i] = frame_default_4x6[i];
  }

  int64_t     off_x      = 2;
  int64_t     off_y      = 3;
  piwo::color res1_lhs[] = { { 0x6, 0x6, 0x6 }, { 0x7, 0x7, 0x7 },
                             { 0x1, 0x1, 0x1 }, { 0x2, 0x2, 0x2 },
                             { 0x5, 0x5, 0x5 }, { 0x6, 0x6, 0x6 } };
  piwo::color res1_rhs[] = { { 0x1, 0x1, 0x1 }, { 0x2, 0x2, 0x2 },
                             { 0x5, 0x5, 0x5 }, { 0x6, 0x6, 0x6 },
                             { 0x9, 0x9, 0x9 }, { 0x1, 0x1, 0x1 } };
  ASSERT_TRUE(std::size(res1_lhs) == std::size(res1_rhs));
  make_iterate_test(
    frame1, frame2, off_x, off_y, res1_lhs, res1_rhs, std::size(res1_lhs));

  off_x                  = 3;
  off_y                  = 5;
  piwo::color res2_lhs[] = { { 0x6, 0x6, 0x6 } };
  piwo::color res2_rhs[] = { { 0x1, 0x1, 0x1 } };
  ASSERT_TRUE(std::size(res2_lhs) == std::size(res2_rhs));
  make_iterate_test(
    frame1, frame2, off_x, off_y, res2_lhs, res2_rhs, std::size(res2_lhs));

  off_x              = 4;
  off_y              = 6;
  piwo::color res3[] = {};
  make_iterate_test(
    frame1, frame2, off_x, off_y, res3, sizeof(res3) / sizeof(piwo::color));
}

// clang-format off
//  ////////////////////////////////////////////////////////////////////////
//  This is an example image frame with negative offsets: off_x -2, off_y =-3
//    ____________________
//   | 0x1  0x2  0x3  0x4 |
//   | 0x5  0x6  0x7  0x8 | <- rhs frame
//   | 0x9  0x1  0x2  0x3 |
//               ____________________
//   | 0x4  0x5 |6/1  7/2 | 0x3  0x4|   6/1 <- means that value 6 belongs to rhs frame and 1 to lhs frame
//   | 0x8  0x9 |1/5  2/6 | 0x7  0x8| <- lhs frame
//   | 0x3  0x4 |5/9  6/1 | 0x2  0x3|
//   |__________|_________|         |
//              |0x4  0x5   0x6  0x7|
//              |0x8  0x9   0x1  0x2|
//              |0x3  0x4   0x5  0x6|
//              |___________________|
//
// The intersection is the place we are intresterd in
// clang-format on

TEST(FrameIntersections,
     create_correct_iteration_range_with_rhs_frame_negative_offset)
{
  constexpr size_t width  = 4;
  constexpr size_t height = 6;

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;
  const uint8_t default_frame_size = std::size(frame_default_4x6);

  ASSERT_TRUE(default_frame_size >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size >= frame2->width * frame2->height);
  for (int i = 0; i < default_frame_size; i++)
  {
    frame1->begin()[i] = frame_default_4x6[i];
    frame2->begin()[i] = frame_default_4x6[i];
  }

  int64_t     off_x      = -2;
  int64_t     off_y      = -3;
  piwo::color res1_lhs[] = { { 0x1, 0x1, 0x1 }, { 0x2, 0x2, 0x2 },
                             { 0x5, 0x5, 0x5 }, { 0x6, 0x6, 0x6 },
                             { 0x9, 0x9, 0x9 }, { 0x1, 0x1, 0x1 } };
  piwo::color res1_rhs[] = { { 0x6, 0x6, 0x6 }, { 0x7, 0x7, 0x7 },
                             { 0x1, 0x1, 0x1 }, { 0x2, 0x2, 0x2 },
                             { 0x5, 0x5, 0x5 }, { 0x6, 0x6, 0x6 } };
  ASSERT_TRUE(std::size(res1_lhs) == std::size(res1_rhs));
  make_iterate_test(
    frame1, frame2, off_x, off_y, res1_lhs, res1_rhs, std::size(res1_lhs));

  off_x                  = -1;
  off_y                  = -5;
  piwo::color res2_lhs[] = { { 0x1, 0x1, 0x1 },
                             { 0x2, 0x2, 0x2 },
                             { 0x3, 0x3, 0x3 } };
  piwo::color res2_rhs[] = {
    { 0x4, 0x4, 0x4 },
    { 0x5, 0x5, 0x5 },
    { 0x6, 0x6, 0x6 },
  };
  ASSERT_TRUE(std::size(res2_lhs) == std::size(res2_rhs));

  make_iterate_test(
    frame1, frame2, off_x, off_y, res2_lhs, res2_rhs, std::size(res2_lhs));

  off_x              = -4;
  off_y              = -6;
  piwo::color res3[] = {};

  make_iterate_test(
    frame1, frame2, off_x, off_y, res3, sizeof(res3) / sizeof(piwo::color));
}

// No frames generated, cause of incorrect offsets
TEST(FrameIntersections,
     create_correct_iteration_range_with_offset_greater_than_frame_size)
{
  constexpr size_t width  = 4;
  constexpr size_t height = 6;

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;

  auto off_x = std::numeric_limits<int64_t>::min();
  auto off_y = std::numeric_limits<int64_t>::min();

  size_t cnt = 0;

  piwo::frames_intersection frames(frame1, frame2, off_x, off_y);
  for ([[maybe_unused]] auto frame_pair : frames)
  {
    ASSERT_TRUE(false); // the frame shouldn't enter any time
  }

  off_x = std::numeric_limits<int64_t>::max();
  off_y = std::numeric_limits<int64_t>::max();
  cnt   = 0;
  piwo::frames_intersection frames2(frame1, frame2, off_x, off_y);
  for ([[maybe_unused]] auto frame_pair : frames2)
  {
    ASSERT_TRUE(false); // the frame shouldn't enter any time
  }
}

// clang-format off
//  ////////////////////////////////////////////////////////////////////////
//  This is an example image frame with offsets: off_x = 1, off_y = 2 and off_x=3, off_y=2
//    ____________________                                ____________________
//   | 0x1  0x2  0x3  0x4 |                              | 0x1  0x2  0x3  0x4 |
//   | 0x5  0x6  0x7  0x8 | <- lhs frame                 | 0x5  0x6  0x7  0x8 | <- lhs frame
//   |      ________      |                              |                ________
//   | 0x9 |1/5  2/6| 0x3 |                              | 0x9  0x1  0x2 |3/5 |0x6|
//   | 0x4 |5/7  6/8| 0x7 |                              | 0x4  0x5  0x6 |7/7 |0x8|
//   |     |________| <----- rhs frame                   |               |________| <- rhs frame
//   | 0x8  0x9  0x1  0x2 |                              | 0x8  0x9  0x1  0x2 |
//   | 0x3  0x4  0x5  0x6 |                              | 0x3  0x4  0x5  0x6 |
//   |____________________|                              |____________________|
//
// The intersection is the place we are intresterd in
// clang-format on

TEST(
  FrameIntersections,
  create_correct_iteration_range_when_rhs_frame_is_smaller_than_lhs_with_positive_and_negative_offsets)
{
  constexpr size_t w1 = 4;
  constexpr size_t h1 = 6;
  constexpr size_t w2 = 2;
  constexpr size_t h2 = 2;

  auto frame1 = piwo::alloc_frame_shared(w1, h1);
  if (frame1 == nullptr)
    return;

  auto frame2 = piwo::alloc_frame_shared(w2, h2);
  if (frame2 == nullptr)
    return;

  const uint8_t default_frame_size1 = std::size(frame_default_4x6);
  const uint8_t default_frame_size2 = std::size(frame_default_2x2);

  ASSERT_TRUE(default_frame_size1 >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size2 >= frame2->width * frame2->height);

  for (int i = 0; i < default_frame_size1; i++)
  {
    frame1->begin()[i] = frame_default_4x6[i];
  }

  for (int i = 0; i < default_frame_size2; i++)
  {
    frame2->begin()[i] = frame_default_2x2[i];
  }

  int64_t     off_x      = 1;
  int64_t     off_y      = 2;
  piwo::color res1_lhs[] = {
    { 0x1, 0x1, 0x1 }, { 0x2, 0x2, 0x2 }, { 0x5, 0x5, 0x5 }, { 0x6, 0x6, 0x6 }
  };
  piwo::color res1_rhs[] = {
    { 0x5, 0x5, 0x5 }, { 0x6, 0x6, 0x6 }, { 0x7, 0x7, 0x7 }, { 0x8, 0x8, 0x8 }
  };
  ASSERT_TRUE(std::size(res1_lhs) == std::size(res1_rhs));

  make_iterate_test(
    frame1, frame2, off_x, off_y, res1_lhs, res1_rhs, std::size(res1_lhs));
  off_x                  = 3;
  off_y                  = 2;
  piwo::color res2_lhs[] = { { 0x3, 0x3, 0x3 }, { 0x7, 0x7, 0x7 } };
  piwo::color res2_rhs[] = { { 0x5, 0x5, 0x5 }, { 0x7, 0x7, 0x7 } };
  ASSERT_TRUE(std::size(res2_lhs) == std::size(res2_rhs));

  make_iterate_test(
    frame1, frame2, off_x, off_y, res2_lhs, res2_rhs, std::size(res2_lhs));

  off_x                  = 3;
  off_y                  = 5;
  piwo::color res3_lhs[] = { { 0x6, 0x6, 0x6 } };
  piwo::color res3_rhs[] = { { 0x5, 0x5, 0x5 } };
  ASSERT_TRUE(std::size(res3_lhs) == std::size(res3_rhs));

  make_iterate_test(
    frame1, frame2, off_x, off_y, res3_lhs, res3_rhs, std::size(res3_lhs));

  off_x                  = -1;
  off_y                  = -1;
  piwo::color res4_lhs[] = { { 0x1, 0x1, 0x1 } };
  piwo::color res4_rhs[] = { { 0x8, 0x8, 0x8 } };
  ASSERT_TRUE(std::size(res4_lhs) == std::size(res4_rhs));

  make_iterate_test(
    frame1, frame2, off_x, off_y, res4_lhs, res4_rhs, std::size(res4_lhs));
}

// clang-format off
//  ////////////////////////////////////////////////////////////////////////
//  This is an example image frame with offsets: off_x = 1, off_y = 1
//    ____________________
//   | 0x1  0x2  0x3  0x4 |
//   |      ________ <------ lhs frame
//   | 0x5 |6/5  7/6| 0x8 |
//   | 0x9 |1/7  2/8| 0x3 |<-- rhs frame
//   |     |________|     | 6/5 6 belongs to rhs frame, 5 belongs to lhs frame
//   | 0x4  0x5  0x6  0x7 |
//   | 0x8  0x9  0x1  0x2 |
//   | 0x3  0x4  0x5  0x6 |
//   |____________________|
//
// The intersection is the place we are intresterd in
// clang-format on

TEST(
  FrameIntersections,
  create_correct_iteration_range_when__lhs_frame_is_smaller_than_rhs_with_positive_and_negative_offsets)
{
  constexpr size_t w1        = 2;
  constexpr size_t h1        = 2;
  constexpr size_t w2        = 4;
  constexpr size_t h2        = 6;
  auto             ref_color = piwo::color();

  auto frame1 = piwo::alloc_frame_shared(w1, h1);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(w2, h2);
  if (frame2 == nullptr)
    return;

  const uint8_t default_frame_size1 = std::size(frame_default_2x2);
  const uint8_t default_frame_size2 = std::size(frame_default_4x6);

  ASSERT_TRUE(default_frame_size1 >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size2 >= frame2->width * frame2->height);

  for (int i = 0; i < default_frame_size1; i++)
  {
    frame1->begin()[i] = frame_default_2x2[i];
  }

  for (int i = 0; i < default_frame_size2; i++)
  {
    frame2->begin()[i] = frame_default_4x6[i];
  }

  int64_t     off_x      = 1;
  int64_t     off_y      = 2;
  piwo::color res1_lhs[] = {};
  piwo::color res1_rhs[] = {};
  ASSERT_TRUE(sizeof(res1_lhs) == sizeof(res1_rhs));

  make_iterate_test(
    frame1, frame2, off_x, off_y, res1_lhs, res1_rhs, sizeof(res1_lhs));

  off_x                  = 1;
  off_y                  = 1;
  piwo::color res3_lhs[] = { { 0x8, 0x8, 0x8 } };
  piwo::color res3_rhs[] = { { 0x1, 0x1, 0x1 } };
  ASSERT_TRUE(std::size(res3_lhs) == std::size(res3_rhs));

  make_iterate_test(
    frame1, frame2, off_x, off_y, res3_lhs, res3_rhs, std::size(res3_lhs));

  off_x                  = -1;
  off_y                  = -1;
  piwo::color res4_lhs[] = {
    { 0x5, 0x5, 0x5 }, { 0x6, 0x6, 0x6 }, { 0x7, 0x7, 0x7 }, { 0x8, 0x8, 0x8 }
  };
  piwo::color res4_rhs[] = {
    { 0x6, 0x6, 0x6 }, { 0x7, 0x7, 0x7 }, { 0x1, 0x1, 0x1 }, { 0x2, 0x2, 0x2 }
  };
  ASSERT_TRUE(std::size(res4_lhs) == std::size(res4_rhs));

  make_iterate_test(
    frame1, frame2, off_x, off_y, res4_lhs, res4_rhs, std::size(res4_lhs));
}
