#include "animation.h"
#include "animation_view.h"

#include "finbuf_init.h"

#include <mipc/file.h>

#include <gtest/gtest.h>

#include <istream>
#include <sstream>
#include <string_view>

using namespace std;
using namespace testing;
using namespace piwo;
using namespace mipc;

// DO NOT CHANGE !!!
// TESTS DEPENDS ON THIS CONTENT
constexpr std::string_view test_file = "PIWO_7_FILE\n"
                                       "3 3\n"
                                       "\n"
                                       "36\n"
                                       "7 8 9 \n"
                                       "4 5 6 \n"
                                       "1 2 3 \n"
                                       "\n"
                                       "89\n"
                                       "16 17 18 \n"
                                       "13 14 15 \n"
                                       "10 11 12 \n";

class AnimationViewTest : public ::testing::Test
{
protected:
  AnimationViewTest()          = default;
  virtual ~AnimationViewTest() = default;

  void
  SetUp() override
  {
    finbuf test_finbuf = create_test_finbuf(test_file);

    auto error = anim.build_from_file(test_finbuf);

    if (error.error != animation::error_e::ok)
      FAIL() << "Failed to prepare test animation. Build from file failed.";

    if (anim.frame_count() != 2)
      FAIL() << "Failed to prepare test animation. Incorrect size.";

    if (anim.total_time() != 36 + 89)
      FAIL() << "Failed to prepare test animation. Incorrect total time.";

    size_t i = 1;
    for (auto&& frame : anim)
    {
      for (auto it = frame.begin(); it != frame.end(); ++it)
      {
        if (*it != color::convert(i))
          FAIL() << "Failed to prepare test animation. Unexpected color.";
        ++i;
      }
    }
  }

  piwo::animation anim;
};

#define ANIM_VIEW_ASSERT_ALL_FIELDS(anim, anim_view)                           \
  ASSERT_EQ(anim_view.width(), anim.width());                                  \
  ASSERT_EQ(anim_view.height(), anim.height());                                \
  ASSERT_EQ(anim_view.frame_count(), anim.frame_count());                      \
  ASSERT_EQ(anim_view.total_time(), anim.total_time());                        \
  ASSERT_EQ(anim_view.data(), anim.data());

#define ANIM_VIEW_ASSERT_ALL_METHODS(anim, anim_view)                          \
  ASSERT_EQ(anim_view.frame_offset(), anim.frame_offset());                    \
  ASSERT_EQ(anim_view.begin(), anim.begin());                                  \
  ASSERT_EQ(anim_view.end(), anim.end());

void
ANIM_VIEW_ASSERT_EACH_PIXEL(animation& anim, animation_view& anim_view)
{
  // Range: for each Frame in animation
  auto anit     = anim.begin();
  auto anit_end = anim.end();

  // Range:: for each frame in animation_view
  auto avit     = anim_view.begin();
  auto avit_end = anim_view.end();

  while (anit != anit_end && avit != avit_end)
  {

    // Range: for each pixel in animation frame
    auto anpixel_it    = anit->begin();
    auto anpixel_itend = anit->end();

    // Range: for each pixel in animation_view frame
    auto avpixel_it    = avit->begin();
    auto avpixel_itend = avit->end();

    while (anpixel_it != anpixel_itend && avpixel_it != avpixel_itend)
    {
      ASSERT_EQ(*anpixel_it, *avpixel_it);

      ++anpixel_it;
      ++avpixel_it;
    }

    // Make sure both ended (they are the same size)
    ASSERT_TRUE(anpixel_it == anpixel_itend && avpixel_it == avpixel_itend);

    ++anit;
    ++avit;
  }

  // Make sure both ended (they are the same size)
  ASSERT_TRUE(anit == anit_end && avit == avit_end);
}

#define ANIM_VIEW_ASSERT_DEEP_COMPARE(anim, anim_view)                         \
  ANIM_VIEW_ASSERT_ALL_FIELDS(anim, av);                                       \
  ANIM_VIEW_ASSERT_ALL_METHODS(anim, av);                                      \
  ANIM_VIEW_ASSERT_EACH_PIXEL(anim, av);

TEST_F(AnimationViewTest, from_const_ref_animation_constructible)
{
  piwo::animation_view av(this->anim);

  ANIM_VIEW_ASSERT_DEEP_COMPARE(this->anim, av);
}

TEST_F(AnimationViewTest, from_const_ref_animation_assignable)
{
  piwo::animation_view av;
  av = this->anim;

  ANIM_VIEW_ASSERT_DEEP_COMPARE(this->anim, av);
}
