#include <gtest/gtest.h>

#include "common_intersection_operators.h"
#include "frames_intersection.h"

namespace
{
// clang-format off
const piwo::color frame_default_2x2[] = { {0x6, 0x6, 0x6}, {0x6, 0x6, 0x6},
                                          {0x6, 0x6, 0x6}, {0x6, 0x6, 0x6}};
// clang-format on

void
validate_frames(piwo::frames_intersection& frames,
                const piwo::color*         result_lhs,
                const piwo::color*         result_rhs,
                uint8_t                    result_size)
{
  size_t frame_num = 0;

  for (auto frame_pair : frames)
  {
    ASSERT_TRUE(frame_num < result_size);
    EXPECT_EQ(frame_pair.first.convert<uint32_t>(),
              result_lhs[frame_num].convert<uint32_t>());
    EXPECT_EQ(frame_pair.second.convert<uint32_t>(),
              result_rhs[frame_num].convert<uint32_t>());
    frame_num++;
  }
  EXPECT_EQ(result_size, frame_num);
}

void
validate_frames(piwo::frame&       f_orgin1,
                piwo::frame&       f_orgin2,
                const piwo::color* result_lhs,
                const piwo::color* result_rhs,
                uint8_t            result_size)
{
  int frame_num = 0;
  for (auto c : f_orgin1)
  {
    ASSERT_TRUE(frame_num < result_size);
    EXPECT_EQ(c.convert<uint32_t>(), result_lhs[frame_num].convert<uint32_t>());
    frame_num++;
  }

  frame_num = 0;
  for (auto c : f_orgin2)
  {
    ASSERT_TRUE(frame_num < result_size);
    EXPECT_EQ(c.convert<uint32_t>(), result_rhs[frame_num].convert<uint32_t>());
    frame_num++;
  }
}

void
validate_frames(piwo::frames_intersection& frames,
                const piwo::color*         result,
                uint8_t                    result_size)
{
  validate_frames(frames, result, result, result_size);
}

static void
frame_mul_op(piwo::frames_intersection fi, double x)
{
  for (auto [p1, p2] : fi)
  {
    p1.r *= x;
    p1.g *= x;
    p1.b *= x;
    p2.r *= x;
    p2.g *= x;
    p2.b *= x;
  }
}

static void
frame_mul_op(piwo::frames_intersection_unsafe fi, double x)
{
  for (auto [p1, p2] : fi)
  {
    p1.r *= x;
    p1.g *= x;
    p1.b *= x;
    p2.r *= x;
    p2.g *= x;
    p2.b *= x;
  }
}

void
frame_mul_by_half_op(piwo::frames_intersection fi)
{
  frame_mul_op(fi, 0.5);
};

void
frame_mul_by_half_op(piwo::frames_intersection_unsafe fi)
{
  frame_mul_op(fi, 0.5);
};

} // namespace

TEST(FrameEffects, rhs_frame_was_correctly_added_to_lhs_frame)
{
  constexpr size_t width  = 2;
  constexpr size_t height = 2;

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;
  constexpr uint8_t default_frame_size = std::size(frame_default_2x2);

  ASSERT_TRUE(default_frame_size >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size >= frame2->width * frame2->height);
  for (int i = 0; i < default_frame_size; i++)
  {
    frame1->begin()[i] = frame_default_2x2[i];
    frame2->begin()[i] = frame_default_2x2[i];
  }
  int64_t                   off_x = 0;
  int64_t                   off_y = 0;
  piwo::frames_intersection frames(frame1, frame2, off_x, off_y);
  // add second frame to first
  frame_add_op(frames);
  const piwo::color res1_lhs[] = {
    { 0xc, 0xc, 0xc }, { 0xc, 0xc, 0xc }, { 0xc, 0xc, 0xc }, { 0xc, 0xc, 0xc }
  };
  const piwo::color res1_rhs[] = {
    { 0x6, 0x6, 0x6 }, { 0x6, 0x6, 0x6 }, { 0x6, 0x6, 0x6 }, { 0x6, 0x6, 0x6 }
  };
  ASSERT_TRUE(std::size(res1_lhs) == std::size(res1_rhs));

  validate_frames(frames, res1_lhs, res1_rhs, std::size(res1_lhs));
}

TEST(FrameEffects, lhs_frame_was_correctly_substracted_from_rhs_frame)
{
  constexpr size_t width  = 2;
  constexpr size_t height = 2;

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;
  constexpr uint8_t default_frame_size = std::size(frame_default_2x2);

  ASSERT_TRUE(default_frame_size >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size >= frame2->width * frame2->height);
  for (int i = 0; i < default_frame_size; i++)
  {
    frame1->begin()[i] = frame_default_2x2[i];
    frame2->begin()[i] = frame_default_2x2[i];
  }
  int64_t                   off_x = 0;
  int64_t                   off_y = 0;
  piwo::frames_intersection frames(frame1, frame2, off_x, off_y);

  // substract first frame from second
  frame_sub_op(frames);
  const piwo::color res1_lhs[] = {
    { 0x0, 0x0, 0x0 }, { 0x0, 0x0, 0x0 }, { 0x0, 0x0, 0x0 }, { 0x0, 0x0, 0x0 }
  };
  const piwo::color res1_rhs[] = {
    { 0x6, 0x6, 0x6 }, { 0x6, 0x6, 0x6 }, { 0x6, 0x6, 0x6 }, { 0x6, 0x6, 0x6 }
  };
  ASSERT_TRUE(std::size(res1_lhs) == std::size(res1_rhs));
  validate_frames(frames, res1_lhs, res1_rhs, std::size(res1_lhs));
}

TEST(FrameEffects, both_frames_were_corectly_multiplied_by_half)
{
  constexpr size_t width  = 2;
  constexpr size_t height = 2;

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;
  constexpr uint8_t default_frame_size = std::size(frame_default_2x2);

  ASSERT_TRUE(default_frame_size >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size >= frame2->width * frame2->height);
  for (int i = 0; i < default_frame_size; i++)
  {
    frame1->begin()[i] = frame_default_2x2[i];
    frame2->begin()[i] = frame_default_2x2[i];
  }
  int64_t                   off_x = 0;
  int64_t                   off_y = 0;
  piwo::frames_intersection frames(frame1, frame2, off_x, off_y);

  // multiply both frames by 0.5
  frame_mul_by_half_op(frames);
  const piwo::color res1_lhs[] = {
    { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }
  };
  const piwo::color res1_rhs[] = {
    { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }
  };
  ASSERT_TRUE(std::size(res1_lhs) == std::size(res1_rhs));

  validate_frames(frames, res1_lhs, res1_rhs, std::size(res1_lhs));
}

TEST(FrameEffects,
     correctly_perform_adding_substracting_and_multiplying_by_half_in_pipeline)
{
  std::vector<piwo::frames_intersection_operator> operations = {
    piwo::frame_add_op, piwo::frame_sub_op, frame_mul_by_half_op
  };

  constexpr size_t width     = 2;
  constexpr size_t height    = 2;
  auto             ref_color = piwo::color();

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;
  constexpr uint8_t default_frame_size = std::size(frame_default_2x2);

  ASSERT_TRUE(default_frame_size >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size >= frame2->width * frame2->height);
  for (int i = 0; i < default_frame_size; i++)
  {
    frame1->begin()[i] = frame_default_2x2[i];
    frame2->begin()[i] = frame_default_2x2[i];
  }
  int64_t                   off_x = 0;
  int64_t                   off_y = 0;
  piwo::frames_intersection frames(frame1, frame2, off_x, off_y);
  for (auto* op : operations)
  {
    std::invoke(op, frames);
  }
  const piwo::color res1_lhs[] = {
    { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }
  };
  const piwo::color res1_rhs[] = {
    { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }
  };
  ASSERT_TRUE(std::size(res1_lhs) == std::size(res1_rhs));

  validate_frames(frames, res1_lhs, res1_rhs, std::size(res1_lhs));
}

TEST(FrameEffectsUnsafe, rhs_frame_was_correctly_added_to_lhs_frame)
{
  constexpr size_t width  = 2;
  constexpr size_t height = 2;

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;
  constexpr uint8_t default_frame_size = std::size(frame_default_2x2);

  ASSERT_TRUE(default_frame_size >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size >= frame2->width * frame2->height);
  for (int i = 0; i < default_frame_size; i++)
  {
    frame1->begin()[i] = frame_default_2x2[i];
    frame2->begin()[i] = frame_default_2x2[i];
  }
  int64_t off_x = 0;
  int64_t off_y = 0;

  // add second frame to first
  piwo::unsafe_intersection_apply(
    *frame1, *frame2, off_x, off_y, piwo::frame_add_op);

  const piwo::color res1_lhs[] = {
    { 0xc, 0xc, 0xc }, { 0xc, 0xc, 0xc }, { 0xc, 0xc, 0xc }, { 0xc, 0xc, 0xc }
  };
  const piwo::color res1_rhs[] = {
    { 0x6, 0x6, 0x6 }, { 0x6, 0x6, 0x6 }, { 0x6, 0x6, 0x6 }, { 0x6, 0x6, 0x6 }
  };
  ASSERT_TRUE(std::size(res1_lhs) == std::size(res1_rhs));

  validate_frames(*frame1, *frame2, res1_lhs, res1_rhs, std::size(res1_lhs));
}

TEST(FrameEffectsUnsafe,
     correctly_perform_adding_substracting_and_multiplying_by_half_in_pipeline)
{
  std::vector<piwo::frames_intersection_operator_unsafe> operations = {
    piwo::frame_add_op, piwo::frame_sub_op, frame_mul_by_half_op
  };
  constexpr size_t width  = 2;
  constexpr size_t height = 2;

  auto frame1 = piwo::alloc_frame_shared(width, height);
  if (frame1 == nullptr)
    return;
  auto frame2 = piwo::alloc_frame_shared(width, height);
  if (frame2 == nullptr)
    return;
  constexpr uint8_t default_frame_size = std::size(frame_default_2x2);

  ASSERT_TRUE(default_frame_size >= frame1->width * frame1->height);
  ASSERT_TRUE(default_frame_size >= frame2->width * frame2->height);
  for (int i = 0; i < default_frame_size; i++)
  {
    frame1->begin()[i] = frame_default_2x2[i];
    frame2->begin()[i] = frame_default_2x2[i];
  }
  int64_t off_x = 0;
  int64_t off_y = 0;

  for (auto* op : operations)
  {
    piwo::unsafe_intersection_apply(*frame1, *frame2, off_x, off_y, op);
  }

  const piwo::color res1_lhs[] = {
    { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }
  };
  const piwo::color res1_rhs[] = {
    { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }, { 0x3, 0x3, 0x3 }
  };
  ASSERT_TRUE(std::size(res1_lhs) == std::size(res1_rhs));

  validate_frames(*frame1, *frame2, res1_lhs, res1_rhs, std::size(res1_lhs));
}
