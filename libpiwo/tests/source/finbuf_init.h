#pragma once

#include <mipc/file.h>
#include <string_view>

// Returned buffer in the string_view has to be freed manually.
// This should be handled by finbuf itself.
// It is strongly advised to enable sanitizer at the time of testing
// to ensure this behavior from finbuf since we are using unsafe
// initialization.
inline std::string_view
copy_test_file(std::string_view filebuf)
{
  char*  buf     = new char[filebuf.size()];
  size_t bufsize = filebuf.size();

  std::copy(std::begin(filebuf), std::end(filebuf), buf);

  return { buf, bufsize };
}

inline mipc::finbuf_unsafe_init
create_test_finbuf(std::string_view buffer)
{
  auto test_file = copy_test_file(buffer);

  const auto buf  = test_file.data();
  const auto size = test_file.size();

  return mipc::finbuf_unsafe_init(size, buf);
}
