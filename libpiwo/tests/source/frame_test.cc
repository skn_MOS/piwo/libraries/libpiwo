#include <gtest/gtest.h>

#include "frame.h"

TEST(Frame, fill_whole_frame_with_color_successfully)
{
  auto        f            = piwo::alloc_frame(10, 12);
  piwo::color tested_color = { 240, 120, 57 };

  f->fill(tested_color);
  for (const auto& c : *f)
  {
    EXPECT_EQ(c.convert<uint32_t>(), tested_color.convert<uint32_t>());
  }
}
