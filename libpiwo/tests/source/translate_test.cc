#include "animation.h"

#include "finbuf_init.h"

#include <mipc/file.h>

#include <gtest/gtest.h>

#include <istream>
#include <sstream>
#include <string_view>

using namespace std;
using namespace testing;
using namespace piwo;
using namespace mipc;

class TranslateTest : public ::testing::Test
{
protected:
  TranslateTest()          = default;
  virtual ~TranslateTest() = default;

  piwo::animation anim;
};

TEST_F(TranslateTest,
       translate_to_ms50_exact_works_with_delays_that_are_multiples_of_50)
{
  auto test_file = "PIWO_7_FILE\n"
                   "3 3\n"
                   "\n"
                   "150\n"
                   "7 8 9 \n"
                   "4 5 6 \n"
                   "1 2 3 \n"
                   "\n"
                   "250\n"
                   "16 17 18 \n"
                   "13 14 15 \n"
                   "10 11 12 \n"
                   "\n"
                   "50\n"
                   "7 8 9 \n"
                   "4 5 6 \n"
                   "1 2 3 \n"
                   "\n"
                   "750\n"
                   "16 17 18 \n"
                   "13 14 15 \n"
                   "10 11 12 \n"
                   "\n"
                   "100\n"
                   "7 8 9 \n"
                   "4 5 6 \n"
                   "1 2 3 \n"
                   "\n"
                   "350\n"
                   "16 17 18 \n"
                   "13 14 15 \n"
                   "10 11 12 \n";

  size_t expected_total_delay = 150 + 250 + 50 + 750 + 100 + 350;
  size_t expected_frame_count = expected_total_delay / 50;

  size_t reminder = expected_total_delay % 50;
  if (reminder != 0)
    FAIL() << "Setup failure! Expected total frame time to be divisible by 50";

  finbuf test_finbuf = create_test_finbuf(test_file);

  using parse_option = animation::parse_option;
  parse_option parse_op;
  parse_op.translate = animation::parse_option::translate_type::ms50_exact;

  auto error = anim.build_from_file(test_finbuf, parse_op);

  ASSERT_EQ(error.error, animation::error_e::ok);
  ASSERT_EQ(anim.total_time(), expected_total_delay);
  ASSERT_EQ(anim.frame_count(), expected_frame_count);
}

TEST_F(TranslateTest,
       translate_to_ms50_exact_fails_with_delays_that_are_not_multiples_of_50)
{
  auto test_file = "PIWO_7_FILE\n"
                   "3 3\n"
                   "\n"
                   "150\n"
                   "7 8 9 \n"
                   "4 5 6 \n"
                   "1 2 3 \n"
                   "\n"
                   "250\n"
                   "16 17 18 \n"
                   "13 14 15 \n"
                   "10 11 12 \n"
                   "\n"
                   "50\n"
                   "7 8 9 \n"
                   "4 5 6 \n"
                   "1 2 3 \n"
                   "\n"
                   "725\n"
                   "16 17 18 \n"
                   "13 14 15 \n"
                   "10 11 12 \n"
                   "\n"
                   "100\n"
                   "7 8 9 \n"
                   "4 5 6 \n"
                   "1 2 3 \n"
                   "\n"
                   "350\n"
                   "16 17 18 \n"
                   "13 14 15 \n"
                   "10 11 12 \n";

  size_t expected_total_delay = 150 + 250 + 50 + 725 + 100 + 350;
  size_t expected_frame_count = expected_total_delay / 50;

  size_t reminder = expected_total_delay % 50;
  if (reminder != 25)
    FAIL() << "Setup failure! Expected total frame delay reminder to be 25.";

  finbuf test_finbuf = create_test_finbuf(test_file);

  using parse_option = animation::parse_option;
  parse_option parse_op;
  parse_op.translate = animation::parse_option::translate_type::ms50_exact;

  auto error = anim.build_from_file(test_finbuf, parse_op);

  ASSERT_EQ(error.error, animation::error_e::bad_parse);
  ASSERT_EQ(error.line_number, 19);
  ASSERT_EQ(anim.total_time(), 0);
  ASSERT_EQ(anim.frame_count(), 0);
}
