#include "animation.h"

#include "finbuf_init.h"

#include <mipc/file.h>

#include <gtest/gtest.h>

#include <istream>
#include <sstream>
#include <string_view>

using namespace std;
using namespace testing;
using namespace piwo;
using namespace mipc;

class ParseTest : public ::testing::Test
{
protected:
  ParseTest()          = default;
  virtual ~ParseTest() = default;

  piwo::animation anim;
};

TEST_F(ParseTest, verify_signature_successfull_with_piwo7_signature)
{
  auto test_file = "PIWO_7_FILE\n"
                   "2 2\n";
  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);
}

TEST_F(ParseTest, verify_signature_fails_with_bad_signature)
{
  auto test_file = "PIWO_7_FIL\n"
                   "2 2\n";

  finbuf test_finbuf = create_test_finbuf(test_file);
  auto   error       = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::bad_signature);
  ASSERT_EQ(error.line_number, 1);
}

TEST_F(ParseTest,
       build_from_file_pixels_parsed_ok_when_row_ends_with_space_and_newline)
{
  auto test_file = "PIWO_7_FILE\n"
                   "3 3\n"
                   "\n"
                   "50\n"
                   "7 8 9 \n"
                   "4 5 6 \n"
                   "1 2 3 \n"
                   "\n"
                   "50\n"
                   "16 17 18 \n"
                   "13 14 15 \n"
                   "10 11 12 \n";

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);

  ASSERT_EQ(error.error, animation::error_e::ok);
  ASSERT_EQ(anim.frame_count(), 2);
  ASSERT_EQ(anim.total_time(), 50 + 50);

  size_t i = 1;
  for (auto&& frame : anim)
  {
    for (auto it = frame.begin(); it != frame.end(); ++it)
    {
      ASSERT_EQ(*it, color::convert(i));
      ++i;
    }
  }
}

TEST_F(
  ParseTest,
  build_from_file_pixels_parsed_ok_when_row_ends_without_space_before_newline)
{
  auto test_file{ "PIWO_7_FILE\n"
                  "3 3\n"
                  "\n"
                  "123\n"
                  "7 8 9\n"
                  "4 5 6\n"
                  "1 2 3\n"
                  "\n"
                  "123\n"
                  "16 17 18\n"
                  "13 14 15\n"
                  "10 11 12\n" };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);
  ASSERT_EQ(anim.frame_count(), 2);
  ASSERT_EQ(anim.total_time(), 246);

  size_t i = 1;
  for (auto&& frame : anim)
  {
    for (auto it = frame.begin(); it != frame.end(); ++it)
    {
      ASSERT_EQ(*it, color::convert(i));
      ++i;
    }
  }
}

TEST_F(ParseTest, build_from_file_delays_parsed_ok)
{
  std::string frame{ "1 2 3 \n"
                     "0 0 0 \n"
                     "0 0 0 \n" };

  std::stringstream ss;

  // Initialization it with string below causes rdbuf
  // to return empty string for some reason.
  // I dont understand it atm. TODO(holz) perhaps fix it?
  ss << "PIWO_7_FILE\n"
        "3 3\n";

  size_t                          delay       = 50;
  constexpr size_t                frame_count = 10;
  constexpr size_t                delay_delta = 50;
  std::array<size_t, frame_count> delays;

  for (auto i = 0ull; i < frame_count; ++i)
  {
    // cache delays for later verification
    delays[i] = delay;

    ss << "\n" << delay << "\n" << frame;
    delay += delay_delta;
  }

  finbuf          test_finbuf = create_test_finbuf(ss.str());
  piwo::animation anim;
  auto            status = anim.build_from_file(test_finbuf);

  ASSERT_EQ(status.error, animation::error_e::ok);
  ASSERT_EQ(anim.frame_count(), frame_count);

  for (auto i = 0ull; i < anim.frame_count(); ++i)
    ASSERT_EQ(anim[i].delay, delays[i]);
}

TEST_F(ParseTest, build_from_file_fails_on_corrupted_size_width_character)
{
  auto test_file{
    "PIWO_7_FILE\n"
    "a 3\n" // Corrupted: character
    "\n"
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
    "\n"
    "50\n"
    "10 11 12\n"
    "13 14 15\n"
    "16 17 18\n"
  };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::bad_parse);
  ASSERT_EQ(error.line_number, 2);
}

TEST_F(ParseTest, build_from_file_fails_on_corrupted_size_height_exceed_max_value)
{
  auto test_file{
    "PIWO_7_FILE\n"
    "3 16777216\n" // Corrupted: color::MAX_VALUE + 1
    "\n"
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
    "\n"
    "50\n"
    "10 11 12\n"
    "13 14 15\n"
    "16 17 18\n"
  };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::bad_parse);
  ASSERT_EQ(error.line_number, 2);
}

TEST_F(ParseTest, build_from_file_fails_on_corrupted_size_additional_number_after_animation_size)
{
  auto test_file{
    "PIWO_7_FILE\n"
    "3 3 3\n" // Corrupted: additional number
    "\n"
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
    "\n"
    "50\n"
    "10 11 12\n"
    "13 14 15\n"
    "16 17 18\n"
  };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::bad_parse);
  ASSERT_EQ(error.line_number, 2);
}

TEST_F(ParseTest, build_from_file_fails_on_corrupted_size_missing_height)
{
  auto test_file{
    "PIWO_7_FILE\n"
    "3   \n" // Corrupted: missing height value
    "\n"
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
    "\n"
    "50\n"
    "10 11 12\n"
    "13 14 15\n"
    "16 17 18\n"
  };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::bad_parse);
  ASSERT_EQ(error.line_number, 2);
}

TEST_F(ParseTest, build_from_file_fails_on_corrupted_frame)
{
  auto test_file{
    "PIWO_7_FILE\n"
    "3 3\n"
    "\n"
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
    "\n"
    "50\n"
    "10 11 12\n"
    "13 14 15\n"
    "16 \n" // Corrupted: missing values
    "\n"
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
  };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::bad_parse);
  ASSERT_EQ(error.line_number, 12);
}

TEST_F(ParseTest, build_from_file_fails_on_corrupted_lines_missing_empty_line_between_frames)
{
  auto test_file{
    "PIWO_7_FILE\n"
    "3 3\n"
    "\n"
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
    "\n"
    "50\n"
    "10 11 12\n"
    "13 14 15\n"
    "16 17 18\n" // Corrupted: missing empty line between frames
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
  };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::bad_parse);
  ASSERT_EQ(error.line_number, 13);
}

TEST_F(ParseTest, build_from_file_fails_on_corrupted_lines_corrupted_empty_line_between_frames)
{
  auto test_file{
    "PIWO_7_FILE\n"
    "3 3\n"
    "\n"
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
    "\n"
    "50\n"
    "10 11 12\n"
    "13 14 15\n"
    "16 17 18\n"
    "123\n"  // Corrupted: non-empty line
    "50\n"
    "1 2 3\n"
    "4 5 6\n"
    "7 8 9\n"
  };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error, animation::error_e::bad_parse);
  ASSERT_EQ(error.line_number, 13);
}

TEST_F(ParseTest, build_from_file_properly_parses_dump_produced_by_itself)
{
  auto   test_file{ "PIWO_7_FILE\n"
                  "3 3\n"
                  "\n"
                  "49\n"
                  "1 2 3\n"
                  "4 5 6\n"
                  "7 8 9\n"
                  "\n"
                  "36\n"
                  "10 11 12\n"
                  "13 14 15\n"
                  "16 17 18\n" };
  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  auto frame_count = anim.frame_count();
  auto width       = anim.width();
  auto height      = anim.height();
  auto total_time  = anim.total_time();

  std::stringstream os;
  os << anim;

  test_finbuf = create_test_finbuf(os.str());
  error       = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  ASSERT_EQ(frame_count, anim.frame_count());
  ASSERT_EQ(width, anim.width());
  ASSERT_EQ(height, anim.height());
  ASSERT_EQ(total_time, anim.total_time());
}

TEST_F(
  ParseTest,
  build_from_file_properly_parses_files_ending_with_multiple_whitespace_characters1)
{
  auto   test_file{ "PIWO_7_FILE\n"
                  "3 3\n"
                  "\n"
                  "49\n"
                  "1 2 3\n"
                  "4 5 6\n"
                  "7 8 9\n"
                  "\n"
                  "36\n"
                  "10 11 12\n"
                  "13 14 15\n"
                  "16 17 18\n"
                  "\n" };
  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  auto frame_count = anim.frame_count();
  auto width       = anim.width();
  auto height      = anim.height();
  auto total_time  = anim.total_time();

  std::stringstream os;
  os << anim;

  test_finbuf = create_test_finbuf(os.str());
  error       = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  ASSERT_EQ(frame_count, anim.frame_count());
  ASSERT_EQ(width, anim.width());
  ASSERT_EQ(height, anim.height());
  ASSERT_EQ(total_time, anim.total_time());
}

TEST_F(
  ParseTest,
  build_from_file_properly_parses_files_ending_with_multiple_whitespace_characters2)
{
  auto   test_file{ "PIWO_7_FILE\n"
                  "3 3\n"
                  "\n"
                  "49\n"
                  "1 2 3\n"
                  "4 5 6\n"
                  "7 8 9\n"
                  "\n"
                  "36\n"
                  "10 11 12\n"
                  "13 14 15\n"
                  "16 17 18\n"
                  " " };
  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  auto frame_count = anim.frame_count();
  auto width       = anim.width();
  auto height      = anim.height();
  auto total_time  = anim.total_time();

  std::stringstream os;
  os << anim;

  test_finbuf = create_test_finbuf(os.str());
  error       = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  ASSERT_EQ(frame_count, anim.frame_count());
  ASSERT_EQ(width, anim.width());
  ASSERT_EQ(height, anim.height());
  ASSERT_EQ(total_time, anim.total_time());
}

TEST_F(
  ParseTest,
  build_from_file_properly_parses_files_ending_with_multiple_whitespace_characters3)
{
  auto   test_file{ "PIWO_7_FILE\n"
                  "3 3\n"
                  "\n"
                  "49\n"
                  "1 2 3\n"
                  "4 5 6\n"
                  "7 8 9\n"
                  "\n"
                  "36\n"
                  "10 11 12\n"
                  "13 14 15\n"
                  "16 17 18\n"
                  "  \n " };
  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  auto frame_count = anim.frame_count();
  auto width       = anim.width();
  auto height      = anim.height();
  auto total_time  = anim.total_time();

  std::stringstream os;
  os << anim;

  test_finbuf = create_test_finbuf(os.str());
  error       = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  ASSERT_EQ(frame_count, anim.frame_count());
  ASSERT_EQ(width, anim.width());
  ASSERT_EQ(height, anim.height());
  ASSERT_EQ(total_time, anim.total_time());
}

TEST_F(ParseTest,
       build_from_file_properly_parses_dump_produced_by_itself_rectangular)
{

  auto test_file{
    "PIWO_7_FILE\n"
    "12 10\n"
    "\n"
    "50\n"
    "0 0 0 0 0 0 0 0 0 0 0 0 \n"
    "0 0 0 0 8344832 8344832 8344832 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \n"
    "0 0 0 16765862 23167 23167 23167 16765862 0 0 0 0 \n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \n"
    "\n"
    "50\n"
    "0 0 0 0 0 0 0 0 0 0 0 0 \n"
    "0 0 0 0 8344832 8344832 8344832 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \n"
    "0 0 0 16765862 23167 23167 23167 16765862 0 0 0 0 \n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \n"
    "\n"
    "50\n"
    "0 0 0 0 0 0 0 0 0 0 0 0 \n"
    "0 0 0 0 8344832 8344832 8344832 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \n"
    "0 0 0 16765862 23167 23167 23167 16765862 0 0 0 0 \n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \n"
    "\n"
    "50\n"
    "0 0 0 0 0 0 0 0 0 0 0 0 \n"
    "0 0 0 0 8344832 8344832 8344832 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \n"
    "0 0 0 16765862 23167 23167 23167 16765862 0 0 0 0 \n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \n"
  };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  auto frame_count = anim.frame_count();
  auto width       = anim.width();
  auto height      = anim.height();
  auto total_time  = anim.total_time();

  std::stringstream os;
  os << anim;

  test_finbuf = create_test_finbuf(os.str());
  error       = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  ASSERT_EQ(frame_count, anim.frame_count());
  ASSERT_EQ(width, anim.width());
  ASSERT_EQ(height, anim.height());
  ASSERT_EQ(total_time, anim.total_time());
}
TEST_F(
  ParseTest,
  build_from_file_properly_parses_dump_produced_by_itself_windows_style_endline)
{

  auto test_file{
    "PIWO_7_FILE\r\n"
    "12 10\r\n"
    "\r\n"
    "50\r\n"
    "0 0 0 0 0 0 0 0 0 0 0 0 \r\n"
    "0 0 0 0 8344832 8344832 8344832 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \r\n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \r\n"
    "0 0 0 16765862 23167 23167 23167 16765862 0 0 0 0 \r\n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \r\n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \r\n"
    "\r\n"
    "50\r\n"
    "0 0 0 0 0 0 0 0 0 0 0 0 \r\n"
    "0 0 0 0 8344832 8344832 8344832 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \r\n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \r\n"
    "0 0 0 16765862 23167 23167 23167 16765862 0 0 0 0 \r\n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \r\n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \r\n"
    "\r\n"
    "50\r\n"
    "0 0 0 0 0 0 0 0 0 0 0 0 \r\n"
    "0 0 0 0 8344832 8344832 8344832 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \r\n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \r\n"
    "0 0 0 16765862 23167 23167 23167 16765862 0 0 0 0 \r\n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \r\n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \r\n"
    "\r\n"
    "50\r\n"
    "0 0 0 0 0 0 0 0 0 0 0 0 \r\n"
    "0 0 0 0 8344832 8344832 8344832 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 0 16765862 16765862 16765862 0 0 0 0 0 \r\n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \r\n"
    "0 0 0 23167 23167 23167 23167 23167 0 0 0 0 \r\n"
    "0 0 0 16765862 23167 23167 23167 16765862 0 0 0 0 \r\n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \r\n"
    "0 8864512 8864512 8864512 8864512 8864512 8864512 8864512 8864512 "
    "8864512 8864512 0 \r\n"
  };

  finbuf test_finbuf = create_test_finbuf(test_file);

  auto error = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  auto frame_count = anim.frame_count();
  auto width       = anim.width();
  auto height      = anim.height();
  auto total_time  = anim.total_time();

  std::stringstream os;
  os << anim;

  test_finbuf = create_test_finbuf(os.str());
  error       = anim.build_from_file(test_finbuf);
  ASSERT_EQ(error.error, animation::error_e::ok);

  ASSERT_EQ(frame_count, anim.frame_count());
  ASSERT_EQ(width, anim.width());
  ASSERT_EQ(height, anim.height());
  ASSERT_EQ(total_time, anim.total_time());
}
