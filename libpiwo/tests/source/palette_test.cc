#include "color_palette.h"
#include <array>
#include <filesystem>
#include <fstream>
#include <gtest/gtest.h>
#include <string_view>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

namespace fs = std::filesystem;

constexpr std::string_view test_file = "PIWO_7_FILE\n"
                                       "2 2\n"
                                       "\n"
                                       "50\n"
                                       "1 2 \n"
                                       "3 4 \n";

template<size_t N>
static consteval size_t
find_index(const char (&array)[N], const char character)
{
  auto it = std::find(std::begin(array), std::end(array), character);

  if (it == std::end(array))
    throw "Character not found";

  return std::distance(std::begin(array), it);
}

constexpr char dir[] = "/tmp/test_dirXXXXXX";

constexpr size_t dir_name_length             = std::size(dir);
constexpr char   filename_colorpaletteTest[] = "/anim.piwo7";

struct colorpaletteTest : public ::testing::Test
{
  size_t files_read;

  // - 1 for needless '\0'
  static constexpr size_t file_path_length = dir_name_length + std::size(filename_colorpaletteTest) - 1;

  void
  SetUp() override
  {
    ASSERT_EQ(filename_colorpaletteTest[0], '/');

    char dir_name[dir_name_length];
    std::copy_n(dir, dir_name_length, dir_name);

    const char* const dir_name_ptr = mkdtemp(dir_name);
    ASSERT_NE(dir_name_ptr, nullptr);

    char         file_path[file_path_length];
    const size_t bytes_written = snprintf(file_path, file_path_length, "%s%s",
                                           dir_name, filename_colorpaletteTest);
    ASSERT_EQ(bytes_written, file_path_length - 1);

    std::ofstream file(file_path);
    file << test_file;
    file.close();

    files_read = _palette.extend(dir_name, 1);
    fs::remove_all(dir_name);
  }

  const piwo::color_palette&
  get_palette()
  {
    return _palette;
  }

private:
  piwo::color_palette _palette;
};

TEST_F(colorpaletteTest, extendShouldMakepaletteOfProperSizeFromAnimations)
{
  if (files_read != 1)
    FAIL() << "Failed to prepare test animation. Build from file failed.";

  const piwo::color_palette& palette_ref = get_palette();
  ASSERT_EQ(palette_ref.size(), 4);
}

TEST_F(colorpaletteTest, paletteIterationShouldBePossible)
{
  const piwo::color_palette& palette_ref = get_palette();
  const std::array           colors_packed{ 1, 2, 3, 4 };
  size_t                     index = 0;
  for (const auto& [color, number] : palette_ref)
  {
    ASSERT_EQ(color.packed(), colors_packed[index]);
    ASSERT_EQ(number, index++);
  }
}

TEST_F(colorpaletteTest, getpaletteNumShouldReturnIndexNumberOfColorInpalette)
{
  const piwo::color_palette& palette_ref = get_palette();
  ASSERT_EQ(palette_ref.get_palette_num(piwo::color{ 1 }).value(), 0);
  ASSERT_EQ(palette_ref.get_palette_num(piwo::color{ 2 }).value(), 1);
  ASSERT_EQ(palette_ref.get_palette_num(piwo::color{ 3 }).value(), 2);
  ASSERT_EQ(palette_ref.get_palette_num(piwo::color{ 4 }).value(), 3);

  ASSERT_FALSE(palette_ref.get_palette_num(piwo::color{ 5 }));
}

TEST_F(colorpaletteTest, getpaletteColorShouldReturnColorFrompalette)
{
  const piwo::color_palette& palette_ref = get_palette();
  ASSERT_EQ(palette_ref.get_palette_color(0).value(), piwo::color{ 1 });
  ASSERT_EQ(palette_ref.get_palette_color(1).value(), piwo::color{ 2 });
  ASSERT_EQ(palette_ref.get_palette_color(2).value(), piwo::color{ 3 });
  ASSERT_EQ(palette_ref.get_palette_color(3).value(), piwo::color{ 4 });

  ASSERT_FALSE(palette_ref.get_palette_color(4));
}

constexpr size_t SHIFT_RED   = 16;
constexpr size_t SHIFT_GREEN = 8;

constexpr std::string_view file_bg = "PIWO_7_FILE\n"
                                     "2 2\n"
                                     "\n"
                                     "50\n"
                                     "10 13 \n"
                                     "25 36 \n"
                                     "\n"
                                     "50\n"
                                     "2560 3329 \n"
                                     "6401 9217 \n";

constexpr std::string_view file_r = "PIWO_7_FILE\n"
                                    "2 2\n"
                                    "\n"
                                    "50\n"
                                    "655361 851969 \n"
                                    "1638401 2359297 \n";

constexpr char filename_paletteRoundTest[] = "/anim0.piwo7";

struct paletteRoundTest : public ::testing::Test
{
  piwo::animation::error_t error_inf;

  // - 1 for needless '\0'
  static constexpr size_t file_path_length  = dir_name_length + std::size(filename_paletteRoundTest) - 1;

  // - 1, for needless '\0'
  static constexpr size_t file_number_index = dir_name_length + find_index(filename_paletteRoundTest, '0') - 1;

  void
  SetUp() override
  {
    ASSERT_EQ(filename_paletteRoundTest[0], '/');

    char dir_name[dir_name_length];
    std::copy_n(dir, dir_name_length, dir_name);

    const char* const dir_name_ptr = mkdtemp(dir_name);
    ASSERT_NE(dir_name_ptr, nullptr);

    std::vector<piwo::animation> animations;
    piwo::animation              anim;

    char         file_path[file_path_length];
    const size_t bytes_written = snprintf(file_path, file_path_length, "%s%s",
                                           dir_name, filename_paletteRoundTest);
    ASSERT_EQ(bytes_written, file_path_length - 1);

    std::ofstream file1(file_path);
    file1 << file_bg;
    file1.close();

    error_inf = anim.build_from_file(file_path);
    animations.push_back(anim);

    file_path[file_number_index] = '1';

    std::ofstream file2(file_path);
    file2 << file_r;
    file2.close();

    auto err = anim.build_from_file(file_path);
    if (err.error != piwo::animation::error_e::ok)
      error_inf = err;

    animations.push_back(anim);
    _palette.extend(animations.begin(), animations.end(), _treshold);

    fs::remove_all(dir_name);
  }

  const piwo::color_palette&
  get_palette()
  {
    return _palette;
  }

private:
  piwo::color_palette _palette;
  const size_t        _treshold = 10;
};

TEST_F(paletteRoundTest, paletteShouldBeMadeFromMultipleFiles)
{
  const piwo::color_palette& palette_ref = get_palette();
  if (error_inf.error != piwo::animation::error_e::ok)
    FAIL() << "Failed to prepare test animation. Build from file failed.";

  ASSERT_EQ(palette_ref.size(), 9);
}

TEST_F(paletteRoundTest, blueSubpixelShouldBeRounded)
{
  const piwo::color_palette& palette_ref = get_palette();
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 10 }).value(),
            piwo::color{ 10 });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 13 }).value(),
            piwo::color{ 10 });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 25 }).value(),
            piwo::color{ 20 });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 36 }).value(),
            piwo::color{ 40 });

  ASSERT_FALSE(palette_ref.get_rounded_color(piwo::color{ 0 }));
}

TEST_F(paletteRoundTest, greenSubpixelShouldBeRounded)
{
  const piwo::color_palette& palette_ref = get_palette();
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 2560 }).value(),
            piwo::color{ 10 << SHIFT_GREEN });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 3329 }).value(),
            piwo::color{ 10 << SHIFT_GREEN });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 6401 }).value(),
            piwo::color{ 20 << SHIFT_GREEN });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 9217 }).value(),
            piwo::color{ 40 << SHIFT_GREEN });
}

TEST_F(paletteRoundTest, redSubpixelShouldBeRounded)
{
  const piwo::color_palette& palette_ref = get_palette();
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 655361 }).value(),
            piwo::color{ 10 << SHIFT_RED });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 851969 }).value(),
            piwo::color{ 10 << SHIFT_RED });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 1638401 }).value(),
            piwo::color{ 20 << SHIFT_RED });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 2359297 }).value(),
            piwo::color{ 40 << SHIFT_RED });
}

TEST_F(paletteRoundTest, paletteShouldContainOnlyRoundedColorsAndTheirIndexes)
{
  const piwo::color_palette& palette_ref = get_palette();
  const std::array           rounded_colors{ 10, 20, 40 };
  for (size_t i = 0; i < 3; ++i)
  {
    ASSERT_EQ(
      palette_ref.get_palette_num(piwo::color{ rounded_colors[i] }).value(), i);
    ASSERT_EQ(palette_ref.get_palette_color(i).value(),
              piwo::color{ rounded_colors[i] });
  }

  for (size_t i = 0; i < 3; ++i)
  {
    ASSERT_EQ(
      palette_ref
        .get_palette_num(piwo::color{ rounded_colors[i] << SHIFT_GREEN })
        .value(),
      i + 3);
    ASSERT_EQ(palette_ref.get_palette_color(i + 3).value(),
              piwo::color{ rounded_colors[i] << SHIFT_GREEN });
  }

  for (size_t i = 0; i < 3; ++i)
  {
    ASSERT_EQ(
      palette_ref.get_palette_num(piwo::color{ rounded_colors[i] << SHIFT_RED })
        .value(),
      i + 6);
    ASSERT_EQ(palette_ref.get_palette_color(i + 6).value(),
              piwo::color{ rounded_colors[i] << SHIFT_RED });
  }
}

constexpr std::string_view file_bgr = "PIWO_7_FILE\n"
                                      "2 2\n"
                                      "\n"
                                      "50\n"
                                      "255 254 \n"
                                      "65280 16711680 \n";

constexpr char filename_paletteRoundLimitTest[] = "/anim_round_limit.piwo7";

struct paletteRoundLimitTest : public ::testing::Test
{
  piwo::animation::error_t error_inf;

  // - 1 for needless '\0'
  static constexpr size_t file_path_length = dir_name_length + std::size(filename_paletteRoundLimitTest) - 1;

  void
  SetUp() override
  {
    ASSERT_EQ(filename_paletteRoundLimitTest[0], '/');

    char dir_name[dir_name_length];
    std::copy_n(dir, dir_name_length, dir_name);

    const char* const dir_name_ptr = mkdtemp(dir_name);
    ASSERT_NE(dir_name_ptr, nullptr);

    char         file_path[file_path_length];
    const size_t bytes_written = snprintf(file_path, file_path_length, "%s%s",
                                           dir_name, filename_paletteRoundLimitTest);
    ASSERT_EQ(bytes_written, file_path_length - 1);

    std::ofstream file(file_path);
    file << file_bgr;
    file.close();

    piwo::animation anim;
    error_inf = anim.build_from_file(file_path);
    _palette.extend(anim, _treshold);

    fs::remove_all(dir_name);
  }

  piwo::color_palette&
  get_palette()
  {
    return _palette;
  }

private:
  piwo::color_palette _palette;
  const size_t        _treshold = 8;
};

TEST_F(paletteRoundLimitTest, subpixelsShouldBeRounded)
{
  piwo::color_palette& palette_ref = get_palette();
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 255 }).value(),
            piwo::color{ 255 });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 254 }).value(),
            piwo::color{ 255 });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 65280 }).value(),
            piwo::color{ 255 << SHIFT_GREEN });
  ASSERT_EQ(palette_ref.get_rounded_color(piwo::color{ 16711680 }).value(),
            piwo::color{ 255 << SHIFT_RED });

  palette_ref.clear();
  ASSERT_EQ(palette_ref.size(), 0);
}
