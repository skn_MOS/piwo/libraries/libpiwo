#include "animation.h"

#include <gtest/gtest.h>

#include <istream>
#include <sstream>

using namespace std;
using namespace testing;
using namespace piwo;

class ConversionTest : public ::testing::Test
{
protected:
  ConversionTest()          = default;
  virtual ~ConversionTest() = default;

  piwo::animation anim;
};

TEST_F(ConversionTest, size_t_properly_converts_to_color)
{
  const size_t full_red   = 0xff0000;
  const size_t full_green = 0x00ff00;
  const size_t full_blue  = 0x0000ff;

  color c_full_red;
  c_full_red.r = 0xff;
  c_full_red.g = 0x00;
  c_full_red.b = 0x00;

  color c_full_green;
  c_full_green.r = 0x00;
  c_full_green.g = 0xff;
  c_full_green.b = 0x00;

  color c_full_blue;
  c_full_blue.r = 0x00;
  c_full_blue.g = 0x00;
  c_full_blue.b = 0xff;

  ASSERT_EQ(c_full_red, color::convert(full_red));
  ASSERT_EQ(c_full_green, color::convert(full_green));
  ASSERT_EQ(c_full_blue, color::convert(full_blue));
}

TEST_F(ConversionTest, color_properly_converts_to_size_t)
{
  const size_t full_red   = 0xff0000;
  const size_t full_green = 0x00ff00;
  const size_t full_blue  = 0x0000ff;

  color c_full_red;
  c_full_red.r = 0xff;
  c_full_red.g = 0x00;
  c_full_red.b = 0x00;

  color c_full_green;
  c_full_green.r = 0x00;
  c_full_green.g = 0xff;
  c_full_green.b = 0x00;

  color c_full_blue;
  c_full_blue.r = 0x00;
  c_full_blue.g = 0x00;
  c_full_blue.b = 0xff;

  ASSERT_EQ(c_full_red.convert<size_t>(), full_red);
  ASSERT_EQ(c_full_green.convert<size_t>(), full_green);
  ASSERT_EQ(c_full_blue.convert<size_t>(), full_blue);
}

TEST_F(ConversionTest, color_properly_constructs_from_size_t)
{
  const size_t full_red   = 0xff0000;
  const size_t full_green = 0x00ff00;
  const size_t full_blue  = 0x0000ff;

  const color c_full_red(full_red);
  const color c_full_green(full_green);
  const color c_full_blue(full_blue);

  ASSERT_EQ(c_full_red.convert<size_t>(), full_red);
  ASSERT_EQ(c_full_green.convert<size_t>(), full_green);
  ASSERT_EQ(c_full_blue.convert<size_t>(), full_blue);
}
