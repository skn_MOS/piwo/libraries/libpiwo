#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <type_traits>

namespace piwo
{
struct __attribute__((packed, aligned(4))) color
{
  static constexpr size_t SUBPIXEL_MASK        = 0xff;
  static constexpr size_t SUBPIXEL_SHIFT_RED   = 16;
  static constexpr size_t SUBPIXEL_SHIFT_GREEN = 8;
  static constexpr size_t SUBPIXEL_SHIFT_BLUE  = 0;
  static constexpr size_t MAX_VALUE            = 0xffffff;

  using subpixel_t = uint8_t;

  subpixel_t r, g, b;

  color() = default;

  color(const color& c) = default;
  color(color&& c)      = default;

  color&
  operator=(const color& c) = default;
  color&
  operator=(color&& c) = default;

  constexpr color(subpixel_t r, subpixel_t g, subpixel_t b)
    : r(r)
    , g(g)
    , b(b)
  {
  }

  template<typename T,
           typename = typename std::enable_if_t<
             !std::is_same_v<std::remove_reference_t<std::decay_t<T>>, color>>>
  constexpr color(T&& packed) noexcept
  {
    static_assert(sizeof(T) >= 3);

    this->r = packed >> SUBPIXEL_SHIFT_RED;
    this->g = packed >> SUBPIXEL_SHIFT_GREEN;
    this->b = packed >> SUBPIXEL_SHIFT_BLUE;
  }

  color(const char* ascii) noexcept
  {
    size_t clr = atoi(ascii);

    r = (clr >> SUBPIXEL_SHIFT_RED) & SUBPIXEL_MASK;
    g = (clr >> SUBPIXEL_SHIFT_GREEN) & SUBPIXEL_MASK;
    b = (clr >> SUBPIXEL_SHIFT_BLUE) & SUBPIXEL_MASK;
  }

  template<typename T>
  constexpr T
  convert() const
  {
    static_assert(sizeof(T) >= 3);

    T color_packed = (static_cast<T>(this->r) << SUBPIXEL_SHIFT_RED) |
                     (static_cast<T>(this->g) << SUBPIXEL_SHIFT_GREEN) |
                     (static_cast<T>(this->b) << SUBPIXEL_SHIFT_BLUE);

    return color_packed;
  }

  template<typename T,
           typename = typename std::enable_if_t<
             !std::is_same_v<std::remove_reference_t<std::decay_t<T>>, color>>>
  static color
  convert(T&& packed)
  {
    static_assert(sizeof(T) >= 3);

    color ret;

    ret.r = packed >> SUBPIXEL_SHIFT_RED;
    ret.g = packed >> SUBPIXEL_SHIFT_GREEN;
    ret.b = packed >> SUBPIXEL_SHIFT_BLUE;

    return ret;
  }

  size_t
  packed() const noexcept
  {
    return (static_cast<size_t>(this->r) << SUBPIXEL_SHIFT_RED) |
           (static_cast<size_t>(this->g) << SUBPIXEL_SHIFT_GREEN) |
           (static_cast<size_t>(this->b) << SUBPIXEL_SHIFT_BLUE);
  }

  bool
  operator==(const color& other) const noexcept
  {
    return (this->r == other.r) && (this->g == other.g) && (this->b == other.b);
  }

  bool
  operator!=(const color& other) const noexcept
  {
    return !(*this == other);
  }
};

struct __attribute__((packed)) packed_color
{
  using single_color_type = uint8_t;

  static constexpr size_t size      = 3 * sizeof(single_color_type);
  static constexpr size_t red_pos   = 0;
  static constexpr size_t green_pos = 1;
  static constexpr size_t blue_pos  = 2;

  packed_color() {}

  packed_color(single_color_type red,
               single_color_type green,
               single_color_type blue)
  {
    this->_data[red_pos]   = red;
    this->_data[green_pos] = green;
    this->_data[blue_pos]  = blue;
  }

  void
  set_red(const single_color_type red) noexcept
  {
    this->_data[red_pos] = red;
  }

  void
  set_green(const single_color_type green) noexcept
  {
    this->_data[green_pos] = green;
  }

  void
  set_blue(const single_color_type blue) noexcept
  {
    this->_data[blue_pos] = blue;
  }

  single_color_type
  get_red() const noexcept
  {
    return this->_data[red_pos];
  }

  single_color_type
  get_green() const noexcept
  {
    return this->_data[green_pos];
  }

  single_color_type
  get_blue() const noexcept
  {
    return this->_data[blue_pos];
  }

  uint8_t*
  data()
  {
    return this->_data.data();
  }

  const uint8_t*
  cdata() const
  {
    return this->_data.data();
  }

private:
  std::array<single_color_type, size> _data;
};

} // namespace piwo
