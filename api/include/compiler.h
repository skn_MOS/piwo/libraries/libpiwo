#pragma once
#include <type_traits>

#define MOS_LIKELY(x)    __builtin_expect((x), 1)
#define MOS_UNLIKELY(x)  __builtin_expect((x), 0)
#define MOS_FORCE_INLINE __attribute__((always_inline))

namespace piwo
{

template<class T>
using utype = std::underlying_type_t<T>;

template<class T>
inline constexpr decltype(auto)
underlay_cast(T e)
{
  return static_cast<utype<T>>(e);
}

} // namespace piwo
