#pragma once

#include <algorithm>
#include <bit>
#include <cstdint>

namespace piwo
{

template<typename T>
T
swap_bytes_order_(T& bytes)
{
  T     le16   =  0;
  char* le16p  = (char*)(&le16);
  char* hostbp = (char*)(&bytes);

  std::reverse_copy(hostbp, hostbp + sizeof(bytes), le16p);

  return le16;
}

template<typename T>
T
htole(T bytes)
{
  if constexpr (std::endian::native == std::endian::big)
  {
   return swap_bytes_order(bytes);
  }

  return bytes;
}

template<typename T>
T
letoh(T bytes)
{
  if constexpr (std::endian::native == std::endian::big)
  {
    return swap_bytes_order(bytes);
  }

  return bytes;
}

} // namespace piwo
