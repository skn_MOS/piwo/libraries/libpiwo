#include "lp_proto.h"

#include <gtest/gtest.h>

using piwo::underlay_cast;

namespace
{

constexpr size_t LP_ACCEPT_SIZE         = 0x1;
constexpr size_t LP_ACCEPT_HUGE_SIZE    = 0x536;
constexpr size_t LP_ACCEPT_BAD_SIZE     = 0x0;
constexpr size_t COMMON_PACKET_TYPE_POS = 0x0;
constexpr size_t LP_ACCEPT_TYPE         = 0x1;

} // namespace

TEST(LpAcceptTest, lp_accept_packet_is_successfully_encoded_and_decoded)
{
  piwo::lp::pipe_byte_t buffer[LP_ACCEPT_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_ACCEPT_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_accept_builder::make_lp_accept_builder(raw_packet);

  ASSERT_TRUE(builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_ACCEPT_TYPE });

  piwo::lp::lp_accept accept_packet(builder_opt.value());

  EXPECT_EQ(accept_packet.size(), LP_ACCEPT_SIZE);

  EXPECT_EQ(accept_packet.cdata(), buffer);
  EXPECT_EQ(accept_packet.data(), buffer);
  EXPECT_EQ(accept_packet.begin(), buffer);
  EXPECT_EQ(accept_packet.cbegin(), buffer);
  EXPECT_EQ(accept_packet.end(), buffer + LP_ACCEPT_SIZE);
  EXPECT_EQ(accept_packet.cend(), buffer + LP_ACCEPT_SIZE);

  EXPECT_EQ(accept_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_ACCEPT_TYPE });

  ASSERT_TRUE(accept_packet.get_type().has_value());
  EXPECT_EQ(underlay_cast(accept_packet.get_type().value()), LP_ACCEPT_TYPE);
}

TEST(
  LpAcceptTest,
  lp_accept_packet_is_successfully_encoded_and_decoded_with_buffer_bigger_than_necessary)
{
  static_assert(LP_ACCEPT_HUGE_SIZE > LP_ACCEPT_SIZE);

  piwo::lp::pipe_byte_t buffer[LP_ACCEPT_HUGE_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_ACCEPT_HUGE_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_accept_builder::make_lp_accept_builder(raw_packet);

  ASSERT_TRUE(builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_ACCEPT_TYPE });

  piwo::lp::lp_accept accept_packet(builder_opt.value());

  EXPECT_EQ(accept_packet.size(), LP_ACCEPT_SIZE);

  EXPECT_EQ(accept_packet.cdata(), buffer);
  EXPECT_EQ(accept_packet.data(), buffer);
  EXPECT_EQ(accept_packet.begin(), buffer);
  EXPECT_EQ(accept_packet.cbegin(), buffer);
  EXPECT_EQ(accept_packet.end(), buffer + LP_ACCEPT_SIZE);
  EXPECT_EQ(accept_packet.cend(), buffer + LP_ACCEPT_SIZE);

  EXPECT_EQ(accept_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_ACCEPT_TYPE });

  ASSERT_TRUE(accept_packet.get_type().has_value());
  EXPECT_EQ(underlay_cast(accept_packet.get_type().value()), LP_ACCEPT_TYPE);
}

TEST(LpAcceptTest,
     lp_accept_packet_creation_fails_with_insufficient_packet_buffer)
{
  piwo::lp::pipe_byte_t buffer[LP_ACCEPT_BAD_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_ACCEPT_BAD_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_accept_builder::make_lp_accept_builder(raw_packet);

  ASSERT_FALSE(builder_opt.has_value());
}
