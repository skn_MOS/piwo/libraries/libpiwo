#include "protodef.h"

#include <cstring>

#include <gtest/gtest.h>

TEST(UidFromAsciParseTest, uid_from_ascii_properly_parses_good_uid)
{
  using piwo::uid;

  std::string test_uid = "AA:BB:CC:DD:EE:FF:A2:B2:C2:D2:E2:F2";

  std::optional uid_opt = uid::from_ascii(test_uid);

  EXPECT_TRUE(uid_opt.has_value());

  auto& uid = *uid_opt;

  EXPECT_EQ(uid.data[0], 0xAA);
  EXPECT_EQ(uid.data[1], 0xBB);
  EXPECT_EQ(uid.data[2], 0xCC);
  EXPECT_EQ(uid.data[3], 0xDD);
  EXPECT_EQ(uid.data[4], 0xEE);
  EXPECT_EQ(uid.data[5], 0xFF);
  EXPECT_EQ(uid.data[6], 0xA2);
  EXPECT_EQ(uid.data[7], 0xB2);
  EXPECT_EQ(uid.data[8], 0xC2);
  EXPECT_EQ(uid.data[9], 0xD2);
  EXPECT_EQ(uid.data[10], 0xE2);
  EXPECT_EQ(uid.data[11], 0xF2);
}

TEST(UidFromAsciParseTest, uid_from_ascii_rejects_too_long_uid1)
{
  using piwo::uid;

  std::string test_uid = "AA:BB:CC:DD:EE:FF:A2:B2:C2:D2:E2:FF2";

  std::optional uid_opt = uid::from_ascii(test_uid);

  EXPECT_FALSE(uid_opt.has_value());
}

TEST(UidFromAsciParseTest,
     uid_from_ascii_properly_parses_non_2digit_aligned_uid)
{
  using piwo::uid;

  std::string test_uid = "A:B:C:D:E:FF:A2:B2:C2:D2:E2:F2";

  std::optional uid_opt = uid::from_ascii(test_uid);

  EXPECT_TRUE(uid_opt.has_value());

  auto& uid = *uid_opt;

  EXPECT_EQ(uid.data[0], 0x0A);
  EXPECT_EQ(uid.data[1], 0x0B);
  EXPECT_EQ(uid.data[2], 0x0C);
  EXPECT_EQ(uid.data[3], 0x0D);
  EXPECT_EQ(uid.data[4], 0x0E);
  EXPECT_EQ(uid.data[5], 0xFF);
  EXPECT_EQ(uid.data[6], 0xA2);
  EXPECT_EQ(uid.data[7], 0xB2);
  EXPECT_EQ(uid.data[8], 0xC2);
  EXPECT_EQ(uid.data[9], 0xD2);
  EXPECT_EQ(uid.data[10], 0xE2);
  EXPECT_EQ(uid.data[11], 0xF2);
}

TEST(UidFromAsciParseTest,
     uid_from_ascii_properly_parses_zero_left_extended_bytes)
{
  using piwo::uid;

  std::string test_uid = "0AA:BB:CC:DD:EE:FF:A2:0B2:0C2:0D2:E2:0F2";

  std::optional uid_opt = uid::from_ascii(test_uid);

  EXPECT_TRUE(uid_opt.has_value());

  auto& uid = *uid_opt;

  EXPECT_EQ(uid.data[0], 0xAA);
  EXPECT_EQ(uid.data[1], 0xBB);
  EXPECT_EQ(uid.data[2], 0xCC);
  EXPECT_EQ(uid.data[3], 0xDD);
  EXPECT_EQ(uid.data[4], 0xEE);
  EXPECT_EQ(uid.data[5], 0xFF);
  EXPECT_EQ(uid.data[6], 0xA2);
  EXPECT_EQ(uid.data[7], 0xB2);
  EXPECT_EQ(uid.data[8], 0xC2);
  EXPECT_EQ(uid.data[9], 0xD2);
  EXPECT_EQ(uid.data[10], 0xE2);
  EXPECT_EQ(uid.data[11], 0xF2);
}

TEST(UidFromAsciParseTest, uid_from_ascii_rejects_too_short_uid1)
{
  using piwo::uid;

  std::string test_uid = "AA:BB:CC:DD:EE:FF:B2:C2:D2:E2:F2";

  std::optional uid_opt = uid::from_ascii(test_uid);

  EXPECT_FALSE(uid_opt.has_value());
}

TEST(UidFromAsciParseTest, uid_from_ascii_rejects_too_short_uid2)
{
  using piwo::uid;

  std::string test_uid = "BB:CC:DD:EE:FF:B2:C2:D2:E2:F2";

  std::optional uid_opt = uid::from_ascii(test_uid);

  EXPECT_FALSE(uid_opt.has_value());
}

TEST(UidFromAsciParseTest, uid_from_ascii_rejects_uid_with_invalid_hex_digits)
{
  using piwo::uid;

  std::string test_uid = "AA:BB:CC:DD:EE:KK:A2:ZZ:C2:D2:E2:F2";

  std::optional uid_opt = uid::from_ascii(test_uid);

  EXPECT_FALSE(uid_opt.has_value());
}

TEST(UidFromAsciParseTest, uid_from_ascii_rejects_uid_with_separator_at_the_end)
{
  using piwo::uid;

  std::string test_uid = "AA:BB:CC:DD:EE:KK:A2:ZZ:C2:D2:E2:F2:";

  std::optional uid_opt = uid::from_ascii(test_uid);

  EXPECT_FALSE(uid_opt.has_value());
}

TEST(UidFromAsciParseTest, uid_from_ascii_rejects_empty_string)
{
  using piwo::uid;

  std::string test_uid = "";

  std::optional uid_opt = uid::from_ascii(test_uid);

  EXPECT_FALSE(uid_opt.has_value());
}

TEST(UidFromAsciParseTest, uid_to_ascii1)
{
  using piwo::uid;

  uid u;
  u.data[0]  = 0xA1;
  u.data[1]  = 0xB1;
  u.data[2]  = 0xC1;
  u.data[3]  = 0xD1;
  u.data[4]  = 0xE1;
  u.data[5]  = 0xF1;
  u.data[6]  = 0xA2;
  u.data[7]  = 0xB2;
  u.data[8]  = 0xC2;
  u.data[9]  = 0xD2;
  u.data[10] = 0xE2;
  u.data[11] = 0xF2;

  std::string       uid_str          = u.to_ascii();
  const std::string expected_uid_str = "A1:B1:C1:D1:E1:F1:A2:B2:C2:D2:E2:F2";

  EXPECT_EQ(uid_str, expected_uid_str);
}
