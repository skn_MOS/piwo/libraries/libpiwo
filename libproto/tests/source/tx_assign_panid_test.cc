#include "proto.h"

#include <gtest/gtest.h>

#include <array>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  TX_ASSIGN_PANID_SIZE      = 17;
constexpr size_t  UID_SIZE                  = 12;
constexpr uint8_t TX_ASSIGN_PANID_TYPE      = 0x0F;
constexpr uint8_t TX_ASSIGN_PANID_SEQ_POS   = 2;
constexpr uint8_t TX_ASSIGN_PANID_UID_POS   = 3;
constexpr uint8_t TX_ASSIGN_PANID_PANID_POS = 15;

constexpr uint8_t  TX_ASSIGN_PANID_SEQ_TEST_VALUE = 2;
constexpr uint16_t TX_ASSIGN_PANID_PANID_VALUE    = 12345;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(TxAssignPanidTest, build_and_make_tx_assign_panid_success)
{
  piwo::net_byte_t buffer[TX_ASSIGN_PANID_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  piwo::uid        uid{ reinterpret_cast<uint8_t*>(uid_buffer.data()) };

  piwo::raw_packet raw_packet(buffer, TX_ASSIGN_PANID_SIZE);

  auto tx_assign_panid_builder_opt =
    piwo::tx_assign_panid_builder::make_tx_assign_panid_builder(raw_packet);

  ASSERT_TRUE(tx_assign_panid_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ TX_ASSIGN_PANID_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ TX_ASSIGN_PANID_TYPE });

  auto tx_assign_panid_builder = *tx_assign_panid_builder_opt;
  tx_assign_panid_builder.set_seq(TX_ASSIGN_PANID_SEQ_TEST_VALUE);
  tx_assign_panid_builder.set_panid(TX_ASSIGN_PANID_PANID_VALUE);
  tx_assign_panid_builder.set_uid(uid);

  piwo::tx_assign_panid tx_assign_panid_packet(tx_assign_panid_builder);

  // Pointers must be the same
  EXPECT_EQ(tx_assign_panid_packet.cdata(), buffer);
  EXPECT_EQ(tx_assign_panid_packet.data(), buffer);
  EXPECT_EQ(tx_assign_panid_packet.cdata(), buffer);
  EXPECT_EQ(tx_assign_panid_packet.begin(), buffer);
  EXPECT_EQ(tx_assign_panid_packet.cbegin(), buffer);
  EXPECT_EQ(tx_assign_panid_packet.end(), buffer + TX_ASSIGN_PANID_SIZE);
  EXPECT_EQ(tx_assign_panid_packet.cend(), buffer + TX_ASSIGN_PANID_SIZE);

  EXPECT_EQ(tx_assign_panid_packet.size(), TX_ASSIGN_PANID_SIZE);
  EXPECT_EQ(tx_assign_panid_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ TX_ASSIGN_PANID_SIZE });

  ASSERT_TRUE(tx_assign_panid_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(tx_assign_panid_packet.get_type().value()),
            TX_ASSIGN_PANID_TYPE);
  EXPECT_EQ(tx_assign_panid_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ TX_ASSIGN_PANID_TYPE });

  EXPECT_EQ(tx_assign_panid_packet.get_seq(), TX_ASSIGN_PANID_SEQ_TEST_VALUE);
  EXPECT_EQ(tx_assign_panid_packet.data()[TX_ASSIGN_PANID_SEQ_POS],
            piwo::net_byte_t{ TX_ASSIGN_PANID_SEQ_TEST_VALUE });

  EXPECT_EQ(tx_assign_panid_packet.get_panid(), TX_ASSIGN_PANID_PANID_VALUE);

  EXPECT_EQ(tx_assign_panid_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> tx_assign_panid_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(tx_assign_panid_packet.data() + TX_ASSIGN_PANID_UID_POS),
                UID_SIZE, tx_assign_panid_packet_uid_data.data());

  EXPECT_EQ(tx_assign_panid_packet_uid_data, uid_buffer);
}

TEST(TxAssignPanidTest, make_tx_assign_panid_success)
{
  piwo::net_byte_t buffer[TX_ASSIGN_PANID_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  uint16_t         panid        = htole16(TX_ASSIGN_PANID_PANID_VALUE);

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ TX_ASSIGN_PANID_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ TX_ASSIGN_PANID_TYPE };
  buffer[TX_ASSIGN_PANID_SEQ_POS] =
    piwo::net_byte_t{ TX_ASSIGN_PANID_SEQ_TEST_VALUE };

  std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&panid),
              sizeof(panid),
              buffer + TX_ASSIGN_PANID_PANID_POS);
  std::copy(reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data()),
            reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data() + UID_SIZE),
            buffer + TX_ASSIGN_PANID_UID_POS);

  piwo::raw_packet raw_packet(buffer, TX_ASSIGN_PANID_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), TX_ASSIGN_PANID_TYPE);

  auto assign_la_packet_opt =
    piwo::tx_assign_panid::make_tx_assign_panid(raw_packet);

  ASSERT_TRUE(assign_la_packet_opt.has_value());

  auto tx_assign_panid_packet = assign_la_packet_opt.value();

  // //Pointers must be the same
  EXPECT_EQ(tx_assign_panid_packet.cdata(), buffer);
  EXPECT_EQ(tx_assign_panid_packet.data(), buffer);
  EXPECT_EQ(tx_assign_panid_packet.begin(), buffer);
  EXPECT_EQ(tx_assign_panid_packet.cbegin(), buffer);
  EXPECT_EQ(tx_assign_panid_packet.end(), buffer + TX_ASSIGN_PANID_SIZE);
  EXPECT_EQ(tx_assign_panid_packet.cend(), buffer + TX_ASSIGN_PANID_SIZE);

  EXPECT_EQ(tx_assign_panid_packet.size(), TX_ASSIGN_PANID_SIZE);
  EXPECT_EQ(tx_assign_panid_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ TX_ASSIGN_PANID_SIZE });

  ASSERT_TRUE(tx_assign_panid_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(tx_assign_panid_packet.get_type().value()),
            TX_ASSIGN_PANID_TYPE);
  EXPECT_EQ(tx_assign_panid_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ TX_ASSIGN_PANID_TYPE });

  EXPECT_EQ(tx_assign_panid_packet.get_seq(), TX_ASSIGN_PANID_SEQ_TEST_VALUE);
  EXPECT_EQ(tx_assign_panid_packet.data()[TX_ASSIGN_PANID_SEQ_POS],
            piwo::net_byte_t{ TX_ASSIGN_PANID_SEQ_TEST_VALUE });

  EXPECT_EQ(tx_assign_panid_packet.get_panid(), TX_ASSIGN_PANID_PANID_VALUE);

  EXPECT_EQ(tx_assign_panid_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> tx_assign_panid_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(tx_assign_panid_packet.data() + TX_ASSIGN_PANID_UID_POS),
                UID_SIZE, tx_assign_panid_packet_uid_data.data());

  EXPECT_EQ(tx_assign_panid_packet_uid_data, uid_buffer);
}

TEST(TxAssignPanidTest, make_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = TX_ASSIGN_PANID_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto tx_assign_panid_builder_opt =
    piwo::tx_assign_panid_builder::make_tx_assign_panid_builder(raw_packet);

  EXPECT_FALSE(tx_assign_panid_builder_opt.has_value());
}

TEST(TxAssignPanidTest, make_tx_assign_panid_fail_buffer_too_small)
{
  auto             too_small_buffer_size = TX_ASSIGN_PANID_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto tx_assign_panid_opt =
    piwo::tx_assign_panid::make_tx_assign_panid(raw_packet);

  EXPECT_FALSE(tx_assign_panid_opt.has_value());
}

TEST(TxAssignPanidTest, make_tx_assign_panid_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = TX_ASSIGN_PANID_SIZE + 1;
  piwo::net_byte_t buffer[TX_ASSIGN_PANID_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, TX_ASSIGN_PANID_SIZE);

  auto tx_assign_panid_opt =
    piwo::tx_assign_panid::make_tx_assign_panid(raw_packet);

  EXPECT_FALSE(tx_assign_panid_opt.has_value());

  inconsistent_frame_length = TX_ASSIGN_PANID_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  tx_assign_panid_opt = piwo::tx_assign_panid::make_tx_assign_panid(raw_packet);

  EXPECT_FALSE(tx_assign_panid_opt.has_value());
}

TEST(TxAssignPanidTest, make_tx_assign_panid_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[TX_ASSIGN_PANID_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ TX_ASSIGN_PANID_SIZE };

  piwo::raw_packet raw_packet(buffer, TX_ASSIGN_PANID_SIZE);

  auto tx_assign_panid_opt =
    piwo::tx_assign_panid::make_tx_assign_panid(raw_packet);

  EXPECT_FALSE(tx_assign_panid_opt.has_value());
}
