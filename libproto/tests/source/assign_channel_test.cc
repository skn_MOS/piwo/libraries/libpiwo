#include "proto.h"

#include <gtest/gtest.h>

#include <array>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  ASSIGN_CHANNEL_SIZE        = 16;
constexpr size_t  UID_SIZE                   = 12;
constexpr uint8_t ASSIGN_CHANNEL_TYPE        = 0x09;
constexpr uint8_t ASSIGN_CHANNEL_SEQ_POS     = 2;
constexpr uint8_t ASSIGN_CHANNEL_UID_POS     = 3;
constexpr uint8_t ASSIGN_CHANNEL_CHANNEL_POS = 15;

constexpr uint8_t  ASSIGN_CHANNEL_SEQ_TEST_VALUE = 2;
constexpr uint16_t ASSIGN_CHANNEL_CHANNEL_VALUE  = 17;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(AssignChannelTest, build_and_make_assign_channel_success)
{
  piwo::net_byte_t buffer[ASSIGN_CHANNEL_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  piwo::uid        uid{ reinterpret_cast<uint8_t*>(uid_buffer.data()) };

  piwo::raw_packet raw_packet(buffer, ASSIGN_CHANNEL_SIZE);

  auto assign_channel_builder_opt =
    piwo::assign_channel_builder::make_assign_channel_builder(raw_packet);

  ASSERT_TRUE(assign_channel_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_TYPE });

  auto assign_channel_builder = *assign_channel_builder_opt;
  assign_channel_builder.set_seq(ASSIGN_CHANNEL_SEQ_TEST_VALUE);
  assign_channel_builder.set_channel(ASSIGN_CHANNEL_CHANNEL_VALUE);
  assign_channel_builder.set_uid(uid);

  piwo::assign_channel assign_channel_packet(assign_channel_builder);

  // Pointers must be the same
  EXPECT_EQ(assign_channel_packet.cdata(), buffer);
  EXPECT_EQ(assign_channel_packet.data(), buffer);
  EXPECT_EQ(assign_channel_packet.cdata(), buffer);
  EXPECT_EQ(assign_channel_packet.begin(), buffer);
  EXPECT_EQ(assign_channel_packet.cbegin(), buffer);
  EXPECT_EQ(assign_channel_packet.end(), buffer + ASSIGN_CHANNEL_SIZE);
  EXPECT_EQ(assign_channel_packet.cend(), buffer + ASSIGN_CHANNEL_SIZE);

  EXPECT_EQ(assign_channel_packet.size(), ASSIGN_CHANNEL_SIZE);
  EXPECT_EQ(assign_channel_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_SIZE });

  ASSERT_TRUE(assign_channel_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(assign_channel_packet.get_type().value()),
            ASSIGN_CHANNEL_TYPE);
  EXPECT_EQ(assign_channel_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_TYPE });

  EXPECT_EQ(assign_channel_packet.get_seq(), ASSIGN_CHANNEL_SEQ_TEST_VALUE);
  EXPECT_EQ(assign_channel_packet.data()[ASSIGN_CHANNEL_SEQ_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_SEQ_TEST_VALUE });

  EXPECT_EQ(assign_channel_packet.get_channel(), ASSIGN_CHANNEL_CHANNEL_VALUE);
  EXPECT_EQ(assign_channel_packet.data()[ASSIGN_CHANNEL_CHANNEL_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_CHANNEL_VALUE });

  EXPECT_EQ(assign_channel_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> assign_channel_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(assign_channel_packet.data() + ASSIGN_CHANNEL_UID_POS),
                UID_SIZE, assign_channel_packet_uid_data.data());

  EXPECT_EQ(assign_channel_packet_uid_data, uid_buffer);

}

TEST(AssignChannelTest, make_assign_channel_success)
{
  piwo::net_byte_t buffer[ASSIGN_CHANNEL_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  uint16_t         channel      = htole16(ASSIGN_CHANNEL_CHANNEL_VALUE);

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ ASSIGN_CHANNEL_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ ASSIGN_CHANNEL_TYPE };
  buffer[ASSIGN_CHANNEL_SEQ_POS] =
    piwo::net_byte_t{ ASSIGN_CHANNEL_SEQ_TEST_VALUE };
  buffer[ASSIGN_CHANNEL_CHANNEL_POS] =
    piwo::net_byte_t{ ASSIGN_CHANNEL_CHANNEL_VALUE };

  std::copy(reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data()),
             reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data() + UID_SIZE),
             buffer + ASSIGN_CHANNEL_UID_POS);

  piwo::raw_packet raw_packet(buffer, ASSIGN_CHANNEL_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), ASSIGN_CHANNEL_TYPE);

  auto assign_la_packet_opt =
    piwo::assign_channel::make_assign_channel(raw_packet);

  ASSERT_TRUE(assign_la_packet_opt.has_value());

  auto assign_channel_packet = assign_la_packet_opt.value();

  // //Pointers must be the same
  EXPECT_EQ(assign_channel_packet.cdata(), buffer);
  EXPECT_EQ(assign_channel_packet.data(), buffer);
  EXPECT_EQ(assign_channel_packet.begin(), buffer);
  EXPECT_EQ(assign_channel_packet.cbegin(), buffer);
  EXPECT_EQ(assign_channel_packet.end(), buffer + ASSIGN_CHANNEL_SIZE);
  EXPECT_EQ(assign_channel_packet.cend(), buffer + ASSIGN_CHANNEL_SIZE);

  EXPECT_EQ(assign_channel_packet.size(), ASSIGN_CHANNEL_SIZE);
  EXPECT_EQ(assign_channel_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_SIZE });

  ASSERT_TRUE(assign_channel_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(assign_channel_packet.get_type().value()),
            ASSIGN_CHANNEL_TYPE);
  EXPECT_EQ(assign_channel_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_TYPE });

  EXPECT_EQ(assign_channel_packet.get_seq(), ASSIGN_CHANNEL_SEQ_TEST_VALUE);
  EXPECT_EQ(assign_channel_packet.data()[ASSIGN_CHANNEL_SEQ_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_SEQ_TEST_VALUE });

  EXPECT_EQ(assign_channel_packet.get_channel(), ASSIGN_CHANNEL_CHANNEL_VALUE);
  EXPECT_EQ(assign_channel_packet.data()[ASSIGN_CHANNEL_CHANNEL_POS],
            piwo::net_byte_t{ ASSIGN_CHANNEL_CHANNEL_VALUE });

  EXPECT_EQ(assign_channel_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> assign_channel_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(assign_channel_packet.data() + ASSIGN_CHANNEL_UID_POS),
                UID_SIZE, assign_channel_uid_data.data());

  EXPECT_EQ(assign_channel_uid_data, uid_buffer);
}

TEST(AssignChannelTest, make_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = ASSIGN_CHANNEL_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto assign_channel_builder_opt =
    piwo::assign_channel_builder::make_assign_channel_builder(raw_packet);

  EXPECT_FALSE(assign_channel_builder_opt.has_value());
}

TEST(AssignChannelTest, make_assign_channel_fail_buffer_too_small)
{
  auto             too_small_buffer_size = ASSIGN_CHANNEL_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto assign_channel_opt =
    piwo::assign_channel::make_assign_channel(raw_packet);

  EXPECT_FALSE(assign_channel_opt.has_value());
}

TEST(AssignChannelTest, make_assign_channel_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = ASSIGN_CHANNEL_SIZE + 1;
  piwo::net_byte_t buffer[ASSIGN_CHANNEL_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, ASSIGN_CHANNEL_SIZE);

  auto assign_channel_opt =
    piwo::assign_channel::make_assign_channel(raw_packet);

  EXPECT_FALSE(assign_channel_opt.has_value());

  inconsistent_frame_length = ASSIGN_CHANNEL_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  assign_channel_opt = piwo::assign_channel::make_assign_channel(raw_packet);

  EXPECT_FALSE(assign_channel_opt.has_value());
}

TEST(AssignChannelTest, make_assign_channel_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[ASSIGN_CHANNEL_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ ASSIGN_CHANNEL_SIZE };

  piwo::raw_packet raw_packet(buffer, ASSIGN_CHANNEL_SIZE);

  auto assign_channel_opt =
    piwo::assign_channel::make_assign_channel(raw_packet);

  EXPECT_FALSE(assign_channel_opt.has_value());
}
