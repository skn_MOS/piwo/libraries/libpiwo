#include "proto.h"

#include <gtest/gtest.h>

#include <array>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  ASSIGN_LA_SIZE    = 16;
constexpr size_t  UID_SIZE          = 12;
constexpr uint8_t ASSIGN_LA_TYPE    = 0x04;
constexpr uint8_t ASSIGN_LA_SEQ_POS = 2;
constexpr uint8_t ASSIGN_LA_UID_POS = 3;
constexpr uint8_t ASSIGN_LA_LA_POS  = 15;

constexpr uint8_t ASSIGN_LA_SEQ_TEST_VALUE = 2;
constexpr uint8_t ASSIGN_LA_LA_TEST_VALUE  = 15;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(AssignLogicAddressTest, build_and_make_status_req_success)
{
  piwo::net_byte_t buffer[ASSIGN_LA_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  piwo::uid        uid{ reinterpret_cast<uint8_t*>(uid_buffer.data()) };

  piwo::raw_packet raw_packet(buffer, ASSIGN_LA_SIZE);

  auto assign_la_builder_opt =
    piwo::assign_la_builder::make_assign_la_builder(raw_packet);

  ASSERT_TRUE(assign_la_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ ASSIGN_LA_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS], piwo::net_byte_t{ ASSIGN_LA_TYPE });

  auto assign_la_builder = *assign_la_builder_opt;
  assign_la_builder.set_seq(ASSIGN_LA_SEQ_TEST_VALUE);
  assign_la_builder.set_la(ASSIGN_LA_LA_TEST_VALUE);
  assign_la_builder.set_uid(uid);

  piwo::assign_la assign_la_packet(assign_la_builder);

  // Pointers must be the same
  EXPECT_EQ(assign_la_packet.cdata(), buffer);
  EXPECT_EQ(assign_la_packet.data(), buffer);
  EXPECT_EQ(assign_la_packet.cdata(), buffer);
  EXPECT_EQ(assign_la_packet.begin(), buffer);
  EXPECT_EQ(assign_la_packet.cbegin(), buffer);
  EXPECT_EQ(assign_la_packet.end(), buffer + ASSIGN_LA_SIZE);
  EXPECT_EQ(assign_la_packet.cend(), buffer + ASSIGN_LA_SIZE);

  EXPECT_EQ(assign_la_packet.size(), ASSIGN_LA_SIZE);
  EXPECT_EQ(assign_la_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ ASSIGN_LA_SIZE });

  ASSERT_TRUE(assign_la_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(assign_la_packet.get_type().value()), ASSIGN_LA_TYPE);
  EXPECT_EQ(assign_la_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ ASSIGN_LA_TYPE });

  EXPECT_EQ(assign_la_packet.get_seq(), ASSIGN_LA_SEQ_TEST_VALUE);
  EXPECT_EQ(assign_la_packet.data()[ASSIGN_LA_SEQ_POS],
            piwo::net_byte_t{ ASSIGN_LA_SEQ_TEST_VALUE });

  EXPECT_EQ(assign_la_packet.get_la(), ASSIGN_LA_LA_TEST_VALUE);
  EXPECT_EQ(assign_la_packet.data()[ASSIGN_LA_LA_POS],
            piwo::net_byte_t{ ASSIGN_LA_LA_TEST_VALUE });

  EXPECT_EQ(assign_la_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> assign_la_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(assign_la_packet.data() + ASSIGN_LA_UID_POS),
                UID_SIZE, assign_la_packet_uid_data.data());

  EXPECT_EQ(assign_la_packet_uid_data, uid_buffer);
}

TEST(AssignLogicAddressTest, make_status_req_success)
{
  piwo::net_byte_t buffer[ASSIGN_LA_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ ASSIGN_LA_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ ASSIGN_LA_TYPE };
  buffer[ASSIGN_LA_SEQ_POS] = piwo::net_byte_t{ ASSIGN_LA_SEQ_TEST_VALUE };
  buffer[ASSIGN_LA_LA_POS]  = piwo::net_byte_t{ ASSIGN_LA_LA_TEST_VALUE };
  std::copy(reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data()),
            reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data() + UID_SIZE),
            buffer + ASSIGN_LA_UID_POS);

  piwo::raw_packet raw_packet(buffer, ASSIGN_LA_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), ASSIGN_LA_TYPE);

  auto assign_la_packet_opt = piwo::assign_la::make_assign_la(raw_packet);

  ASSERT_TRUE(assign_la_packet_opt.has_value());

  auto assign_la_packet = assign_la_packet_opt.value();

  // //Pointers must be the same
  EXPECT_EQ(assign_la_packet.cdata(), buffer);
  EXPECT_EQ(assign_la_packet.data(), buffer);
  EXPECT_EQ(assign_la_packet.begin(), buffer);
  EXPECT_EQ(assign_la_packet.cbegin(), buffer);
  EXPECT_EQ(assign_la_packet.end(), buffer + ASSIGN_LA_SIZE);
  EXPECT_EQ(assign_la_packet.cend(), buffer + ASSIGN_LA_SIZE);

  EXPECT_EQ(assign_la_packet.size(), ASSIGN_LA_SIZE);
  EXPECT_EQ(assign_la_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ ASSIGN_LA_SIZE });

  ASSERT_TRUE(assign_la_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(assign_la_packet.get_type().value()), ASSIGN_LA_TYPE);
  EXPECT_EQ(assign_la_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ ASSIGN_LA_TYPE });

  EXPECT_EQ(assign_la_packet.get_seq(), ASSIGN_LA_SEQ_TEST_VALUE);
  EXPECT_EQ(assign_la_packet.data()[ASSIGN_LA_SEQ_POS],
            piwo::net_byte_t{ ASSIGN_LA_SEQ_TEST_VALUE });

  EXPECT_EQ(assign_la_packet.get_la(), ASSIGN_LA_LA_TEST_VALUE);
  EXPECT_EQ(assign_la_packet.data()[ASSIGN_LA_LA_POS],
            piwo::net_byte_t{ ASSIGN_LA_LA_TEST_VALUE });

  EXPECT_EQ(assign_la_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> assign_la_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(assign_la_packet.data() + ASSIGN_LA_UID_POS),
                UID_SIZE, assign_la_packet_uid_data.data());

  EXPECT_EQ(assign_la_packet_uid_data, uid_buffer);
}

TEST(AssignLogicAddressTest, make_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = ASSIGN_LA_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto assign_la_builder_opt =
    piwo::assign_la_builder::make_assign_la_builder(raw_packet);

  EXPECT_FALSE(assign_la_builder_opt.has_value());
}

TEST(AssignLogicAddressTest, make_assign_la_fail_buffer_too_small)
{
  auto             too_small_buffer_size = ASSIGN_LA_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto assign_la_opt = piwo::assign_la::make_assign_la(raw_packet);

  EXPECT_FALSE(assign_la_opt.has_value());
}

TEST(AssignLogicAddressTest, make_assign_la_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = ASSIGN_LA_SIZE + 1;
  piwo::net_byte_t buffer[ASSIGN_LA_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, ASSIGN_LA_SIZE);

  auto assign_la_opt = piwo::assign_la::make_assign_la(raw_packet);

  EXPECT_FALSE(assign_la_opt.has_value());

  inconsistent_frame_length = ASSIGN_LA_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  assign_la_opt = piwo::assign_la::make_assign_la(raw_packet);

  EXPECT_FALSE(assign_la_opt.has_value());
}

TEST(AssignLogicAddressTest, make_assign_la_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[ASSIGN_LA_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ ASSIGN_LA_SIZE };

  piwo::raw_packet raw_packet(buffer, ASSIGN_LA_SIZE);

  auto assign_la_opt = piwo::assign_la::make_assign_la(raw_packet);

  EXPECT_FALSE(assign_la_opt.has_value());
}
