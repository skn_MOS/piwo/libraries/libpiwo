#include "proto.h"

#include <gtest/gtest.h>

#include <array>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  CONST_COLOR_SIZE      = 18;
constexpr size_t  UID_SIZE              = 12;
constexpr uint8_t CONST_COLOR_TYPE      = 0x06;
constexpr uint8_t CONST_COLOR_SEQ_POS   = 2;
constexpr uint8_t CONST_COLOR_UID_POS   = 3;
constexpr uint8_t CONST_COLOR_RED_POS   = 15;
constexpr uint8_t CONST_COLOR_GREEN_POS = 16;
constexpr uint8_t CONST_COLOR_BLUE_POS  = 17;

constexpr uint8_t CONST_COLOR_SEQ_TEST_VALUE   = 2;
constexpr uint8_t CONST_COLOR_RED_TEST_VALUE   = 200;
constexpr uint8_t CONST_COLOR_GREEN_TEST_VALUE = 210;
constexpr uint8_t CONST_COLOR_BLUE_TEST_VALUE  = 220;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(ConstantColorTest, build_and_make_constant_color_success)
{
  piwo::net_byte_t   buffer[CONST_COLOR_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  piwo::uid          uid{ reinterpret_cast<uint8_t*>(uid_buffer.data()) };
  piwo::packed_color color(CONST_COLOR_RED_TEST_VALUE,
                           CONST_COLOR_GREEN_TEST_VALUE,
                           CONST_COLOR_BLUE_TEST_VALUE);

  piwo::raw_packet raw_packet(buffer, CONST_COLOR_SIZE);

  auto constant_color_builder_opt =
    piwo::constant_color_builder::make_constant_color_builder(raw_packet);

  ASSERT_TRUE(constant_color_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ CONST_COLOR_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ CONST_COLOR_TYPE });

  auto constant_color_builder = *constant_color_builder_opt;
  constant_color_builder.set_seq(CONST_COLOR_SEQ_TEST_VALUE);
  constant_color_builder.set_uid(uid);
  constant_color_builder.set_color(color);

  piwo::constant_color constant_color_packet(constant_color_builder);

  // Pointers must be the same
  EXPECT_EQ(constant_color_packet.cdata(), buffer);
  EXPECT_EQ(constant_color_packet.data(), buffer);
  EXPECT_EQ(constant_color_packet.cdata(), buffer);
  EXPECT_EQ(constant_color_packet.begin(), buffer);
  EXPECT_EQ(constant_color_packet.cbegin(), buffer);
  EXPECT_EQ(constant_color_packet.end(), buffer + CONST_COLOR_SIZE);
  EXPECT_EQ(constant_color_packet.cend(), buffer + CONST_COLOR_SIZE);

  EXPECT_EQ(constant_color_packet.size(), CONST_COLOR_SIZE);
  EXPECT_EQ(constant_color_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ CONST_COLOR_SIZE });

  ASSERT_TRUE(constant_color_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(constant_color_packet.get_type().value()),
            CONST_COLOR_TYPE);
  EXPECT_EQ(constant_color_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ CONST_COLOR_TYPE });

  EXPECT_EQ(constant_color_packet.get_seq(), CONST_COLOR_SEQ_TEST_VALUE);
  EXPECT_EQ(constant_color_packet.data()[CONST_COLOR_SEQ_POS],
            piwo::net_byte_t{ CONST_COLOR_SEQ_TEST_VALUE });

  EXPECT_EQ(constant_color_packet.get_color().get_red(),
            CONST_COLOR_RED_TEST_VALUE);
  EXPECT_EQ(constant_color_packet.get_color().get_green(),
            CONST_COLOR_GREEN_TEST_VALUE);
  EXPECT_EQ(constant_color_packet.get_color().get_blue(),
            CONST_COLOR_BLUE_TEST_VALUE);
  EXPECT_EQ(constant_color_packet.data()[CONST_COLOR_RED_POS],
            piwo::net_byte_t{ CONST_COLOR_RED_TEST_VALUE });
  EXPECT_EQ(constant_color_packet.data()[CONST_COLOR_GREEN_POS],
            piwo::net_byte_t{ CONST_COLOR_GREEN_TEST_VALUE });
  EXPECT_EQ(constant_color_packet.data()[CONST_COLOR_BLUE_POS],
            piwo::net_byte_t{ CONST_COLOR_BLUE_TEST_VALUE });

  EXPECT_EQ(constant_color_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> constant_color_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(constant_color_packet.data() + CONST_COLOR_UID_POS),
                UID_SIZE, constant_color_packet_uid_data.data());

  EXPECT_EQ(constant_color_packet_uid_data, uid_buffer);
}

TEST(ConstantColorTest, make_const_color_success)
{
  piwo::net_byte_t buffer[CONST_COLOR_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ CONST_COLOR_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ CONST_COLOR_TYPE };
  buffer[CONST_COLOR_SEQ_POS] = piwo::net_byte_t{ CONST_COLOR_SEQ_TEST_VALUE };
  buffer[CONST_COLOR_RED_POS] = piwo::net_byte_t{ CONST_COLOR_RED_TEST_VALUE };
  buffer[CONST_COLOR_GREEN_POS] =
    piwo::net_byte_t{ CONST_COLOR_GREEN_TEST_VALUE };
  buffer[CONST_COLOR_BLUE_POS] =
    piwo::net_byte_t{ CONST_COLOR_BLUE_TEST_VALUE };

  std::copy(reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data()),
            reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data() + UID_SIZE),
            buffer + CONST_COLOR_UID_POS);

  piwo::raw_packet raw_packet(buffer, CONST_COLOR_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), CONST_COLOR_TYPE);

  auto const_color_packet_opt =
    piwo::constant_color::make_constant_color(raw_packet);

  ASSERT_TRUE(const_color_packet_opt.has_value());

  auto const_color_packet = const_color_packet_opt.value();

  // //Pointers must be the same
  EXPECT_EQ(const_color_packet.cdata(), buffer);
  EXPECT_EQ(const_color_packet.data(), buffer);
  EXPECT_EQ(const_color_packet.begin(), buffer);
  EXPECT_EQ(const_color_packet.cbegin(), buffer);
  EXPECT_EQ(const_color_packet.end(), buffer + CONST_COLOR_SIZE);
  EXPECT_EQ(const_color_packet.cend(), buffer + CONST_COLOR_SIZE);

  EXPECT_EQ(const_color_packet.size(), CONST_COLOR_SIZE);
  EXPECT_EQ(const_color_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ CONST_COLOR_SIZE });

  ASSERT_TRUE(const_color_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(const_color_packet.get_type().value()),
            CONST_COLOR_TYPE);
  EXPECT_EQ(const_color_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ CONST_COLOR_TYPE });

  EXPECT_EQ(const_color_packet.get_seq(), CONST_COLOR_SEQ_TEST_VALUE);
  EXPECT_EQ(const_color_packet.data()[CONST_COLOR_SEQ_POS],
            piwo::net_byte_t{ CONST_COLOR_SEQ_TEST_VALUE });

  EXPECT_EQ(const_color_packet.get_color().get_red(),
            CONST_COLOR_RED_TEST_VALUE);
  EXPECT_EQ(const_color_packet.get_color().get_green(),
            CONST_COLOR_GREEN_TEST_VALUE);
  EXPECT_EQ(const_color_packet.get_color().get_blue(),
            CONST_COLOR_BLUE_TEST_VALUE);
  EXPECT_EQ(const_color_packet.data()[CONST_COLOR_RED_POS],
            piwo::net_byte_t{ CONST_COLOR_RED_TEST_VALUE });
  EXPECT_EQ(const_color_packet.data()[CONST_COLOR_GREEN_POS],
            piwo::net_byte_t{ CONST_COLOR_GREEN_TEST_VALUE });
  EXPECT_EQ(const_color_packet.data()[CONST_COLOR_BLUE_POS],
            piwo::net_byte_t{ CONST_COLOR_BLUE_TEST_VALUE });

  EXPECT_EQ(const_color_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> const_color_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(const_color_packet.data() + CONST_COLOR_UID_POS),
                UID_SIZE, const_color_packet_uid_data.data());

  EXPECT_EQ(const_color_packet_uid_data, uid_buffer);
}

TEST(ConstantColorTest, make_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = CONST_COLOR_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto constant_color_builder_opt =
    piwo::constant_color_builder::make_constant_color_builder(raw_packet);

  EXPECT_FALSE(constant_color_builder_opt.has_value());
}

TEST(ConstantColorTest, make_constant_color_fail_buffer_too_small)
{
  auto             too_small_buffer_size = CONST_COLOR_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto const_color_opt = piwo::constant_color::make_constant_color(raw_packet);

  EXPECT_FALSE(const_color_opt.has_value());
}

TEST(ConstantColorTest, make_constant_color_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = CONST_COLOR_SIZE + 1;
  piwo::net_byte_t buffer[CONST_COLOR_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, CONST_COLOR_SIZE);

  auto const_color_opt = piwo::constant_color::make_constant_color(raw_packet);

  EXPECT_FALSE(const_color_opt.has_value());

  inconsistent_frame_length = CONST_COLOR_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  const_color_opt = piwo::constant_color::make_constant_color(raw_packet);

  EXPECT_FALSE(const_color_opt.has_value());
}

TEST(ConstantColorTest, make_constant_color_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[CONST_COLOR_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ CONST_COLOR_SIZE };

  piwo::raw_packet raw_packet(buffer, CONST_COLOR_SIZE);

  auto const_color_opt = piwo::constant_color::make_constant_color(raw_packet);

  EXPECT_FALSE(const_color_opt.has_value());
}
