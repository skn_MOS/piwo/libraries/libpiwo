#include "proto.h"

#include <gtest/gtest.h>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  ACK_SIZE          = 16;
constexpr size_t  UID_SIZE          = 12;
constexpr uint8_t ACK_TYPE          = 0x01;
constexpr uint8_t ACK_SEQ_POS       = 2;
constexpr uint8_t ACK_UID_POS       = 3;
constexpr uint8_t ACK_PACKET_ID_POS = 15;

constexpr uint8_t ACK_SEQ_TEST_VALUE       = 2;
constexpr uint8_t ACK_PACKET_ID_TEST_VALUE = 1;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(AckTest, build_and_make_ack_success)
{
  piwo::net_byte_t buffer[ACK_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  piwo::uid        uid{ reinterpret_cast<uint8_t*>(uid_buffer.data()) };

  piwo::raw_packet raw_packet(buffer, ACK_SIZE);

  auto ack_builder_opt = piwo::ack_builder::make_ack_builder(raw_packet);

  ASSERT_TRUE(ack_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS], piwo::net_byte_t{ ACK_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS], piwo::net_byte_t{ ACK_TYPE });

  auto ack_builder = *ack_builder_opt;
  ack_builder.set_seq(ACK_SEQ_TEST_VALUE);
  ack_builder.set_packet_id(ACK_PACKET_ID_TEST_VALUE);
  ack_builder.set_uid(uid);

  piwo::ack ack_packet(ack_builder);

  // Pointers must be the same
  EXPECT_EQ(ack_packet.cdata(), buffer);
  EXPECT_EQ(ack_packet.data(), buffer);
  EXPECT_EQ(ack_packet.cdata(), buffer);
  EXPECT_EQ(ack_packet.begin(), buffer);
  EXPECT_EQ(ack_packet.cbegin(), buffer);
  EXPECT_EQ(ack_packet.end(), buffer + ACK_SIZE);
  EXPECT_EQ(ack_packet.cend(), buffer + ACK_SIZE);

  EXPECT_EQ(ack_packet.size(), ACK_SIZE);
  EXPECT_EQ(ack_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ ACK_SIZE });

  ASSERT_TRUE(ack_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(ack_packet.get_type().value()), ACK_TYPE);
  EXPECT_EQ(ack_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ ACK_TYPE });

  EXPECT_EQ(ack_packet.get_seq(), ACK_SEQ_TEST_VALUE);
  EXPECT_EQ(ack_packet.data()[ACK_SEQ_POS],
            piwo::net_byte_t{ ACK_SEQ_TEST_VALUE });

  EXPECT_EQ(ack_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> ack_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(ack_packet.data() + ACK_UID_POS),
                UID_SIZE, ack_packet_uid_data.data());

  EXPECT_EQ(ack_packet_uid_data, uid_buffer);

  EXPECT_EQ(ack_packet.get_packet_id(), ACK_PACKET_ID_TEST_VALUE);
  EXPECT_EQ(ack_packet.data()[ACK_PACKET_ID_POS],
            piwo::net_byte_t{ ACK_PACKET_ID_TEST_VALUE });
}

TEST(AckTest, make_ack_success)
{
  piwo::net_byte_t buffer[ACK_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ ACK_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ ACK_TYPE };
  buffer[ACK_SEQ_POS]              = piwo::net_byte_t{ ACK_SEQ_TEST_VALUE };
  buffer[ACK_PACKET_ID_POS] = piwo::net_byte_t{ ACK_PACKET_ID_TEST_VALUE };
  std::copy(reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data()),
             reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data() + UID_SIZE),
             buffer + ACK_UID_POS);

  piwo::raw_packet raw_packet(buffer, ACK_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), ACK_TYPE);

  auto ack_packet_opt = piwo::ack::make_ack(raw_packet);

  ASSERT_TRUE(ack_packet_opt.has_value());

  auto ack_packet = ack_packet_opt.value();

  // //Pointers must be the same
  EXPECT_EQ(ack_packet.cdata(), buffer);
  EXPECT_EQ(ack_packet.data(), buffer);
  EXPECT_EQ(ack_packet.begin(), buffer);
  EXPECT_EQ(ack_packet.cbegin(), buffer);
  EXPECT_EQ(ack_packet.end(), buffer + ACK_SIZE);
  EXPECT_EQ(ack_packet.cend(), buffer + ACK_SIZE);

  EXPECT_EQ(ack_packet.size(), ACK_SIZE);
  EXPECT_EQ(ack_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ ACK_SIZE });

  ASSERT_TRUE(ack_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(ack_packet.get_type().value()), ACK_TYPE);
  EXPECT_EQ(ack_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ ACK_TYPE });

  EXPECT_EQ(ack_packet.get_seq(), ACK_SEQ_TEST_VALUE);
  EXPECT_EQ(ack_packet.data()[ACK_SEQ_POS],
            piwo::net_byte_t{ ACK_SEQ_TEST_VALUE });

  EXPECT_EQ(ack_packet.get_packet_id(), ACK_PACKET_ID_TEST_VALUE);
  EXPECT_EQ(ack_packet.data()[ACK_PACKET_ID_POS],
            piwo::net_byte_t{ ACK_PACKET_ID_TEST_VALUE });

  EXPECT_EQ(ack_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> ack_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(ack_packet.data() + ACK_UID_POS),
                UID_SIZE, ack_packet_uid_data.data());

  EXPECT_EQ(ack_packet_uid_data, uid_buffer);
}

TEST(AckTest, make_ack_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = ACK_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto ack_builder_opt = piwo::ack_builder::make_ack_builder(raw_packet);

  EXPECT_FALSE(ack_builder_opt.has_value());
}

TEST(AckTest, make_ack_fail_buffer_too_small)
{
  auto             too_small_buffer_size = ACK_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto ack_opt = piwo::ack::make_ack(raw_packet);

  EXPECT_FALSE(ack_opt.has_value());
}

TEST(AckTest, make_ack_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = ACK_SIZE + 1;
  piwo::net_byte_t buffer[ACK_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, ACK_SIZE);

  auto ack_opt = piwo::ack::make_ack(raw_packet);

  EXPECT_FALSE(ack_opt.has_value());

  inconsistent_frame_length = ACK_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  ack_opt = piwo::ack::make_ack(raw_packet);

  EXPECT_FALSE(ack_opt.has_value());
}

TEST(AckTest, make_ack_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[ACK_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ ACK_SIZE };

  piwo::raw_packet raw_packet(buffer, ACK_SIZE);

  auto ack_opt = piwo::ack::make_ack(raw_packet);

  EXPECT_FALSE(ack_opt.has_value());
}
