#include "lp_proto.h"

#include <gtest/gtest.h>

using piwo::underlay_cast;

namespace
{

constexpr size_t LP_REJECT_SIZE         = 0x1;
constexpr size_t LP_REJECT_HUGE_SIZE    = 0x753;
constexpr size_t LP_REJECT_BAD_SIZE     = 0x0;
constexpr size_t COMMON_PACKET_TYPE_POS = 0x0;
constexpr size_t LP_REJECT_TYPE         = 0x0;

} // namespace

TEST(LpRejectTest, lp_reject_packet_is_successfully_encoded_and_decoded)
{
  piwo::lp::pipe_byte_t buffer[LP_REJECT_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_REJECT_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_reject_builder::make_lp_reject_builder(raw_packet);

  ASSERT_TRUE(builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_REJECT_TYPE });

  piwo::lp::lp_reject reject_packet(builder_opt.value());

  EXPECT_EQ(reject_packet.size(), LP_REJECT_SIZE);

  EXPECT_EQ(reject_packet.cdata(), buffer);
  EXPECT_EQ(reject_packet.data(), buffer);
  EXPECT_EQ(reject_packet.begin(), buffer);
  EXPECT_EQ(reject_packet.cbegin(), buffer);
  EXPECT_EQ(reject_packet.end(), buffer + LP_REJECT_SIZE);
  EXPECT_EQ(reject_packet.cend(), buffer + LP_REJECT_SIZE);

  EXPECT_EQ(reject_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_REJECT_TYPE });

  ASSERT_TRUE(reject_packet.get_type().has_value());
  EXPECT_EQ(underlay_cast(reject_packet.get_type().value()), LP_REJECT_TYPE);
}

TEST(
  LpRejectTest,
  lp_reject_packet_is_successfully_encoded_and_decoded_with_buffer_bigger_than_necessary)
{
  static_assert(LP_REJECT_HUGE_SIZE > LP_REJECT_SIZE);

  piwo::lp::pipe_byte_t buffer[LP_REJECT_HUGE_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_REJECT_HUGE_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_reject_builder::make_lp_reject_builder(raw_packet);

  ASSERT_TRUE(builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_REJECT_TYPE });

  piwo::lp::lp_reject reject_packet(builder_opt.value());

  EXPECT_EQ(reject_packet.size(), LP_REJECT_SIZE);

  EXPECT_EQ(reject_packet.cdata(), buffer);
  EXPECT_EQ(reject_packet.data(), buffer);
  EXPECT_EQ(reject_packet.begin(), buffer);
  EXPECT_EQ(reject_packet.cbegin(), buffer);
  EXPECT_EQ(reject_packet.end(), buffer + LP_REJECT_SIZE);
  EXPECT_EQ(reject_packet.cend(), buffer + LP_REJECT_SIZE);

  EXPECT_EQ(reject_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_REJECT_TYPE });

  ASSERT_TRUE(reject_packet.get_type().has_value());
  EXPECT_EQ(underlay_cast(reject_packet.get_type().value()), LP_REJECT_TYPE);
}

TEST(LpRejectTest,
     lp_reject_packet_creation_fails_with_insufficient_packet_buffer)
{
  piwo::lp::pipe_byte_t buffer[LP_REJECT_BAD_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_REJECT_BAD_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_reject_builder::make_lp_reject_builder(raw_packet);

  ASSERT_FALSE(builder_opt.has_value());
}
