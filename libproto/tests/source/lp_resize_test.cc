#include "lp_proto.h"

#include <gtest/gtest.h>

using piwo::underlay_cast;

namespace
{
constexpr int MEM_CMP_SUCCESS = 0;

constexpr size_t LP_RESIZE_SIZE         = 0x9;
constexpr size_t LP_RESIZE_HUGE_SIZE    = 0x1234;
constexpr size_t LP_RESIZE_BAD_SIZE     = 0x8;
constexpr size_t COMMON_PACKET_TYPE_POS = 0x0;
constexpr size_t LP_RESIZE_WIDTH_POS    = 0x1;
constexpr size_t LP_RESIZE_HEIGHT_POS   = 0x5;
constexpr size_t LP_RESIZE_TYPE         = 0x4;

constexpr size_t LP_RESIZE_WIDTH_TEST_VALUE  = 420;
constexpr size_t LP_RESIZE_HEIGHT_TEST_VALUE = 69;

} // namespace

static void
lp_resize_encode_decode(piwo::lp::pipe_byte_t* buffer, size_t buffer_size)
{
  piwo::lp::raw_packet raw_packet(buffer, buffer_size);

  std::expected builder_opt =
    piwo::lp::lp_resize_builder::make_lp_resize_builder(raw_packet);

  ASSERT_TRUE(builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_RESIZE_TYPE });

  auto builder = builder_opt.value();
  builder.set_width(LP_RESIZE_WIDTH_TEST_VALUE);
  builder.set_height(LP_RESIZE_HEIGHT_TEST_VALUE);

  piwo::lp::lp_resize resize_packet(builder);

  EXPECT_EQ(resize_packet.size(), LP_RESIZE_SIZE);

  EXPECT_EQ(resize_packet.cdata(), buffer);
  EXPECT_EQ(resize_packet.data(), buffer);
  EXPECT_EQ(resize_packet.begin(), buffer);
  EXPECT_EQ(resize_packet.cbegin(), buffer);
  EXPECT_EQ(resize_packet.end(), buffer + LP_RESIZE_SIZE);
  EXPECT_EQ(resize_packet.cend(), buffer + LP_RESIZE_SIZE);

  EXPECT_EQ(resize_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_RESIZE_TYPE });

  ASSERT_TRUE(resize_packet.get_type().has_value());
  EXPECT_EQ(underlay_cast(resize_packet.get_type().value()), LP_RESIZE_TYPE);

  piwo::lp::raw_packet decode_packet(
    static_cast<piwo::lp::raw_packet>(resize_packet));
  ASSERT_TRUE(decode_packet.get_type().has_value());

  const auto decoded_type = decode_packet.get_type().value();
  EXPECT_EQ(underlay_cast(decoded_type), LP_RESIZE_TYPE);

  std::expected decoded_lp_resize_opt =
    piwo::lp::lp_resize::make_lp_resize(decode_packet);
  ASSERT_TRUE(decoded_lp_resize_opt.has_value());

  const auto decoded_lp_resize = decoded_lp_resize_opt.value();
  EXPECT_EQ(decoded_lp_resize.get_width(), LP_RESIZE_WIDTH_TEST_VALUE);
  EXPECT_EQ(decoded_lp_resize.get_height(), LP_RESIZE_HEIGHT_TEST_VALUE);
}

TEST(LpResizeTest, lp_resize_packet_is_successfully_encoded_and_decoded)
{
  piwo::lp::pipe_byte_t buffer[LP_RESIZE_SIZE];

  lp_resize_encode_decode(buffer, LP_RESIZE_SIZE);
}

TEST(
  LpResizeTest,
  lp_resize_packet_is_successfully_encoded_and_decoded_with_buffer_bigger_than_necessary)
{
  static_assert(LP_RESIZE_HUGE_SIZE > LP_RESIZE_SIZE);

  piwo::lp::pipe_byte_t buffer[LP_RESIZE_HUGE_SIZE];
  lp_resize_encode_decode(buffer, LP_RESIZE_HUGE_SIZE);
}

TEST(LpResizeTest,
     lp_resize_packet_creation_fails_with_insufficient_packet_buffer)
{
  piwo::lp::pipe_byte_t buffer[LP_RESIZE_BAD_SIZE];
  piwo::lp::raw_packet  raw_packet(buffer, LP_RESIZE_BAD_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_resize_builder::make_lp_resize_builder(raw_packet);

  ASSERT_FALSE(builder_opt.has_value());
}

TEST(LpResizeTest,
     lp_resize_packet_decode_returns_more_data_on_not_complete_packet)
{
  piwo::lp::pipe_byte_t buffer[LP_RESIZE_SIZE];
  piwo::lp::raw_packet  raw_packet_good(buffer, LP_RESIZE_SIZE);

  static_assert(LP_RESIZE_SIZE >= 1);
  piwo::lp::raw_packet raw_packet_bad1(buffer, LP_RESIZE_SIZE - 1);

  static_assert(LP_RESIZE_SIZE >= 2);
  piwo::lp::raw_packet raw_packet_bad2(buffer, LP_RESIZE_SIZE - 2);

  static_assert(LP_RESIZE_SIZE >= 3);
  piwo::lp::raw_packet raw_packet_bad3(buffer, LP_RESIZE_SIZE - 3);

  // First build correct packet.
  std::expected builder_opt =
    piwo::lp::lp_resize_builder::make_lp_resize_builder(raw_packet_good);

  ASSERT_TRUE(builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_RESIZE_TYPE });

  auto builder = builder_opt.value();
  builder.set_width(LP_RESIZE_WIDTH_TEST_VALUE);
  builder.set_height(LP_RESIZE_HEIGHT_TEST_VALUE);

  piwo::lp::lp_resize resize_packet(builder);

  EXPECT_EQ(resize_packet.size(), LP_RESIZE_SIZE);

  EXPECT_EQ(resize_packet.cdata(), buffer);
  EXPECT_EQ(resize_packet.data(), buffer);
  EXPECT_EQ(resize_packet.begin(), buffer);
  EXPECT_EQ(resize_packet.cbegin(), buffer);
  EXPECT_EQ(resize_packet.end(), buffer + LP_RESIZE_SIZE);
  EXPECT_EQ(resize_packet.cend(), buffer + LP_RESIZE_SIZE);

  EXPECT_EQ(resize_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_RESIZE_TYPE });

  ASSERT_TRUE(resize_packet.get_type().has_value());
  EXPECT_EQ(underlay_cast(resize_packet.get_type().value()), LP_RESIZE_TYPE);

  // Try parsing the packet with insufficient amount of data in the packet.
  // 3 bytes off
  piwo::lp::raw_packet decode_packet(
    static_cast<piwo::lp::raw_packet>(raw_packet_bad3));
  ASSERT_TRUE(decode_packet.get_type().has_value());

  auto decoded_type = decode_packet.get_type().value();
  EXPECT_EQ(underlay_cast(decoded_type), LP_RESIZE_TYPE);

  std::expected decoded_lp_resize_opt =
    piwo::lp::lp_resize::make_lp_resize(decode_packet);
  ASSERT_FALSE(decoded_lp_resize_opt.has_value());
  EXPECT_EQ(decoded_lp_resize_opt.error(),
            piwo::lp::lp_packet_error::MORE_DATA);

  // 2 bytes off
  decode_packet =
    piwo::lp::raw_packet(static_cast<piwo::lp::raw_packet>(raw_packet_bad2));
  ASSERT_TRUE(decode_packet.get_type().has_value());

  decoded_type = decode_packet.get_type().value();
  EXPECT_EQ(underlay_cast(decoded_type), LP_RESIZE_TYPE);

  decoded_lp_resize_opt = piwo::lp::lp_resize::make_lp_resize(decode_packet);
  ASSERT_FALSE(decoded_lp_resize_opt.has_value());
  EXPECT_EQ(decoded_lp_resize_opt.error(),
            piwo::lp::lp_packet_error::MORE_DATA);

  // 1 byte off
  decode_packet =
    piwo::lp::raw_packet(static_cast<piwo::lp::raw_packet>(raw_packet_bad1));
  ASSERT_TRUE(decode_packet.get_type().has_value());

  decoded_type = decode_packet.get_type().value();
  EXPECT_EQ(underlay_cast(decoded_type), LP_RESIZE_TYPE);

  decoded_lp_resize_opt = piwo::lp::lp_resize::make_lp_resize(decode_packet);
  ASSERT_FALSE(decoded_lp_resize_opt.has_value());
  EXPECT_EQ(decoded_lp_resize_opt.error(),
            piwo::lp::lp_packet_error::MORE_DATA);

  // all bytes arrived
  decode_packet =
    piwo::lp::raw_packet(static_cast<piwo::lp::raw_packet>(raw_packet_good));
  ASSERT_TRUE(decode_packet.get_type().has_value());

  decoded_type = decode_packet.get_type().value();
  EXPECT_EQ(underlay_cast(decoded_type), LP_RESIZE_TYPE);

  decoded_lp_resize_opt = piwo::lp::lp_resize::make_lp_resize(decode_packet);
  ASSERT_TRUE(decoded_lp_resize_opt.has_value());

  const auto decoded_lp_resize = decoded_lp_resize_opt.value();
  EXPECT_EQ(decoded_lp_resize.get_width(), LP_RESIZE_WIDTH_TEST_VALUE);
  EXPECT_EQ(decoded_lp_resize.get_height(), LP_RESIZE_HEIGHT_TEST_VALUE);
}
