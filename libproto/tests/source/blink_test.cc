#include "proto.h"

#include <gtest/gtest.h>

#include <array>

namespace
{
constexpr uint8_t COMMON_PACKET_TYPE_POS = 0;
constexpr uint8_t COMMON_PACKET_SIZE_POS = 1;

constexpr size_t  BLINK_SIZE    = 14;
constexpr size_t  UID_SIZE      = 12;
constexpr uint8_t BLINK_TYPE    = 0x5;
constexpr uint8_t BLINK_UID_POS = 2;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(BlinkTest, build_and_make_blink_success)
{
  piwo::net_byte_t buffer[BLINK_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  piwo::uid        uid{ reinterpret_cast<uint8_t*>(uid_buffer.data()) };

  piwo::raw_packet raw_packet(buffer, BLINK_SIZE);

  auto blink_builder_opt = piwo::blink_builder::make_blink_builder(raw_packet);

  ASSERT_TRUE(blink_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS], piwo::net_byte_t{ BLINK_TYPE });

  auto blink_builder = *blink_builder_opt;
  blink_builder.set_uid(uid);

  piwo::blink blink_packet(blink_builder);

  // Pointers must be the same
  EXPECT_EQ(blink_packet.cdata(), buffer);
  EXPECT_EQ(blink_packet.data(), buffer);
  EXPECT_EQ(blink_packet.cdata(), buffer);
  EXPECT_EQ(blink_packet.begin(), buffer);
  EXPECT_EQ(blink_packet.cbegin(), buffer);
  EXPECT_EQ(blink_packet.end(), buffer + BLINK_SIZE);
  EXPECT_EQ(blink_packet.cend(), buffer + BLINK_SIZE);

  EXPECT_EQ(blink_packet.size(), BLINK_SIZE);

  ASSERT_TRUE(blink_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(blink_packet.get_type().value()), BLINK_TYPE);
  EXPECT_EQ(blink_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ BLINK_TYPE });
  EXPECT_EQ(blink_packet.data()[COMMON_PACKET_SIZE_POS],
            piwo::net_byte_t{ BLINK_SIZE });

  EXPECT_EQ(blink_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> blink_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(blink_packet.data() + BLINK_UID_POS),
                UID_SIZE, blink_packet_uid_data.data());

  EXPECT_EQ(blink_packet_uid_data, uid_buffer);
}

TEST(BlinkTest, make_blink_success)
{
  piwo::net_byte_t buffer[BLINK_SIZE];
   std::array<uint8_t, UID_SIZE> uid_buffer =
     {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};

  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ BLINK_TYPE };
  buffer[COMMON_PACKET_SIZE_POS] = piwo::net_byte_t{ BLINK_SIZE };
  std::copy(reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data()),
            reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data() + UID_SIZE),
            buffer + BLINK_UID_POS);

  piwo::raw_packet raw_packet(buffer, BLINK_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), BLINK_TYPE);

  auto blink_packet_opt = piwo::blink::make_blink(raw_packet);

  ASSERT_TRUE(blink_packet_opt.has_value());

  auto blink_packet = blink_packet_opt.value();

  // Pointers must be the same
  EXPECT_EQ(blink_packet.cdata(), buffer);
  EXPECT_EQ(blink_packet.data(), buffer);
  EXPECT_EQ(blink_packet.begin(), buffer);
  EXPECT_EQ(blink_packet.cbegin(), buffer);
  EXPECT_EQ(blink_packet.end(), buffer + BLINK_SIZE);
  EXPECT_EQ(blink_packet.cend(), buffer + BLINK_SIZE);

  EXPECT_EQ(blink_packet.size(), BLINK_SIZE);

  ASSERT_TRUE(blink_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(blink_packet.get_type().value()), BLINK_TYPE);
  EXPECT_EQ(blink_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ BLINK_TYPE });
  EXPECT_EQ(blink_packet.data()[COMMON_PACKET_SIZE_POS],
            piwo::net_byte_t{ BLINK_SIZE });

  EXPECT_EQ(blink_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> blink_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(blink_packet.data() + BLINK_UID_POS),
                UID_SIZE, blink_packet_uid_data.data());

  EXPECT_EQ(blink_packet_uid_data, uid_buffer);
}

TEST(BlinkTest, make_blink_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = BLINK_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto blink_builder_opt = piwo::blink_builder::make_blink_builder(raw_packet);

  EXPECT_FALSE(blink_builder_opt.has_value());
}

TEST(BlinkTest, make_blink_fail_buffer_too_small)
{
  auto             too_small_buffer_size = BLINK_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto blink_opt = piwo::blink::make_blink(raw_packet);

  EXPECT_FALSE(blink_opt.has_value());
}

TEST(BlinkTest, make_blink_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[BLINK_SIZE];
  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ UNKNOWN_FRAME };

  piwo::raw_packet raw_packet(buffer, BLINK_SIZE);

  auto blink_opt = piwo::blink::make_blink(raw_packet);

  EXPECT_FALSE(blink_opt.has_value());
}
