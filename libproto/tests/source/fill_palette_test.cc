#include "proto.h"

#include <gtest/gtest.h>

#include <array>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  FILL_PALETTE_SIZE               = 5;
constexpr uint8_t FILL_PALETTE_TYPE               = 0x0B;
constexpr uint8_t FILL_PALETTE_PALETTE_INDEX_POS  = 2;
constexpr uint8_t FILL_PALETTE_PALETTE_OFFSET_POS = 3;
constexpr uint8_t FILL_PALETTE_PALETTE_DATA_POS   = 5;

constexpr uint8_t  FILL_PALETTE_PALETTE_INDEX_TEST_VALUE  = 15;
constexpr uint16_t FILL_PALETTE_PALETTE_OFFSET_TEST_VALUE = 4321;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(FillPaletteTest, build_and_make_fill_palette_success)
{
  const size_t            buffer_size                     = 25;
  piwo::net_byte_t        buffer[buffer_size];
  uint8_t                 fill_palette_data_test_value[]  = { "QWERTYUIOP" };
  uint8_t                 fill_palette_data_test_value2[] = { "0123456789" };
  std::array<uint8_t, 20> expected_fill_palette_data      =
   { 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P',
     '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

  piwo::raw_packet raw_packet(buffer, buffer_size);

  auto fill_palette_builder_opt =
    piwo::fill_palette_builder::make_fill_palette_builder(raw_packet);

  ASSERT_TRUE(fill_palette_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ FILL_PALETTE_TYPE });

  auto fill_palette_builder = *fill_palette_builder_opt;
  fill_palette_builder.set_palette_index(FILL_PALETTE_PALETTE_INDEX_TEST_VALUE);
  fill_palette_builder.set_palette_offset(
    FILL_PALETTE_PALETTE_OFFSET_TEST_VALUE);
  EXPECT_TRUE(fill_palette_builder.append_data(
    reinterpret_cast<piwo::net_byte_t*>(fill_palette_data_test_value), 10));
  EXPECT_TRUE(fill_palette_builder.append_data(
    reinterpret_cast<piwo::net_byte_t*>(fill_palette_data_test_value2), 10));

  piwo::fill_palette fill_palette_packet(fill_palette_builder);

  size_t expected_packet_size = FILL_PALETTE_SIZE + 20;

  // Pointers must be the same
  EXPECT_EQ(fill_palette_packet.cdata(), buffer);
  EXPECT_EQ(fill_palette_packet.data(), buffer);
  EXPECT_EQ(fill_palette_packet.cdata(), buffer);
  EXPECT_EQ(fill_palette_packet.begin(), buffer);
  EXPECT_EQ(fill_palette_packet.cbegin(), buffer);
  EXPECT_EQ(fill_palette_packet.end(), buffer + expected_packet_size);
  EXPECT_EQ(fill_palette_packet.cend(), buffer + expected_packet_size);

  EXPECT_EQ(fill_palette_packet.size(), expected_packet_size);
  EXPECT_EQ(fill_palette_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ static_cast<uint8_t>(expected_packet_size) });

  ASSERT_TRUE(fill_palette_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(fill_palette_packet.get_type().value()),
            FILL_PALETTE_TYPE);
  EXPECT_EQ(fill_palette_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ FILL_PALETTE_TYPE });

  EXPECT_EQ(fill_palette_packet.get_palette_index(),
            FILL_PALETTE_PALETTE_INDEX_TEST_VALUE);
  EXPECT_EQ(fill_palette_packet.data()[FILL_PALETTE_PALETTE_INDEX_POS],
            piwo::net_byte_t{ FILL_PALETTE_PALETTE_INDEX_TEST_VALUE });

  EXPECT_EQ(fill_palette_packet.get_palette_offset(),
            FILL_PALETTE_PALETTE_OFFSET_TEST_VALUE);

  EXPECT_EQ(fill_palette_packet.get_payload().size(), 20);

  std::array<uint8_t, 20> payload_data;
  memcpy(payload_data.data(), fill_palette_packet.get_payload().data(), 20);

  EXPECT_EQ(expected_fill_palette_data, payload_data);
}

TEST(FillPaletteTest, make_fill_palette_success)
{
  const size_t     final_packet_size             = 25;
  piwo::net_byte_t buffer[final_packet_size];
  std::array<uint8_t, 20> fill_palette_data      =
   { 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P',
     '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };


  uint16_t palette_offset = htole16(FILL_PALETTE_PALETTE_OFFSET_TEST_VALUE);

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ final_packet_size };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ FILL_PALETTE_TYPE };
  buffer[FILL_PALETTE_PALETTE_INDEX_POS] =
    piwo::net_byte_t{ FILL_PALETTE_PALETTE_INDEX_TEST_VALUE };

  std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&palette_offset),
              sizeof(palette_offset),
              buffer + FILL_PALETTE_PALETTE_OFFSET_POS);

  std::copy_n(reinterpret_cast<piwo::net_byte_t*>(fill_palette_data.data()),
              20,
              buffer + FILL_PALETTE_PALETTE_DATA_POS);

  piwo::raw_packet raw_packet(buffer, final_packet_size);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), FILL_PALETTE_TYPE);

  auto fill_palette_packet_opt =
    piwo::fill_palette::make_fill_palette(raw_packet);

  ASSERT_TRUE(fill_palette_packet_opt.has_value());

  auto fill_palette_packet = fill_palette_packet_opt.value();

  // Pointers must be the same
  EXPECT_EQ(fill_palette_packet.cdata(), buffer);
  EXPECT_EQ(fill_palette_packet.data(), buffer);
  EXPECT_EQ(fill_palette_packet.begin(), buffer);
  EXPECT_EQ(fill_palette_packet.cbegin(), buffer);
  EXPECT_EQ(fill_palette_packet.end(), buffer + final_packet_size);
  EXPECT_EQ(fill_palette_packet.cend(), buffer + final_packet_size);

  EXPECT_EQ(fill_palette_packet.size(), final_packet_size);
  EXPECT_EQ(fill_palette_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ final_packet_size });

  ASSERT_TRUE(fill_palette_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(fill_palette_packet.get_type().value()),
            FILL_PALETTE_TYPE);
  EXPECT_EQ(fill_palette_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ FILL_PALETTE_TYPE });

  EXPECT_EQ(fill_palette_packet.get_palette_index(),
            FILL_PALETTE_PALETTE_INDEX_TEST_VALUE);
  EXPECT_EQ(fill_palette_packet.data()[FILL_PALETTE_PALETTE_INDEX_POS],
            piwo::net_byte_t{ FILL_PALETTE_PALETTE_INDEX_TEST_VALUE });

  EXPECT_EQ(fill_palette_packet.get_palette_offset(),
            FILL_PALETTE_PALETTE_OFFSET_TEST_VALUE);

  EXPECT_EQ(fill_palette_packet.get_payload().size(), 20);

  std::array<uint8_t, 20> payload_data;
  memcpy(payload_data.data(), fill_palette_packet.get_payload().data(), 20);

  EXPECT_EQ(fill_palette_data, payload_data);
}

TEST(FillPaletteTest, append_data_fail_buffer_too_small)
{
  const size_t     buffer_size = 24;
  piwo::net_byte_t buffer[buffer_size];
  uint8_t          fill_palette_data_test_value[]  = { "QWERTYUIOP" };
  uint8_t          fill_palette_data_test_value2[] = { "0123456789" };
  uint8_t          expected_fill_palette_data[]    = { "QWERTYUIOP0123456789" };

  piwo::raw_packet raw_packet(buffer, buffer_size);

  auto fill_palette_builder_opt =
    piwo::fill_palette_builder::make_fill_palette_builder(raw_packet);

  ASSERT_TRUE(fill_palette_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ FILL_PALETTE_TYPE });

  auto fill_palette_builder = *fill_palette_builder_opt;
  fill_palette_builder.set_palette_index(FILL_PALETTE_PALETTE_INDEX_TEST_VALUE);
  fill_palette_builder.set_palette_offset(
    FILL_PALETTE_PALETTE_OFFSET_TEST_VALUE);

  EXPECT_TRUE(fill_palette_builder.append_data(
    reinterpret_cast<piwo::net_byte_t*>(fill_palette_data_test_value), 10));
  EXPECT_FALSE(fill_palette_builder.append_data(
    reinterpret_cast<piwo::net_byte_t*>(fill_palette_data_test_value2), 10));
}

TEST(FillPaletteTest, make_fill_palette_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = FILL_PALETTE_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto fill_palette_builder_opt =
    piwo::fill_palette_builder::make_fill_palette_builder(raw_packet);

  EXPECT_FALSE(fill_palette_builder_opt.has_value());
}

TEST(FillPaletteTest, make_fill_palette_fail_buffer_too_small)
{
  auto             too_small_buffer_size = FILL_PALETTE_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto fill_palette_opt = piwo::fill_palette::make_fill_palette(raw_packet);

  EXPECT_FALSE(fill_palette_opt.has_value());
}

TEST(FillPaletteTest, make_fill_palette_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = FILL_PALETTE_SIZE + 1;
  piwo::net_byte_t buffer[FILL_PALETTE_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, FILL_PALETTE_SIZE);

  auto fill_palette_opt = piwo::fill_palette::make_fill_palette(raw_packet);

  EXPECT_FALSE(fill_palette_opt.has_value());

  inconsistent_frame_length = FILL_PALETTE_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  auto fill_palette_opt2 = piwo::fill_palette::make_fill_palette(raw_packet);

  EXPECT_FALSE(fill_palette_opt2.has_value());
}

TEST(FillPaletteTest, make_fill_palette_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[FILL_PALETTE_SIZE];
  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ UNKNOWN_FRAME };

  piwo::raw_packet raw_packet(buffer, FILL_PALETTE_SIZE);

  auto fill_palette_opt = piwo::fill_palette::make_fill_palette(raw_packet);

  EXPECT_FALSE(fill_palette_opt.has_value());
}
