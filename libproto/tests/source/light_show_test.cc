#include "proto.h"

#include <gtest/gtest.h>

namespace
{
constexpr int MEM_CMP_SUCCESS = 0;

constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  LIGHTSHOW_MINIMUM_SIZE    = 7;
constexpr size_t  LIGHTSHOW_MAXIMUM_SIZE    = 119;
constexpr size_t  LIGHTSHOW_SIZE            = 4;
constexpr uint8_t LIGHTSHOW_TYPE            = 0x40;
constexpr uint8_t LIGHTSHOW_TTF_POS         = 2;
constexpr uint8_t LIGHTSHOW_FIRST_LA_POS    = 3;
constexpr uint8_t LIGHTSHOW_FIRST_COLOR_POS = 4;

constexpr uint8_t LIGHTSHOW_TTF_TEST_VALUE      = 15;
constexpr uint8_t LIGHTSHOW_FIRST_LA_TEST_VALUE = 9;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(LightShowTest, build_and_make_lightshow_success)
{
  constexpr size_t   colors_count         = 5;
  piwo::packed_color colors[colors_count] = { { 10, 20, 30 },
                                              { 11, 21, 32 },
                                              { 12, 22, 32 },
                                              { 13, 23, 33 },
                                              { 14, 24, 34 } };

  piwo::net_byte_t buffer[LIGHTSHOW_SIZE + (colors_count * 3)];

  size_t expected_lightshow_size =
    LIGHTSHOW_SIZE + (colors_count * piwo::packed_color::size);

  piwo::raw_packet raw_packet(buffer, expected_lightshow_size);

  auto lightshow_builder_opt =
    piwo::lightshow_builder::make_lightshow_builder(raw_packet);

  ASSERT_TRUE(lightshow_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS], piwo::net_byte_t{ LIGHTSHOW_TYPE });

  auto lightshow_builder = *lightshow_builder_opt;
  lightshow_builder.set_ttf(LIGHTSHOW_TTF_TEST_VALUE);
  lightshow_builder.set_first_la(LIGHTSHOW_FIRST_LA_TEST_VALUE);

  for (piwo::packed_color color : colors)
  {
    EXPECT_TRUE(lightshow_builder.add_color(color));
  }

  piwo::lightshow lightshow_packet(lightshow_builder);

  // Pointers must be the same
  EXPECT_EQ(lightshow_packet.cdata(), buffer);
  EXPECT_EQ(lightshow_packet.data(), buffer);
  EXPECT_EQ(lightshow_packet.cdata(), buffer);
  EXPECT_EQ(lightshow_packet.begin(), buffer);
  EXPECT_EQ(lightshow_packet.cbegin(), buffer);
  EXPECT_EQ(lightshow_packet.end(), buffer + expected_lightshow_size);
  EXPECT_EQ(lightshow_packet.cend(), buffer + expected_lightshow_size);

  EXPECT_EQ(lightshow_packet.size(), expected_lightshow_size);
  EXPECT_EQ(lightshow_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ static_cast<uint8_t>(expected_lightshow_size) });

  ASSERT_TRUE(lightshow_packet.get_type().has_value());

  EXPECT_EQ(lightshow_packet.get_colors_count(), colors_count);

  EXPECT_EQ(underlay_cast(lightshow_packet.get_type().value()), LIGHTSHOW_TYPE);
  EXPECT_EQ(lightshow_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ LIGHTSHOW_TYPE });

  EXPECT_EQ(lightshow_packet.get_ttf(), LIGHTSHOW_TTF_TEST_VALUE);
  EXPECT_EQ(lightshow_packet.data()[LIGHTSHOW_TTF_POS],
            piwo::net_byte_t{ LIGHTSHOW_TTF_TEST_VALUE });

  size_t color_index = 0;

  for (piwo::packed_color color : colors)
  {
    auto color_from_frame_opt = lightshow_packet.get_color(color_index);

    ASSERT_TRUE(color_from_frame_opt.has_value());
    EXPECT_EQ(color_from_frame_opt->get_red(), color.get_red());
    EXPECT_EQ(color_from_frame_opt->get_green(), color.get_green());
    EXPECT_EQ(color_from_frame_opt->get_blue(), color.get_blue());

    EXPECT_EQ(buffer[LIGHTSHOW_FIRST_COLOR_POS + (color_index * 3)],
              piwo::net_byte_t{ color.get_red() });
    EXPECT_EQ(buffer[LIGHTSHOW_FIRST_COLOR_POS + (color_index * 3) + 1],
              piwo::net_byte_t{ color.get_green() });
    EXPECT_EQ(buffer[LIGHTSHOW_FIRST_COLOR_POS + (color_index * 3) + 2],
              piwo::net_byte_t{ color.get_blue() });

    ++color_index;
  }
}

TEST(LightShowTest, make_lightshow_success)
{
  constexpr size_t colors_count = 5;
  constexpr size_t expected_lightshow_size =
    LIGHTSHOW_SIZE + (colors_count * piwo::packed_color::size);
  piwo::packed_color colors[colors_count] = {
    { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 10, 11, 12 }, { 13, 14, 15 }
  };

  piwo::net_byte_t buffer[expected_lightshow_size];

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ expected_lightshow_size };
  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ LIGHTSHOW_TYPE };
  buffer[LIGHTSHOW_TTF_POS]      = piwo::net_byte_t{ LIGHTSHOW_TTF_TEST_VALUE };
  buffer[LIGHTSHOW_FIRST_LA_POS] =
    piwo::net_byte_t{ LIGHTSHOW_FIRST_LA_TEST_VALUE };

  size_t color_index = 0;

  for (piwo::packed_color color : colors)
  {
    buffer[LIGHTSHOW_FIRST_COLOR_POS + (color_index * 3)] =
      piwo::net_byte_t{ color.get_red() };
    buffer[LIGHTSHOW_FIRST_COLOR_POS + (color_index * 3) + 1] =
      piwo::net_byte_t{ color.get_green() };
    buffer[LIGHTSHOW_FIRST_COLOR_POS + (color_index * 3) + 2] =
      piwo::net_byte_t{ color.get_blue() };

    ++color_index;
  }

  piwo::raw_packet raw_packet(buffer, expected_lightshow_size);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), LIGHTSHOW_TYPE);

  auto lightshow_packet_opt = piwo::lightshow::make_lightshow(raw_packet);

  ASSERT_TRUE(lightshow_packet_opt.has_value());

  auto lightshow_packet = lightshow_packet_opt.value();

  // Pointers must be the same
  EXPECT_EQ(lightshow_packet.cdata(), buffer);
  EXPECT_EQ(lightshow_packet.data(), buffer);
  EXPECT_EQ(lightshow_packet.cdata(), buffer);
  EXPECT_EQ(lightshow_packet.begin(), buffer);
  EXPECT_EQ(lightshow_packet.cbegin(), buffer);
  EXPECT_EQ(lightshow_packet.end(), buffer + expected_lightshow_size);
  EXPECT_EQ(lightshow_packet.cend(), buffer + expected_lightshow_size);

  EXPECT_EQ(lightshow_packet.size(), expected_lightshow_size);
  EXPECT_EQ(lightshow_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ expected_lightshow_size });

  ASSERT_TRUE(lightshow_packet.get_type().has_value());

  EXPECT_EQ(lightshow_packet.get_colors_count(), colors_count);

  EXPECT_EQ(underlay_cast(lightshow_packet.get_type().value()), LIGHTSHOW_TYPE);
  EXPECT_EQ(lightshow_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ LIGHTSHOW_TYPE });

  EXPECT_EQ(lightshow_packet.get_ttf(), LIGHTSHOW_TTF_TEST_VALUE);
  EXPECT_EQ(lightshow_packet.data()[LIGHTSHOW_TTF_POS],
            piwo::net_byte_t{ LIGHTSHOW_TTF_TEST_VALUE });

  color_index = 0;

  for (piwo::packed_color color : colors)
  {
    auto color_from_frame_opt = lightshow_packet.get_color(color_index);

    ASSERT_TRUE(color_from_frame_opt.has_value());
    EXPECT_EQ(color_from_frame_opt->get_red(), color.get_red());
    EXPECT_EQ(color_from_frame_opt->get_green(), color.get_green());
    EXPECT_EQ(color_from_frame_opt->get_blue(), color.get_blue());

    EXPECT_EQ(buffer[LIGHTSHOW_FIRST_COLOR_POS + (color_index * 3)],
              piwo::net_byte_t{ color.get_red() });
    EXPECT_EQ(buffer[LIGHTSHOW_FIRST_COLOR_POS + (color_index * 3) + 1],
              piwo::net_byte_t{ color.get_green() });
    EXPECT_EQ(buffer[LIGHTSHOW_FIRST_COLOR_POS + (color_index * 3) + 2],
              piwo::net_byte_t{ color.get_blue() });

    ++color_index;
  }
}

TEST(LightShowTest, add_color_fail_buffer_too_small)
{
  constexpr size_t colors_count = 2;
  auto too_small_buffer_size    = LIGHTSHOW_SIZE + (colors_count * 3) - 1;

  piwo::packed_color colors[colors_count] = { { 10, 20, 30 }, { 11, 21, 32 } };

  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto lightshow_builder_opt =
    piwo::lightshow_builder::make_lightshow_builder(raw_packet);

  ASSERT_TRUE(lightshow_builder_opt.has_value());
  EXPECT_TRUE(lightshow_builder_opt->add_color(colors[0]));
  EXPECT_FALSE(lightshow_builder_opt->add_color(colors[1]));
}

TEST(LightShowTest, make_lightshow_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = LIGHTSHOW_MINIMUM_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto lightshow_builder_opt =
    piwo::lightshow_builder::make_lightshow_builder(raw_packet);

  EXPECT_FALSE(lightshow_builder_opt.has_value());
}

TEST(LightShowTest, make_lightshow_fail_buffer_too_small)
{
  const auto       too_small_buffer_size = LIGHTSHOW_MINIMUM_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ LIGHTSHOW_TYPE };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ too_small_buffer_size };

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto lightshow_opt = piwo::lightshow::make_lightshow(raw_packet);

  EXPECT_FALSE(lightshow_opt.has_value());
}

TEST(LightShowTest, make_lightshow_fail_buffer_too_large)
{
  const auto       too_large_buffer_size = LIGHTSHOW_MAXIMUM_SIZE + 1;
  piwo::net_byte_t buffer[too_large_buffer_size];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ LIGHTSHOW_TYPE };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ too_large_buffer_size };

  piwo::raw_packet raw_packet(buffer, too_large_buffer_size);

  auto lightshow_opt = piwo::lightshow::make_lightshow(raw_packet);

  EXPECT_FALSE(lightshow_opt.has_value());
}

TEST(LightShowTest, make_lightshow_fail_frame_length_greater_than_buffer_size)
{
  piwo::net_byte_t buffer[LIGHTSHOW_MINIMUM_SIZE - 1];
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ LIGHTSHOW_MINIMUM_SIZE };

  piwo::raw_packet raw_packet(buffer, LIGHTSHOW_MINIMUM_SIZE - 1);

  auto lightshow_opt = piwo::lightshow::make_lightshow(raw_packet);

  EXPECT_FALSE(lightshow_opt.has_value());
}

TEST(LightShowTest, make_lightshow_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[LIGHTSHOW_SIZE];
  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ UNKNOWN_FRAME };

  piwo::raw_packet raw_packet(buffer, LIGHTSHOW_SIZE);

  auto lightshow_opt = piwo::lightshow::make_lightshow(raw_packet);

  EXPECT_FALSE(lightshow_opt.has_value());
}
