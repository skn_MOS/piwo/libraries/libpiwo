#include "byteorder.h"

#include "proto.h"

#include <gtest/gtest.h>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr uint8_t FORWARD_W_CID_TYPE              = 0x10;
constexpr size_t  FORWARD_W_CID_CID_SIZE          = 1;
constexpr size_t  FORWARD_W_CID_WITHOUT_DATA_SIZE = 3;
} // namespace

TEST(ForwardWCidTest, build_and_make_forward_w_cid_success)
{
  constexpr size_t buffer_size = 200;
  piwo::net_byte_t buffer[buffer_size];

  constexpr uint8_t test_cid_1 = 10;
  constexpr uint8_t test_cid_2 = 20;
  constexpr uint8_t test_cid_3 = 30;

  piwo::raw_packet raw_packet(buffer, buffer_size);

  // Make forward builder
  std::optional forward_w_cid_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

  ASSERT_TRUE(forward_w_cid_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ FORWARD_W_CID_TYPE });

  auto forward_w_cid_builder = forward_w_cid_builder_opt.value();

  // Add ping packet
  auto next_slot_1 = forward_w_cid_builder.get_next_slot();
  ASSERT_TRUE(next_slot_1.has_value());

  std::optional ping_builder_opt =
    piwo::ping_builder::make_ping_builder(next_slot_1.value());
  ASSERT_TRUE(ping_builder_opt.has_value());

  auto ping_builder = ping_builder_opt.value();

  auto ping = piwo::ping(ping_builder);

  EXPECT_TRUE(forward_w_cid_builder.commit(ping, test_cid_1));

  EXPECT_EQ(buffer[FORWARD_W_CID_WITHOUT_DATA_SIZE],
            piwo::net_byte_t{ test_cid_1 });

  EXPECT_EQ(buffer[FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE +
                   piwo::common_length_pos],
            piwo::net_byte_t{ piwo::ping_length });

  EXPECT_EQ(buffer[FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE +
                   piwo::common_type_pos],
            static_cast<piwo::net_byte_t>(piwo::packet_type::PING));

  // Add status request packet
  auto next_slot_2 = forward_w_cid_builder.get_next_slot();
  ASSERT_TRUE(next_slot_2.has_value());

  std::optional status_request_builder_opt =
    piwo::status_request_builder::make_status_request_builder(
      next_slot_2.value());
  ASSERT_TRUE(status_request_builder_opt.has_value());

  auto status_request_builder = status_request_builder_opt.value();
  auto status_request         = piwo::status_request(status_request_builder);

  EXPECT_TRUE(forward_w_cid_builder.commit(status_request, test_cid_2));

  EXPECT_EQ(buffer[FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE +
                   piwo::ping_length],
            piwo::net_byte_t{ test_cid_2 });

  EXPECT_EQ(
    buffer[FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE +
           ping.size() + FORWARD_W_CID_CID_SIZE + +piwo::common_length_pos],
    piwo::net_byte_t{ piwo::status_req_length });

  EXPECT_EQ(
    buffer[FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE +
           ping.size() + FORWARD_W_CID_CID_SIZE + +piwo::common_type_pos],
    static_cast<piwo::net_byte_t>(piwo::packet_type::STATUS_REQ));

  // Add blink packet
  auto next_slot_3 = forward_w_cid_builder.get_next_slot();
  ASSERT_TRUE(next_slot_3.has_value());

  std::optional blink_builder_opt =
    piwo::blink_builder::make_blink_builder(next_slot_3.value());
  ASSERT_TRUE(blink_builder_opt.has_value());

  auto blink_builder = blink_builder_opt.value();
  auto blink         = piwo::blink(blink_builder);

  EXPECT_TRUE(forward_w_cid_builder.commit(blink, test_cid_3));

  EXPECT_EQ(buffer[FORWARD_W_CID_WITHOUT_DATA_SIZE + piwo::ping_length +
                   piwo::status_req_length + 2 * FORWARD_W_CID_CID_SIZE],
            piwo::net_byte_t{ test_cid_3 });

  EXPECT_EQ(buffer[FORWARD_W_CID_WITHOUT_DATA_SIZE + ping.size() +
                   status_request.size() + piwo::common_length_pos +
                   3 * FORWARD_W_CID_CID_SIZE],
            piwo::net_byte_t{ piwo::blink_length });

  EXPECT_EQ(buffer[FORWARD_W_CID_WITHOUT_DATA_SIZE + ping.size() +
                   status_request.size() + piwo::common_type_pos +
                   3 * FORWARD_W_CID_CID_SIZE],
            static_cast<piwo::net_byte_t>(piwo::packet_type::BLINK));

  // Finalize forward frame
  auto forward_w_cid = piwo::forward_w_cid(forward_w_cid_builder);

  auto expected_packet_length = FORWARD_W_CID_WITHOUT_DATA_SIZE +
                                FORWARD_W_CID_CID_SIZE + ping.size() +
                                FORWARD_W_CID_CID_SIZE + status_request.size() +
                                FORWARD_W_CID_CID_SIZE + blink.size();

  // Pointers must be the same
  EXPECT_EQ(forward_w_cid.cdata(), buffer);
  EXPECT_EQ(forward_w_cid.data(), buffer);
  EXPECT_EQ(forward_w_cid.cdata(), buffer);
  EXPECT_EQ(forward_w_cid.begin(), buffer);
  EXPECT_EQ(forward_w_cid.cbegin(), buffer);
  EXPECT_EQ(forward_w_cid.end(), buffer + expected_packet_length);
  EXPECT_EQ(forward_w_cid.cend(), buffer + expected_packet_length);

  EXPECT_EQ(forward_w_cid.size(), expected_packet_length);

  uint16_t expected_little_endian_packet_length =
    piwo::htole(static_cast<uint16_t>(expected_packet_length));

  uint16_t packet_length_from_buffer;
  memcpy(&packet_length_from_buffer,
        (buffer + piwo::common_length_pos),
        piwo::forward_w_cid_length_size);

  EXPECT_EQ(expected_little_endian_packet_length, packet_length_from_buffer);

  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            static_cast<piwo::net_byte_t>(piwo::packet_type::FORWARD_W_CID));

  std::optional forwart_w_cid_packet_type_opt = forward_w_cid.get_type();
  ASSERT_TRUE(forwart_w_cid_packet_type_opt.has_value());
  EXPECT_EQ(underlay_cast(forwart_w_cid_packet_type_opt.value()),
            FORWARD_W_CID_TYPE);

  // Validate first frame
  std::optional received_packet_1_opt = forward_w_cid.get_next_packet();
  ASSERT_TRUE(received_packet_1_opt.has_value());

  piwo::forward_w_cid_encapsulated_packet_t received_packet_1_data =
    received_packet_1_opt.value();
  EXPECT_EQ(received_packet_1_data.packet.cdata(),
            buffer + FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE);
  EXPECT_EQ(received_packet_1_data.packet.size(), ping.size());
  EXPECT_EQ(received_packet_1_data.cid, test_cid_1);

  // Validate second frame
  std::optional received_packet_2_opt = forward_w_cid.get_next_packet();
  ASSERT_TRUE(received_packet_2_opt.has_value());

  piwo::forward_w_cid_encapsulated_packet_t received_packet_2_data =
    received_packet_2_opt.value();
  EXPECT_EQ(received_packet_2_data.packet.cdata(),
            buffer + FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE +
              piwo::ping_length + FORWARD_W_CID_CID_SIZE);
  EXPECT_EQ(received_packet_2_data.packet.size(), status_request.size());
  EXPECT_EQ(received_packet_2_data.cid, test_cid_2);

  // Validate third frame
  std::optional received_packet_3_opt = forward_w_cid.get_next_packet();
  ASSERT_TRUE(received_packet_3_opt.has_value());

  piwo::forward_w_cid_encapsulated_packet_t received_packet_3_data =
    received_packet_3_opt.value();
  EXPECT_EQ(received_packet_3_data.packet.cdata(),
            buffer + FORWARD_W_CID_WITHOUT_DATA_SIZE + piwo::status_req_length +
              piwo::ping_length + 3 * FORWARD_W_CID_CID_SIZE);
  EXPECT_EQ(received_packet_3_data.packet.size(), blink.size());
  EXPECT_EQ(received_packet_3_data.cid, test_cid_3);

  // No more frames should be available
  std::optional received_packet_4_opt = forward_w_cid.get_next_packet();
  EXPECT_FALSE(received_packet_4_opt.has_value());
}

TEST(ForwardWCidTest,
     properly_creates_make_forward_w_cid_with_size_exceeding_underlaying_buffer_size)
{
  constexpr size_t buffer_size = 1500;
  constexpr size_t piwo_frames_in_buffer_count =
    (buffer_size - FORWARD_W_CID_WITHOUT_DATA_SIZE) /
    (FORWARD_W_CID_CID_SIZE + piwo::ping_length);
  piwo::net_byte_t buffer[buffer_size];

  constexpr uint8_t test_cid_1 = 10;

  piwo::raw_packet raw_packet(buffer, buffer_size);

  // Make forward builder
  std::optional forward_w_cid_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

  ASSERT_TRUE(forward_w_cid_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ FORWARD_W_CID_TYPE });

  auto forward_w_cid_builder = forward_w_cid_builder_opt.value();

  // Add ping packet
  for (size_t i = 0; i < piwo_frames_in_buffer_count; i++)
  {
    auto next_slot = forward_w_cid_builder.get_next_slot();
    ASSERT_TRUE(next_slot.has_value());

    std::optional ping_builder_opt =
      piwo::ping_builder::make_ping_builder(next_slot.value());
    ASSERT_TRUE(ping_builder_opt.has_value());

    auto ping_builder = ping_builder_opt.value();

    auto ping = piwo::ping(ping_builder);

    EXPECT_TRUE(forward_w_cid_builder.commit(ping, test_cid_1));
  }

  // Additional loop iteration that should fail to add any more packets
  {
    auto next_slot = forward_w_cid_builder.get_next_slot();

    std::optional ping_builder_opt =
      piwo::ping_builder::make_ping_builder(next_slot.value());
    ASSERT_FALSE(ping_builder_opt.has_value());
  }

  // Finalize forward frame
  auto forward_w_cid = piwo::forward_w_cid(forward_w_cid_builder);

  auto expected_packet_length =
    FORWARD_W_CID_WITHOUT_DATA_SIZE +
    (FORWARD_W_CID_CID_SIZE + piwo::ping_length) * piwo_frames_in_buffer_count;

  // Pointers must be the same
  EXPECT_EQ(forward_w_cid.cdata(), buffer);
  EXPECT_EQ(forward_w_cid.data(), buffer);
  EXPECT_EQ(forward_w_cid.cdata(), buffer);
  EXPECT_EQ(forward_w_cid.begin(), buffer);
  EXPECT_EQ(forward_w_cid.cbegin(), buffer);
  EXPECT_EQ(forward_w_cid.end(), buffer + expected_packet_length);
  EXPECT_EQ(forward_w_cid.cend(), buffer + expected_packet_length);

  EXPECT_EQ(forward_w_cid.size(), expected_packet_length);

  uint16_t expected_little_endian_packet_length =
    piwo::htole(static_cast<uint16_t>(expected_packet_length));

  uint16_t packet_length_from_buffer;
  memcpy(&packet_length_from_buffer,
        (buffer + piwo::common_length_pos),
        piwo::forward_w_cid_length_size);

  EXPECT_EQ(expected_little_endian_packet_length, packet_length_from_buffer);

  // contruct forward_w_cid using raw memeory
  size_t packets_cnt = 0;
  auto   forward_opt = piwo::forward_w_cid::make_forward_w_cid(raw_packet);
  auto   forward     = forward_opt.value();
  auto   packet_opt  = forward.get_next_packet();
  while (packet_opt.has_value())
  {
    packet_opt = forward.get_next_packet();
    packets_cnt++;
  }
  EXPECT_EQ(packets_cnt, piwo_frames_in_buffer_count);
}

TEST(ForwardWCidTest, make_forward_w_cid_success)
{
  constexpr size_t final_packet_size =
    FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE +
    piwo::ping_length + FORWARD_W_CID_CID_SIZE + piwo::status_req_length;

  constexpr size_t buffer_size = 100;
  piwo::net_byte_t buffer[buffer_size];

  constexpr uint8_t test_cid_1 = 10;
  constexpr uint8_t test_cid_2 = 30;

  constexpr size_t first_packet_offset =
    FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE;
  constexpr size_t second_packet_offset =
    first_packet_offset + piwo::ping_length + FORWARD_W_CID_CID_SIZE;

  uint16_t little_endian_final_packet_size =
    piwo::htole(static_cast<uint16_t>(final_packet_size));
  std::copy_n(
    reinterpret_cast<piwo::net_byte_t*>(&little_endian_final_packet_size),
    sizeof(little_endian_final_packet_size),
    buffer + COMMON_PACKET_LENGTH_POS);

  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ FORWARD_W_CID_TYPE };

  // Add ping packet
  buffer[first_packet_offset - FORWARD_W_CID_CID_SIZE] =
    piwo::net_byte_t{ test_cid_1 };
  buffer[first_packet_offset + COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ piwo::ping_length };
  buffer[first_packet_offset + COMMON_PACKET_TYPE_POS] =
    static_cast<piwo::net_byte_t>(piwo::packet_type::PING);

  // Add status request packet
  buffer[second_packet_offset - FORWARD_W_CID_CID_SIZE] =
    piwo::net_byte_t{ test_cid_2 };
  buffer[second_packet_offset + COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ piwo::status_req_length };
  buffer[second_packet_offset + COMMON_PACKET_TYPE_POS] =
    static_cast<piwo::net_byte_t>(piwo::packet_type::STATUS_REQ);

  piwo::raw_packet raw_packet(buffer, buffer_size);

  std::optional readed_frame_type_opt = raw_packet.get_type();
  ASSERT_TRUE(readed_frame_type_opt.has_value());
  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), FORWARD_W_CID_TYPE);

  // Make forward packet
  std::optional forward_w_cid_packet_opt =
    piwo::forward_w_cid::make_forward_w_cid(raw_packet);

  ASSERT_TRUE(forward_w_cid_packet_opt.has_value());

  auto forward_w_cid = forward_w_cid_packet_opt.value();

  // Pointers must be the same
  EXPECT_EQ(forward_w_cid.cdata(), buffer);
  EXPECT_EQ(forward_w_cid.data(), buffer);
  EXPECT_EQ(forward_w_cid.cdata(), buffer);
  EXPECT_EQ(forward_w_cid.begin(), buffer);
  EXPECT_EQ(forward_w_cid.cbegin(), buffer);
  EXPECT_EQ(forward_w_cid.end(), buffer + final_packet_size);
  EXPECT_EQ(forward_w_cid.cend(), buffer + final_packet_size);

  EXPECT_EQ(forward_w_cid.size(), final_packet_size);
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ static_cast<uint8_t>(final_packet_size) });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            static_cast<piwo::net_byte_t>(piwo::packet_type::FORWARD_W_CID));

  std::optional forwart_w_cid_packet_type_opt = forward_w_cid.get_type();
  ASSERT_TRUE(forwart_w_cid_packet_type_opt.has_value());
  EXPECT_EQ(underlay_cast(forwart_w_cid_packet_type_opt.value()),
            FORWARD_W_CID_TYPE);

  // Validate first frame
  std::optional received_packet_1_opt = forward_w_cid.get_next_packet();
  ASSERT_TRUE(received_packet_1_opt.has_value());

  piwo::forward_w_cid_encapsulated_packet_t received_packet_1_data =
    received_packet_1_opt.value();
  EXPECT_EQ(received_packet_1_data.packet.cdata(),
            buffer + FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE);
  EXPECT_EQ(received_packet_1_data.packet.size(), piwo::ping_length);
  EXPECT_EQ(received_packet_1_data.cid, test_cid_1);

  // Validate second frame
  std::optional received_packet_2_opt = forward_w_cid.get_next_packet();
  ASSERT_TRUE(received_packet_2_opt.has_value());

  piwo::forward_w_cid_encapsulated_packet_t received_packet_2_data =
    received_packet_2_opt.value();
  EXPECT_EQ(received_packet_2_data.packet.cdata(),
            buffer + FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE +
              piwo::ping_length + FORWARD_W_CID_CID_SIZE);
  EXPECT_EQ(received_packet_2_data.packet.size(), piwo::status_req_length);
  EXPECT_EQ(received_packet_2_data.cid, test_cid_2);

  // No more frames should be available
  std::optional received_packet_3_opt = forward_w_cid.get_next_packet();
  EXPECT_FALSE(received_packet_3_opt.has_value());
}

TEST(ForwardWCidTest, make_forward_w_cid_builder_buffer_too_small)
{
  constexpr size_t buffer_size = FORWARD_W_CID_WITHOUT_DATA_SIZE - 1;
  piwo::net_byte_t buffer[buffer_size];

  piwo::raw_packet raw_packet(buffer, buffer_size);

  // Make forward builder
  std::optional forward_w_cid_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

  ASSERT_FALSE(forward_w_cid_builder_opt.has_value());
}

TEST(ForwardWCidTest, make_forward_w_cid_buffer_smaller_than_minimum)
{
  constexpr size_t buffer_size_too_small = FORWARD_W_CID_WITHOUT_DATA_SIZE - 1;
  constexpr size_t buffer_size           = 100;
  piwo::net_byte_t buffer[buffer_size];

  uint16_t little_endian_frame_length =
    piwo::htole(static_cast<uint16_t>(buffer_size));
  std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&little_endian_frame_length),
              sizeof(little_endian_frame_length),
              buffer + COMMON_PACKET_LENGTH_POS);

  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ FORWARD_W_CID_TYPE };

  piwo::raw_packet raw_packet(buffer, buffer_size_too_small);

  // Make forward
  std::optional forward_w_cid_opt =
    piwo::forward_w_cid::make_forward_w_cid(raw_packet);

  EXPECT_FALSE(forward_w_cid_opt.has_value());
}

TEST(ForwardWCidTest, make_forward_w_cid_unknown_packet_type)
{
  constexpr uint8_t unknown_packet_type = 255;
  constexpr size_t  buffer_size         = 100;
  piwo::net_byte_t  buffer[buffer_size];

  uint16_t little_endian_buffer_size =
    piwo::htole(static_cast<uint16_t>(buffer_size));
  std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&little_endian_buffer_size),
              sizeof(little_endian_buffer_size),
              buffer + COMMON_PACKET_LENGTH_POS);

  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ unknown_packet_type };

  piwo::raw_packet raw_packet(buffer, buffer_size);

  // Make forward
  std::optional forward_w_cid_opt =
    piwo::forward_w_cid::make_forward_w_cid(raw_packet);

  EXPECT_FALSE(forward_w_cid_opt.has_value());
}

TEST(ForwardWCidTest, make_forward_w_cid_wrong_packet_type)
{
  constexpr size_t buffer_size = 100;
  piwo::net_byte_t buffer[buffer_size];

  uint16_t little_endian_buffer_size =
    piwo::htole(static_cast<uint16_t>(buffer_size));
  std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&little_endian_buffer_size),
              sizeof(little_endian_buffer_size),
              buffer + COMMON_PACKET_LENGTH_POS);

  buffer[COMMON_PACKET_TYPE_POS] = static_cast<piwo::net_byte_t>(
    static_cast<uint8_t>(piwo::packet_type::PING));

  piwo::raw_packet raw_packet(buffer, buffer_size);

  // Make forward
  std::optional forward_w_cid_opt =
    piwo::forward_w_cid::make_forward_w_cid(raw_packet);

  EXPECT_FALSE(forward_w_cid_opt.has_value());
}

TEST(ForwardWCidTest, make_forward_w_cid_buffer_too_small)
{
  constexpr size_t frame_lenght = 21;
  constexpr size_t buffer_size  = 20;
  piwo::net_byte_t buffer[buffer_size];

  uint16_t little_endian_frame_length =
    piwo::htole(static_cast<uint16_t>(frame_lenght));
  std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&little_endian_frame_length),
              sizeof(little_endian_frame_length),
              buffer + COMMON_PACKET_LENGTH_POS);

  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ FORWARD_W_CID_TYPE };

  piwo::raw_packet raw_packet(buffer, buffer_size);

  // Make forward
  std::optional forward_w_cid_opt =
    piwo::forward_w_cid::make_forward_w_cid(raw_packet);

  EXPECT_FALSE(forward_w_cid_opt.has_value());
}

TEST(ForwardWCidTest, get_next_slot_buffer_too_small)
{
  constexpr size_t buffer_size = FORWARD_W_CID_WITHOUT_DATA_SIZE;
  piwo::net_byte_t buffer[buffer_size];

  piwo::raw_packet raw_packet(buffer, buffer_size);

  // Make forward builder
  std::optional forward_w_cid_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

  ASSERT_TRUE(forward_w_cid_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ FORWARD_W_CID_TYPE });

  auto forward_w_cid_builder = forward_w_cid_builder_opt.value();

  std::optional next_slot_opt = forward_w_cid_builder.get_next_slot();
  EXPECT_FALSE(next_slot_opt.has_value());
}

TEST(ForwardWCidTest, get_next_slot_buffer_too_small_2)
{
  constexpr size_t buffer_size =
    FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE;
  piwo::net_byte_t buffer[buffer_size];

  piwo::raw_packet raw_packet(buffer, buffer_size);

  // Make forward builder
  std::optional forward_w_cid_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

  ASSERT_TRUE(forward_w_cid_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ FORWARD_W_CID_TYPE });

  auto forward_w_cid_builder = forward_w_cid_builder_opt.value();

  auto next_slot_opt = forward_w_cid_builder.get_next_slot();
  EXPECT_FALSE(next_slot_opt.has_value());
}

TEST(ForwardWCidTest,
     get_next_packet_held_frame_length_longer_than_forward_packet)
{
  constexpr size_t forward_packet_length = 20;
  constexpr size_t held_frame_length     = 21;

  constexpr size_t buffer_size = 100;
  piwo::net_byte_t buffer[buffer_size];

  constexpr size_t held_frame_offset =
    FORWARD_W_CID_WITHOUT_DATA_SIZE + FORWARD_W_CID_CID_SIZE;

  uint16_t little_endian_forward_packet_length =
    piwo::htole(static_cast<uint16_t>(forward_packet_length));
  std::copy_n(
    reinterpret_cast<piwo::net_byte_t*>(&little_endian_forward_packet_length),
    sizeof(little_endian_forward_packet_length),
    buffer + COMMON_PACKET_LENGTH_POS);

  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ FORWARD_W_CID_TYPE };

  // Set held frame length
  buffer[held_frame_offset + COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ piwo::ping_length };

  piwo::raw_packet raw_packet(buffer, buffer_size);

  // Make forward packet
  std::optional forward_w_cid_packet_opt =
    piwo::forward_w_cid::make_forward_w_cid(raw_packet);

  ASSERT_TRUE(forward_w_cid_packet_opt.has_value());

  auto forward_w_cid = forward_w_cid_packet_opt.value();

  // Pointers must be the same
  EXPECT_EQ(forward_w_cid.cdata(), buffer);
  EXPECT_EQ(forward_w_cid.data(), buffer);
  EXPECT_EQ(forward_w_cid.cdata(), buffer);
  EXPECT_EQ(forward_w_cid.begin(), buffer);
  EXPECT_EQ(forward_w_cid.cbegin(), buffer);
  EXPECT_EQ(forward_w_cid.end(), buffer + forward_packet_length);
  EXPECT_EQ(forward_w_cid.cend(), buffer + forward_packet_length);

  EXPECT_EQ(forward_w_cid.size(), forward_packet_length);
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ static_cast<uint8_t>(forward_packet_length) });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            static_cast<piwo::net_byte_t>(piwo::packet_type::FORWARD_W_CID));

  std::optional forwart_w_cid_packet_type_opt = forward_w_cid.get_type();
  ASSERT_TRUE(forwart_w_cid_packet_type_opt.has_value());
  EXPECT_EQ(underlay_cast(forwart_w_cid_packet_type_opt.value()),
            FORWARD_W_CID_TYPE);

  std::optional received_packet_opt = forward_w_cid.get_next_packet();
  EXPECT_TRUE(received_packet_opt.has_value());
}
