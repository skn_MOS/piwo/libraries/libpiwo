#include "proto.h"

#include <gtest/gtest.h>

#include <array>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  STATUS_REPLY_SIZE         = 20;
constexpr size_t  UID_SIZE                  = 12;
constexpr uint8_t STATUS_REPLY_TYPE         = 0x03;
constexpr uint8_t STATUS_REPLY_SEQ_POS      = 2;
constexpr uint8_t STATUS_REPLY_UID_POS      = 3;
constexpr uint8_t STATUS_REPLY_LA_POS       = 15;
constexpr uint8_t STATUS_REPLY_RSSI_POS     = 16;
constexpr uint8_t STATUS_REPLY_UPTIME_H_POS = 17;
constexpr uint8_t STATUS_REPLY_UPTIME_M_POS = 18;
constexpr uint8_t STATUS_REPLY_UPTIME_S_POS = 19;

constexpr uint8_t STATUS_REPLY_SEQ_TEST_VALUE      = 21;
constexpr uint8_t STATUS_REPLY_LA_TEST_VALUE       = 22;
constexpr uint8_t STATUS_REPLY_RSSI_TEST_VALUE     = 23;
constexpr uint8_t STATUS_REPLY_UPTIME_H_TEST_VALUE = 12;
constexpr uint8_t STATUS_REPLY_UPTIME_M_TEST_VALUE = 25;
constexpr uint8_t STATUS_REPLY_UPTIME_S_TEST_VALUE = 26;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(StatusReplyTest, build_and_make_status_reply_success)
{
  piwo::net_byte_t buffer[STATUS_REPLY_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  piwo::uid        uid{ reinterpret_cast<uint8_t*>(uid_buffer.data()) };

  piwo::raw_packet raw_packet(buffer, STATUS_REPLY_SIZE);

  auto status_reply_builder_opt =
    piwo::status_reply_builder::make_status_reply_builder(raw_packet);

  ASSERT_TRUE(status_reply_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ STATUS_REPLY_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ STATUS_REPLY_TYPE });

  auto status_reply_builder = *status_reply_builder_opt;
  status_reply_builder.set_seq(STATUS_REPLY_SEQ_TEST_VALUE);
  status_reply_builder.set_la(STATUS_REPLY_LA_TEST_VALUE);
  status_reply_builder.set_rssi(STATUS_REPLY_RSSI_TEST_VALUE);
  status_reply_builder.set_up_time_h(STATUS_REPLY_UPTIME_H_TEST_VALUE);
  status_reply_builder.set_up_time_m(STATUS_REPLY_UPTIME_M_TEST_VALUE);
  status_reply_builder.set_up_time_s(STATUS_REPLY_UPTIME_S_TEST_VALUE);
  status_reply_builder.set_uid(uid);

  piwo::status_reply status_reply_packet(status_reply_builder);

  // Pointers must be the same
  EXPECT_EQ(status_reply_packet.cdata(), buffer);
  EXPECT_EQ(status_reply_packet.data(), buffer);
  EXPECT_EQ(status_reply_packet.cdata(), buffer);
  EXPECT_EQ(status_reply_packet.begin(), buffer);
  EXPECT_EQ(status_reply_packet.cbegin(), buffer);
  EXPECT_EQ(status_reply_packet.end(), buffer + STATUS_REPLY_SIZE);
  EXPECT_EQ(status_reply_packet.cend(), buffer + STATUS_REPLY_SIZE);

  EXPECT_EQ(status_reply_packet.size(), STATUS_REPLY_SIZE);
  EXPECT_EQ(status_reply_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ STATUS_REPLY_SIZE });

  ASSERT_TRUE(status_reply_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(status_reply_packet.get_type().value()),
            STATUS_REPLY_TYPE);
  EXPECT_EQ(status_reply_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ STATUS_REPLY_TYPE });

  EXPECT_EQ(status_reply_packet.get_seq(), STATUS_REPLY_SEQ_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_SEQ_POS],
            piwo::net_byte_t{ STATUS_REPLY_SEQ_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_la(), STATUS_REPLY_LA_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_LA_POS],
            piwo::net_byte_t{ STATUS_REPLY_LA_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_rssi(), STATUS_REPLY_RSSI_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_RSSI_POS],
            piwo::net_byte_t{ STATUS_REPLY_RSSI_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_uptime_h(),
            STATUS_REPLY_UPTIME_H_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_UPTIME_H_POS],
            piwo::net_byte_t{ STATUS_REPLY_UPTIME_H_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_uptime_m(),
            STATUS_REPLY_UPTIME_M_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_UPTIME_M_POS],
            piwo::net_byte_t{ STATUS_REPLY_UPTIME_M_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_uptime_s(),
            STATUS_REPLY_UPTIME_S_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_UPTIME_S_POS],
            piwo::net_byte_t{ STATUS_REPLY_UPTIME_S_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> status_reply_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(status_reply_packet.data() + STATUS_REPLY_UID_POS),
                UID_SIZE, status_reply_packet_uid_data.data());

  EXPECT_EQ(status_reply_packet_uid_data, uid_buffer);
}

TEST(StatusReplyTest, make_status_reply_success)
{
  piwo::net_byte_t buffer[STATUS_REPLY_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ STATUS_REPLY_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ STATUS_REPLY_TYPE };
  buffer[STATUS_REPLY_SEQ_POS] =
    piwo::net_byte_t{ STATUS_REPLY_SEQ_TEST_VALUE };
  buffer[STATUS_REPLY_LA_POS] = piwo::net_byte_t{ STATUS_REPLY_LA_TEST_VALUE };
  buffer[STATUS_REPLY_RSSI_POS] =
    piwo::net_byte_t{ STATUS_REPLY_RSSI_TEST_VALUE };
  buffer[STATUS_REPLY_UPTIME_H_POS] =
    piwo::net_byte_t{ STATUS_REPLY_UPTIME_H_TEST_VALUE };
  buffer[STATUS_REPLY_UPTIME_M_POS] =
    piwo::net_byte_t{ STATUS_REPLY_UPTIME_M_TEST_VALUE };
  buffer[STATUS_REPLY_UPTIME_S_POS] =
    piwo::net_byte_t{ STATUS_REPLY_UPTIME_S_TEST_VALUE };

  std::copy(reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data()),
            reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data() + UID_SIZE),
            buffer + STATUS_REPLY_UID_POS);

  piwo::raw_packet raw_packet(buffer, STATUS_REPLY_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), STATUS_REPLY_TYPE);

  auto status_reply_packet_opt =
    piwo::status_reply::make_status_reply(raw_packet);

  ASSERT_TRUE(status_reply_packet_opt.has_value());

  auto status_reply_packet = status_reply_packet_opt.value();

  // //Pointers must be the same
  EXPECT_EQ(status_reply_packet.cdata(), buffer);
  EXPECT_EQ(status_reply_packet.data(), buffer);
  EXPECT_EQ(status_reply_packet.begin(), buffer);
  EXPECT_EQ(status_reply_packet.cbegin(), buffer);
  EXPECT_EQ(status_reply_packet.end(), buffer + STATUS_REPLY_SIZE);
  EXPECT_EQ(status_reply_packet.cend(), buffer + STATUS_REPLY_SIZE);

  EXPECT_EQ(status_reply_packet.size(), STATUS_REPLY_SIZE);
  EXPECT_EQ(status_reply_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ STATUS_REPLY_SIZE });

  ASSERT_TRUE(status_reply_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(status_reply_packet.get_type().value()),
            STATUS_REPLY_TYPE);
  EXPECT_EQ(status_reply_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ STATUS_REPLY_TYPE });

  EXPECT_EQ(status_reply_packet.get_seq(), STATUS_REPLY_SEQ_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_SEQ_POS],
            piwo::net_byte_t{ STATUS_REPLY_SEQ_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_la(), STATUS_REPLY_LA_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_LA_POS],
            piwo::net_byte_t{ STATUS_REPLY_LA_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_rssi(), STATUS_REPLY_RSSI_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_RSSI_POS],
            piwo::net_byte_t{ STATUS_REPLY_RSSI_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_uptime_h(),
            STATUS_REPLY_UPTIME_H_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_UPTIME_H_POS],
            piwo::net_byte_t{ STATUS_REPLY_UPTIME_H_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_uptime_m(),
            STATUS_REPLY_UPTIME_M_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_UPTIME_M_POS],
            piwo::net_byte_t{ STATUS_REPLY_UPTIME_M_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_uptime_s(),
            STATUS_REPLY_UPTIME_S_TEST_VALUE);
  EXPECT_EQ(status_reply_packet.data()[STATUS_REPLY_UPTIME_S_POS],
            piwo::net_byte_t{ STATUS_REPLY_UPTIME_S_TEST_VALUE });

  EXPECT_EQ(status_reply_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> status_reply_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(status_reply_packet.data() + STATUS_REPLY_UID_POS),
                UID_SIZE, status_reply_packet_uid_data.data());

  EXPECT_EQ(status_reply_packet_uid_data, uid_buffer);
}

TEST(StatusReplyTest, make_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = STATUS_REPLY_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto status_reply_builder_opt =
    piwo::status_reply_builder::make_status_reply_builder(raw_packet);

  EXPECT_FALSE(status_reply_builder_opt.has_value());
}

TEST(StatusReplyTest, make_status_reply_fail_buffer_too_small)
{
  auto             too_small_buffer_size = STATUS_REPLY_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto status_reply_opt = piwo::status_reply::make_status_reply(raw_packet);

  EXPECT_FALSE(status_reply_opt.has_value());
}

TEST(StatusReplyTest, make_status_reply_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = STATUS_REPLY_SIZE + 1;
  piwo::net_byte_t buffer[STATUS_REPLY_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, STATUS_REPLY_SIZE);

  auto status_reply_opt = piwo::status_reply::make_status_reply(raw_packet);

  EXPECT_FALSE(status_reply_opt.has_value());

  inconsistent_frame_length = STATUS_REPLY_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  status_reply_opt = piwo::status_reply::make_status_reply(raw_packet);

  EXPECT_FALSE(status_reply_opt.has_value());
}

TEST(StatusReplyTest, make_status_reply_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[STATUS_REPLY_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ STATUS_REPLY_SIZE };

  piwo::raw_packet raw_packet(buffer, STATUS_REPLY_SIZE);

  auto status_reply_opt = piwo::status_reply::make_status_reply(raw_packet);

  EXPECT_FALSE(status_reply_opt.has_value());
}
