#include "protodef.h"

#include <optional>

#include <gtest/gtest.h>

struct fast_uid_testable : public piwo::fast_uid
{
  fast_uid_testable(uint32_t msb, uint64_t lsb)
    : fast_uid(msb, lsb)
  {
  }

  fast_uid_testable(const piwo::uid& uid)
    : fast_uid(uid)
  {
  }

  fast_uid_testable(const piwo::fast_uid& uid)
    : fast_uid(uid)
  {
  }

  uint32_t
  get_msb()
  {
    return this->_msb;
  }

  uint64_t
  get_lsb()
  {
    return this->_lsb;
  }
};

TEST(FastUidTest, ctor_uid)
{
  constexpr const char* uid_ascii = "44:55:66:77:88:99:AA:BB:CC:DD:EE:FF";

  constexpr uint32_t expected_msb = 0x44556677;
  constexpr uint64_t expected_lsb = 0x8899AABBCCDDEEFF;

  std::optional test_uid_1 = piwo::uid::from_ascii(uid_ascii);
  ASSERT_TRUE(test_uid_1.has_value());

  fast_uid_testable fast_uid_1(test_uid_1.value());

  EXPECT_EQ(fast_uid_1.get_msb(), expected_msb);
  EXPECT_EQ(fast_uid_1.get_lsb(), expected_lsb);
}

TEST(FastUidTest, equality_operator_equal_msb_and_lsb)
{
  constexpr uint32_t msb = 0x44556677;
  constexpr uint64_t lsb = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb, lsb);
  fast_uid_testable fast_uid_2(msb, lsb);

  EXPECT_TRUE(fast_uid_1 == fast_uid_2);
}

TEST(FastUidTest, equality_operator_unequal_msb)
{
  constexpr uint32_t msb_1 = 0x44556678;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 == fast_uid_2);
}

TEST(FastUidTest, equality_operator_unequal_lsb)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFE;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 == fast_uid_2);
}

TEST(FastUidTest, equality_operator_unequal_both_msb_and_lsb)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFE;

  constexpr uint32_t msb_2 = 0x44556676;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 == fast_uid_2);
}

TEST(FastUidTest, inequality_operator_unequal_msb)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556676;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 != fast_uid_2);
}

TEST(FastUidTest, inequality_operator_unequal_lsb)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 != fast_uid_2);
}

TEST(FastUidTest, inequality_operator_unequal_both_msb_and_lsb)
{
  constexpr uint32_t msb_1 = 0x44556670;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 != fast_uid_2);
}

TEST(FastUidTest, inequality_operator_equal_both_msb_and_lsb)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 != fast_uid_2);
}

TEST(FastUidTest, or_operator)
{
  constexpr uint32_t msb_1 = 0x00550000;
  constexpr uint64_t lsb_1 = 0x8800AA00CC00EE00;

  constexpr uint32_t msb_2 = 0x44006677;
  constexpr uint64_t lsb_2 = 0x009900BB00DD00FF;

  constexpr uint32_t expected_msb = 0x44556677;
  constexpr uint64_t expected_lsb = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  fast_uid_testable result_fast_uid(fast_uid_1 | fast_uid_2);

  EXPECT_EQ(result_fast_uid.get_msb(), expected_msb);
  EXPECT_EQ(result_fast_uid.get_lsb(), expected_lsb);
}

TEST(FastUidTest, or_equal_operator)
{
  constexpr uint32_t msb_1 = 0x00550000;
  constexpr uint64_t lsb_1 = 0x8800AA00CC00EE00;

  constexpr uint32_t msb_2 = 0x44006677;
  constexpr uint64_t lsb_2 = 0x009900BB00DD00FF;

  constexpr uint32_t expected_msb = 0x44556677;
  constexpr uint64_t expected_lsb = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  fast_uid_1 |= fast_uid_2;

  EXPECT_EQ(fast_uid_1.get_msb(), expected_msb);
  EXPECT_EQ(fast_uid_1.get_lsb(), expected_lsb);
}

TEST(FastUidTest, and_operator)
{
  constexpr uint32_t msb_1 = 0x40550607;
  constexpr uint64_t lsb_1 = 0x8890A0BBC0DDEE0F;

  constexpr uint32_t msb_2 = 0x44506677;
  constexpr uint64_t lsb_2 = 0x8099AAB0CCD00EFF;

  constexpr uint32_t expected_msb = 0x40500607;
  constexpr uint64_t expected_lsb = 0x8090A0B0C0D00E0F;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  fast_uid_testable result_fast_uid(fast_uid_1 & fast_uid_2);

  EXPECT_EQ(result_fast_uid.get_msb(), expected_msb);
  EXPECT_EQ(result_fast_uid.get_lsb(), expected_lsb);
}

TEST(FastUidTest, and_equal_operator)
{
  constexpr uint32_t msb_1 = 0x40550607;
  constexpr uint64_t lsb_1 = 0x8890A0BBC0DDEE0F;

  constexpr uint32_t msb_2 = 0x44506677;
  constexpr uint64_t lsb_2 = 0x8099AAB0CCD00EFF;

  constexpr uint32_t expected_msb = 0x40500607;
  constexpr uint64_t expected_lsb = 0x8090A0B0C0D00E0F;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  fast_uid_1 &= fast_uid_2;

  EXPECT_EQ(fast_uid_1.get_msb(), expected_msb);
  EXPECT_EQ(fast_uid_1.get_lsb(), expected_lsb);
}

TEST(FastUidTest, left_shift_operator)
{
  constexpr uint32_t msb = 0x34455667;
  constexpr uint64_t lsb = 0x78899AABBCCDDEEF;

  constexpr uint32_t expected_msb = 0x44556677;
  constexpr uint64_t expected_lsb = 0x8899AABBCCDDEEF0;

  constexpr uint32_t shift = 4;

  fast_uid_testable fast_uid(msb, lsb);

  fast_uid_testable result_fast_uid(fast_uid << shift);

  EXPECT_EQ(result_fast_uid.get_msb(), expected_msb);
  EXPECT_EQ(result_fast_uid.get_lsb(), expected_lsb);
}

TEST(FastUidTest, left_shift_equal_operator)
{
  constexpr uint32_t msb = 0x34455667;
  constexpr uint64_t lsb = 0x78899AABBCCDDEEF;

  constexpr uint32_t expected_msb = 0x44556677;
  constexpr uint64_t expected_lsb = 0x8899AABBCCDDEEF0;

  constexpr uint32_t shift = 4;

  fast_uid_testable fast_uid(msb, lsb);

  fast_uid_testable result_fast_uid(fast_uid <<= shift);

  EXPECT_EQ(result_fast_uid.get_msb(), expected_msb);
  EXPECT_EQ(result_fast_uid.get_lsb(), expected_lsb);
}

TEST(FastUidTest, right_shift_operator)
{
  constexpr uint32_t msb = 0x45566778;
  constexpr uint64_t lsb = 0x899AABBCCDDEEFFF;

  constexpr uint32_t expected_msb = 0x04556677;
  constexpr uint64_t expected_lsb = 0x8899AABBCCDDEEFF;

  constexpr uint32_t shift = 4;

  fast_uid_testable fast_uid(msb, lsb);
  fast_uid_testable expected_fast_uid(expected_msb, expected_lsb);

  fast_uid_testable result_fast_uid(fast_uid >> shift);

  EXPECT_EQ(result_fast_uid.get_msb(), expected_msb);
  EXPECT_EQ(result_fast_uid.get_lsb(), expected_lsb);
}

TEST(FastUidTest, right_shift_equal_operator)
{
  constexpr uint32_t msb = 0x45566778;
  constexpr uint64_t lsb = 0x899AABBCCDDEEFFF;

  constexpr uint32_t expected_msb = 0x04556677;
  constexpr uint64_t expected_lsb = 0x8899AABBCCDDEEFF;

  constexpr uint32_t shift = 4;

  fast_uid_testable fast_uid(msb, lsb);
  fast_uid_testable expected_fast_uid(expected_msb, expected_lsb);

  fast_uid >>= shift;

  EXPECT_EQ(fast_uid.get_msb(), expected_msb);
  EXPECT_EQ(fast_uid.get_lsb(), expected_lsb);
}

TEST(FastUidTest, less_than_operator_msb_is_lower)
{
  constexpr uint32_t msb_1 = 0x44556676;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 < fast_uid_2);
}

TEST(FastUidTest, less_than_operator_lsb_is_lower)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFE;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 < fast_uid_2);
}

TEST(FastUidTest, less_than_operator_both_msb_and_lsb_are_lower)
{
  constexpr uint32_t msb_1 = 0x44556676;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFE;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 < fast_uid_2);
}

TEST(FastUidTest, less_than_operator_msb_is_higher)
{
  constexpr uint32_t msb_1 = 0x44556678;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 < fast_uid_2);
}

TEST(FastUidTest, less_than_operator_lsb_is_higher)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 < fast_uid_2);
}

TEST(FastUidTest, less_than_operator_both_msb_and_lsb_are_higher)
{
  constexpr uint32_t msb_1 = 0x44556678;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 < fast_uid_2);
}

TEST(FastUidTest, less_than_operator_uids_are_equal)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 < fast_uid_2);
}

TEST(FastUidTest, less_than_equal_operator_msb_is_lower)
{
  constexpr uint32_t msb_1 = 0x44556676;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 <= fast_uid_2);
}

TEST(FastUidTest, less_than_equal_operator_lsb_is_lower)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFE;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 <= fast_uid_2);
}

TEST(FastUidTest, less_than_equal_operator_both_msb_and_lsb_are_lower)
{
  constexpr uint32_t msb_1 = 0x44556676;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFE;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 <= fast_uid_2);
}

TEST(FastUidTest, less_than_equal_operator_msb_is_higher)
{
  constexpr uint32_t msb_1 = 0x44556678;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 <= fast_uid_2);
}

TEST(FastUidTest, less_than_equal_operator_lsb_is_higher)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 <= fast_uid_2);
}

TEST(FastUidTest, less_than_equal_operator_both_msb_and_lsb_are_higher)
{
  constexpr uint32_t msb_1 = 0x44556678;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 <= fast_uid_2);
}

TEST(FastUidTest, less_than_equal_operator_uids_are_equal)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 <= fast_uid_2);
}

TEST(FastUidTest, more_than_operator_msb_is_lower)
{
  constexpr uint32_t msb_1 = 0x44556676;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 > fast_uid_2);
}

TEST(FastUidTest, more_than_operator_lsb_is_lower)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFE;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 > fast_uid_2);
}

TEST(FastUidTest, more_than_operator_both_msb_and_lsb_are_lower)
{
  constexpr uint32_t msb_1 = 0x44556676;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 > fast_uid_2);
}

TEST(FastUidTest, more_than_operator_msb_is_higher)
{
  constexpr uint32_t msb_1 = 0x44556678;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 > fast_uid_2);
}

TEST(FastUidTest, more_than_operator_lsb_is_higher)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 > fast_uid_2);
}

TEST(FastUidTest, more_than_operator_both_msb_and_lsb_are_higher)
{
  constexpr uint32_t msb_1 = 0x44556678;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 > fast_uid_2);
}

TEST(FastUidTest, more_than_operator_uids_are_equal)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 > fast_uid_2);
}

TEST(FastUidTest, more_than_equal_operator_msb_is_lower)
{
  constexpr uint32_t msb_1 = 0x44556676;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 >= fast_uid_2);
}

TEST(FastUidTest, more_than_equal_operator_lsb_is_lower)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFE;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 >= fast_uid_2);
}

TEST(FastUidTest, more_than_equal_operator_both_msb_and_lsb_are_lower)
{
  constexpr uint32_t msb_1 = 0x44556676;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFE;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_FALSE(fast_uid_1 >= fast_uid_2);
}

TEST(FastUidTest, more_than_equal_operator_msb_is_higher)
{
  constexpr uint32_t msb_1 = 0x44556678;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 >= fast_uid_2);
}

TEST(FastUidTest, more_than_equal_operator_lsb_is_higher)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 >= fast_uid_2);
}

TEST(FastUidTest, more_than_equal_operator_both_msb_and_lsb_are_higher)
{
  constexpr uint32_t msb_1 = 0x44556678;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFE;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 >= fast_uid_2);
}

TEST(FastUidTest, more_than_equal_operator_uids_are_equal)
{
  constexpr uint32_t msb_1 = 0x44556677;
  constexpr uint64_t lsb_1 = 0x8899AABBCCDDEEFF;

  constexpr uint32_t msb_2 = 0x44556677;
  constexpr uint64_t lsb_2 = 0x8899AABBCCDDEEFF;

  fast_uid_testable fast_uid_1(msb_1, lsb_1);
  fast_uid_testable fast_uid_2(msb_2, lsb_2);

  EXPECT_TRUE(fast_uid_1 >= fast_uid_2);
}
