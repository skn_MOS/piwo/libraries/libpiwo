#include "proto.h"

#include <gtest/gtest.h>

#include <array>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  PING_SIZE    = 15;
constexpr size_t  UID_SIZE     = 12;
constexpr uint8_t PING_TYPE    = 0x0;
constexpr uint8_t PING_SEQ_POS = 2;
constexpr uint8_t PING_UID_POS = 3;

constexpr uint8_t PING_SEQ_TEST_VALUE = 15;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(PingTest, build_and_make_ping_success)
{
  piwo::net_byte_t buffer[PING_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  piwo::uid        uid{ reinterpret_cast<uint8_t*>(uid_buffer.data()) };

  piwo::raw_packet raw_packet(buffer, PING_SIZE);

  auto ping_builder_opt = piwo::ping_builder::make_ping_builder(raw_packet);

  ASSERT_TRUE(ping_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS], piwo::net_byte_t{ PING_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS], piwo::net_byte_t{ PING_TYPE });

  auto ping_builder = *ping_builder_opt;
  ping_builder.set_seq(PING_SEQ_TEST_VALUE);
  ping_builder.set_uid(uid);

  piwo::ping ping_packet(ping_builder);

  // Pointers must be the same
  EXPECT_EQ(ping_packet.cdata(), buffer);
  EXPECT_EQ(ping_packet.data(), buffer);
  EXPECT_EQ(ping_packet.cdata(), buffer);
  EXPECT_EQ(ping_packet.begin(), buffer);
  EXPECT_EQ(ping_packet.cbegin(), buffer);
  EXPECT_EQ(ping_packet.end(), buffer + PING_SIZE);
  EXPECT_EQ(ping_packet.cend(), buffer + PING_SIZE);

  EXPECT_EQ(ping_packet.size(), PING_SIZE);
  EXPECT_EQ(ping_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ PING_SIZE });

  ASSERT_TRUE(ping_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(ping_packet.get_type().value()), PING_TYPE);
  EXPECT_EQ(ping_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ PING_TYPE });

  EXPECT_EQ(ping_packet.get_seq(), PING_SEQ_TEST_VALUE);
  EXPECT_EQ(ping_packet.data()[PING_SEQ_POS],
            piwo::net_byte_t{ PING_SEQ_TEST_VALUE });

  EXPECT_EQ(ping_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> ping_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(ping_packet.data() + PING_UID_POS),
                UID_SIZE, ping_packet_uid_data.data());

  EXPECT_EQ(ping_packet_uid_data, uid_buffer);
}

TEST(PingTest, make_ping_success)
{
  piwo::net_byte_t buffer[PING_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ PING_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ PING_TYPE };
  buffer[PING_SEQ_POS]             = piwo::net_byte_t{ PING_SEQ_TEST_VALUE };
  std::copy(reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data()),
            reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data() + UID_SIZE),
            buffer + PING_UID_POS);

  piwo::raw_packet raw_packet(buffer, PING_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), PING_TYPE);

  auto ping_packet_opt = piwo::ping::make_ping(raw_packet);

  ASSERT_TRUE(ping_packet_opt.has_value());

  auto ping_packet = ping_packet_opt.value();

  // Pointers must be the same
  EXPECT_EQ(ping_packet.cdata(), buffer);
  EXPECT_EQ(ping_packet.data(), buffer);
  EXPECT_EQ(ping_packet.begin(), buffer);
  EXPECT_EQ(ping_packet.cbegin(), buffer);
  EXPECT_EQ(ping_packet.end(), buffer + PING_SIZE);
  EXPECT_EQ(ping_packet.cend(), buffer + PING_SIZE);

  EXPECT_EQ(ping_packet.size(), PING_SIZE);
  EXPECT_EQ(ping_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ PING_SIZE });

  ASSERT_TRUE(ping_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(ping_packet.get_type().value()), PING_TYPE);
  EXPECT_EQ(ping_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ PING_TYPE });

  EXPECT_EQ(ping_packet.get_seq(), PING_SEQ_TEST_VALUE);
  EXPECT_EQ(ping_packet.data()[PING_SEQ_POS],
            piwo::net_byte_t{ PING_SEQ_TEST_VALUE });

  EXPECT_EQ(ping_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> ping_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(ping_packet.data() + PING_UID_POS),
                UID_SIZE, ping_packet_uid_data.data());

  EXPECT_EQ(ping_packet_uid_data, uid_buffer);
}

TEST(PingTest, make_ping_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = PING_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto ping_builder_opt = piwo::ping_builder::make_ping_builder(raw_packet);

  EXPECT_FALSE(ping_builder_opt.has_value());
}

TEST(PingTest, make_ping_fail_buffer_too_small)
{
  auto             too_small_buffer_size = PING_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto ping_opt = piwo::ping::make_ping(raw_packet);

  EXPECT_FALSE(ping_opt.has_value());
}

TEST(PingTest, make_ping_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = PING_SIZE + 1;
  piwo::net_byte_t buffer[PING_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, PING_SIZE);

  auto ping_opt = piwo::ping::make_ping(raw_packet);

  EXPECT_FALSE(ping_opt.has_value());

  inconsistent_frame_length = PING_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  ping_opt = piwo::ping::make_ping(raw_packet);

  EXPECT_FALSE(ping_opt.has_value());
}

TEST(PingTest, make_ping_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[PING_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ PING_SIZE };

  piwo::raw_packet raw_packet(buffer, PING_SIZE);

  auto ping_opt = piwo::ping::make_ping(raw_packet);

  EXPECT_FALSE(ping_opt.has_value());
}
