#include "proto.h"

#include <gtest/gtest.h>

#include <array>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  STATUS_REQ_SIZE    = 15;
constexpr size_t  UID_SIZE           = 12;
constexpr uint8_t STATUS_REQ_TYPE    = 0x02;
constexpr uint8_t STATUS_REQ_SEQ_POS = 2;
constexpr uint8_t STATUS_REQ_UID_POS = 3;

constexpr uint8_t STATUS_REQ_SEQ_TEST_VALUE = 2;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(StatusRequestTest, build_and_make_status_req_success)
{
  piwo::net_byte_t buffer[STATUS_REQ_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
  piwo::uid        uid{ reinterpret_cast<uint8_t*>(uid_buffer.data()) };

  piwo::raw_packet raw_packet(buffer, STATUS_REQ_SIZE);

  auto status_request_builder_opt =
    piwo::status_request_builder::make_status_request_builder(raw_packet);

  ASSERT_TRUE(status_request_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ STATUS_REQ_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ STATUS_REQ_TYPE });

  auto status_request_builder = *status_request_builder_opt;
  status_request_builder.set_seq(STATUS_REQ_SEQ_TEST_VALUE);
  status_request_builder.set_uid(uid);

  piwo::status_request status_request_packet(status_request_builder);

  // Pointers must be the same
  EXPECT_EQ(status_request_packet.cdata(), buffer);
  EXPECT_EQ(status_request_packet.data(), buffer);
  EXPECT_EQ(status_request_packet.cdata(), buffer);
  EXPECT_EQ(status_request_packet.begin(), buffer);
  EXPECT_EQ(status_request_packet.cbegin(), buffer);
  EXPECT_EQ(status_request_packet.end(), buffer + STATUS_REQ_SIZE);
  EXPECT_EQ(status_request_packet.cend(), buffer + STATUS_REQ_SIZE);

  EXPECT_EQ(status_request_packet.size(), STATUS_REQ_SIZE);
  EXPECT_EQ(status_request_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ STATUS_REQ_SIZE });

  ASSERT_TRUE(status_request_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(status_request_packet.get_type().value()),
            STATUS_REQ_TYPE);
  EXPECT_EQ(status_request_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ STATUS_REQ_TYPE });

  EXPECT_EQ(status_request_packet.get_seq(), STATUS_REQ_SEQ_TEST_VALUE);
  EXPECT_EQ(status_request_packet.data()[STATUS_REQ_SEQ_POS],
            piwo::net_byte_t{ STATUS_REQ_SEQ_TEST_VALUE });

  EXPECT_EQ(status_request_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> status_request_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(status_request_packet.data() + STATUS_REQ_UID_POS),
                UID_SIZE, status_request_packet_uid_data.data());

  EXPECT_EQ(status_request_packet_uid_data, uid_buffer);
}

TEST(StatusRequestTest, make_status_req_success)
{
  piwo::net_byte_t buffer[STATUS_REQ_SIZE];
  std::array<uint8_t, UID_SIZE> uid_buffer =
   {'1', '2', '3', '4', '5', '6', '7', '8', 'D', 'E', 'A', 'D'};
 
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ STATUS_REQ_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ STATUS_REQ_TYPE };
  buffer[STATUS_REQ_SEQ_POS] = piwo::net_byte_t{ STATUS_REQ_SEQ_TEST_VALUE };
  std::copy(reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data()),
            reinterpret_cast<piwo::net_byte_t*>(uid_buffer.data() + UID_SIZE),
            buffer + STATUS_REQ_UID_POS);

  piwo::raw_packet raw_packet(buffer, STATUS_REQ_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), STATUS_REQ_TYPE);

  auto status_request_packet_opt =
    piwo::status_request::make_status_request(raw_packet);

  ASSERT_TRUE(status_request_packet_opt.has_value());

  auto status_request_packet = status_request_packet_opt.value();

  // //Pointers must be the same
  EXPECT_EQ(status_request_packet.cdata(), buffer);
  EXPECT_EQ(status_request_packet.data(), buffer);
  EXPECT_EQ(status_request_packet.begin(), buffer);
  EXPECT_EQ(status_request_packet.cbegin(), buffer);
  EXPECT_EQ(status_request_packet.end(), buffer + STATUS_REQ_SIZE);
  EXPECT_EQ(status_request_packet.cend(), buffer + STATUS_REQ_SIZE);

  EXPECT_EQ(status_request_packet.size(), STATUS_REQ_SIZE);
  EXPECT_EQ(status_request_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ STATUS_REQ_SIZE });

  ASSERT_TRUE(status_request_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(status_request_packet.get_type().value()),
            STATUS_REQ_TYPE);
  EXPECT_EQ(status_request_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ STATUS_REQ_TYPE });

  EXPECT_EQ(status_request_packet.get_seq(), STATUS_REQ_SEQ_TEST_VALUE);
  EXPECT_EQ(status_request_packet.data()[STATUS_REQ_SEQ_POS],
            piwo::net_byte_t{ STATUS_REQ_SEQ_TEST_VALUE });

  EXPECT_EQ(status_request_packet.get_uid().data, uid_buffer);

  std::array<uint8_t, UID_SIZE> status_request_packet_uid_data;
  std::copy_n(reinterpret_cast<uint8_t*>(status_request_packet.data() + STATUS_REQ_UID_POS),
                UID_SIZE, status_request_packet_uid_data.data());

  EXPECT_EQ(status_request_packet_uid_data, uid_buffer);
}

TEST(StatusRequestTest, make_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = STATUS_REQ_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto status_request_builder_opt =
    piwo::status_request_builder::make_status_request_builder(raw_packet);

  EXPECT_FALSE(status_request_builder_opt.has_value());
}

TEST(StatusRequestTest, make_status_request_fail_buffer_too_small)
{
  auto             too_small_buffer_size = STATUS_REQ_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto status_request_opt =
    piwo::status_request::make_status_request(raw_packet);

  EXPECT_FALSE(status_request_opt.has_value());
}

TEST(StatusRequestTest, make_status_request_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = STATUS_REQ_SIZE + 1;
  piwo::net_byte_t buffer[STATUS_REQ_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, STATUS_REQ_SIZE);

  auto status_request_opt =
    piwo::status_request::make_status_request(raw_packet);

  EXPECT_FALSE(status_request_opt.has_value());

  inconsistent_frame_length = STATUS_REQ_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  status_request_opt = piwo::status_request::make_status_request(raw_packet);

  EXPECT_FALSE(status_request_opt.has_value());
}

TEST(StatusRequestTest, make_status_request_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[STATUS_REQ_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ STATUS_REQ_SIZE };

  piwo::raw_packet raw_packet(buffer, STATUS_REQ_SIZE);

  auto status_request_opt =
    piwo::status_request::make_status_request(raw_packet);

  EXPECT_FALSE(status_request_opt.has_value());
}
