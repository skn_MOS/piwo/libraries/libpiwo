#include "proto.h"

#include <gtest/gtest.h>

namespace
{
constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t CONST_COLOR_LA_SIZE = 7;

constexpr uint8_t CONST_COLOR_LA_TYPE      = 0x07;
constexpr uint8_t CONST_COLOR_LA_SEQ_POS   = 2;
constexpr uint8_t CONST_COLOR_LA_POS       = 3;
constexpr uint8_t CONST_COLOR_LA_RED_POS   = 4;
constexpr uint8_t CONST_COLOR_LA_GREEN_POS = 5;
constexpr uint8_t CONST_COLOR_LA_BLUE_POS  = 6;

constexpr uint8_t CONST_COLOR_LA_SEQ_TEST_VALUE   = 10;
constexpr uint8_t CONST_COLOR_LA_RED_TEST_VALUE   = 200;
constexpr uint8_t CONST_COLOR_LA_GREEN_TEST_VALUE = 210;
constexpr uint8_t CONST_COLOR_LA_BLUE_TEST_VALUE  = 220;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(ConstantColorLaTest, build_and_make_constant_color_la_success)
{
  piwo::net_byte_t   buffer[CONST_COLOR_LA_SIZE];
  piwo::packed_color color(CONST_COLOR_LA_RED_TEST_VALUE,
                           CONST_COLOR_LA_GREEN_TEST_VALUE,
                           CONST_COLOR_LA_BLUE_TEST_VALUE);

  piwo::raw_packet raw_packet(buffer, CONST_COLOR_LA_SIZE);

  auto constant_color_la_builder_opt =
    piwo::constant_color_la_builder::make_constant_color_la_builder(raw_packet);

  ASSERT_TRUE(constant_color_la_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_TYPE });

  auto constant_color_la_builder = *constant_color_la_builder_opt;
  constant_color_la_builder.set_seq(CONST_COLOR_LA_SEQ_TEST_VALUE);
  constant_color_la_builder.set_color(color);

  piwo::constant_color_la constant_color_la_packet(constant_color_la_builder);

  // Pointers must be the same
  EXPECT_EQ(constant_color_la_packet.cdata(), buffer);
  EXPECT_EQ(constant_color_la_packet.data(), buffer);
  EXPECT_EQ(constant_color_la_packet.cdata(), buffer);
  EXPECT_EQ(constant_color_la_packet.begin(), buffer);
  EXPECT_EQ(constant_color_la_packet.cbegin(), buffer);
  EXPECT_EQ(constant_color_la_packet.end(), buffer + CONST_COLOR_LA_SIZE);
  EXPECT_EQ(constant_color_la_packet.cend(), buffer + CONST_COLOR_LA_SIZE);

  EXPECT_EQ(constant_color_la_packet.size(), CONST_COLOR_LA_SIZE);
  EXPECT_EQ(constant_color_la_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_SIZE });

  ASSERT_TRUE(constant_color_la_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(constant_color_la_packet.get_type().value()),
            CONST_COLOR_LA_TYPE);
  EXPECT_EQ(constant_color_la_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_TYPE });

  EXPECT_EQ(constant_color_la_packet.get_seq(), CONST_COLOR_LA_SEQ_TEST_VALUE);
  EXPECT_EQ(constant_color_la_packet.data()[CONST_COLOR_LA_SEQ_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_SEQ_TEST_VALUE });

  EXPECT_EQ(constant_color_la_packet.get_color().get_red(),
            CONST_COLOR_LA_RED_TEST_VALUE);
  EXPECT_EQ(constant_color_la_packet.get_color().get_green(),
            CONST_COLOR_LA_GREEN_TEST_VALUE);
  EXPECT_EQ(constant_color_la_packet.get_color().get_blue(),
            CONST_COLOR_LA_BLUE_TEST_VALUE);
  EXPECT_EQ(constant_color_la_packet.data()[CONST_COLOR_LA_RED_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_RED_TEST_VALUE });
  EXPECT_EQ(constant_color_la_packet.data()[CONST_COLOR_LA_GREEN_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_GREEN_TEST_VALUE });
  EXPECT_EQ(constant_color_la_packet.data()[CONST_COLOR_LA_BLUE_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_BLUE_TEST_VALUE });
}

TEST(ConstantColorLaTest, make_const_color_success)
{
  piwo::net_byte_t buffer[CONST_COLOR_LA_SIZE];

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ CONST_COLOR_LA_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ CONST_COLOR_LA_TYPE };
  buffer[CONST_COLOR_LA_SEQ_POS] =
    piwo::net_byte_t{ CONST_COLOR_LA_SEQ_TEST_VALUE };
  buffer[CONST_COLOR_LA_RED_POS] =
    piwo::net_byte_t{ CONST_COLOR_LA_RED_TEST_VALUE };
  buffer[CONST_COLOR_LA_GREEN_POS] =
    piwo::net_byte_t{ CONST_COLOR_LA_GREEN_TEST_VALUE };
  buffer[CONST_COLOR_LA_BLUE_POS] =
    piwo::net_byte_t{ CONST_COLOR_LA_BLUE_TEST_VALUE };

  piwo::raw_packet raw_packet(buffer, CONST_COLOR_LA_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), CONST_COLOR_LA_TYPE);

  auto const_color_la_packet_opt =
    piwo::constant_color_la::make_constant_color_la(raw_packet);

  ASSERT_TRUE(const_color_la_packet_opt.has_value());

  auto constant_color_la_packet = const_color_la_packet_opt.value();

  // //Pointers must be the same
  EXPECT_EQ(constant_color_la_packet.cdata(), buffer);
  EXPECT_EQ(constant_color_la_packet.data(), buffer);
  EXPECT_EQ(constant_color_la_packet.begin(), buffer);
  EXPECT_EQ(constant_color_la_packet.cbegin(), buffer);
  EXPECT_EQ(constant_color_la_packet.end(), buffer + CONST_COLOR_LA_SIZE);
  EXPECT_EQ(constant_color_la_packet.cend(), buffer + CONST_COLOR_LA_SIZE);

  EXPECT_EQ(constant_color_la_packet.size(), CONST_COLOR_LA_SIZE);
  EXPECT_EQ(constant_color_la_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_SIZE });

  ASSERT_TRUE(constant_color_la_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(constant_color_la_packet.get_type().value()),
            CONST_COLOR_LA_TYPE);
  EXPECT_EQ(constant_color_la_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_TYPE });

  EXPECT_EQ(constant_color_la_packet.get_seq(), CONST_COLOR_LA_SEQ_TEST_VALUE);
  EXPECT_EQ(constant_color_la_packet.data()[CONST_COLOR_LA_SEQ_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_SEQ_TEST_VALUE });

  EXPECT_EQ(constant_color_la_packet.get_color().get_red(),
            CONST_COLOR_LA_RED_TEST_VALUE);
  EXPECT_EQ(constant_color_la_packet.get_color().get_green(),
            CONST_COLOR_LA_GREEN_TEST_VALUE);
  EXPECT_EQ(constant_color_la_packet.get_color().get_blue(),
            CONST_COLOR_LA_BLUE_TEST_VALUE);
  EXPECT_EQ(constant_color_la_packet.data()[CONST_COLOR_LA_RED_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_RED_TEST_VALUE });
  EXPECT_EQ(constant_color_la_packet.data()[CONST_COLOR_LA_GREEN_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_GREEN_TEST_VALUE });
  EXPECT_EQ(constant_color_la_packet.data()[CONST_COLOR_LA_BLUE_POS],
            piwo::net_byte_t{ CONST_COLOR_LA_BLUE_TEST_VALUE });
}

TEST(ConstantColorLaTest, make_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = CONST_COLOR_LA_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto constant_color_la_builder_opt =
    piwo::constant_color_la_builder::make_constant_color_la_builder(raw_packet);

  EXPECT_FALSE(constant_color_la_builder_opt.has_value());
}

TEST(ConstantColorLaTest, make_constant_color_la_fail_buffer_too_small)
{
  auto             too_small_buffer_size = CONST_COLOR_LA_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto const_color_la_opt =
    piwo::constant_color_la::make_constant_color_la(raw_packet);

  EXPECT_FALSE(const_color_la_opt.has_value());
}

TEST(ConstantColorLaTest, make_constant_color_la_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = CONST_COLOR_LA_SIZE + 1;
  piwo::net_byte_t buffer[CONST_COLOR_LA_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, CONST_COLOR_LA_SIZE);

  auto const_color_la_opt =
    piwo::constant_color_la::make_constant_color_la(raw_packet);

  EXPECT_FALSE(const_color_la_opt.has_value());

  inconsistent_frame_length = CONST_COLOR_LA_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  const_color_la_opt =
    piwo::constant_color_la::make_constant_color_la(raw_packet);

  EXPECT_FALSE(const_color_la_opt.has_value());
}

TEST(ConstantColorLaTest, make_constant_color_la_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[CONST_COLOR_LA_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ CONST_COLOR_LA_SIZE };

  piwo::raw_packet raw_packet(buffer, CONST_COLOR_LA_SIZE);

  auto const_color_la_opt =
    piwo::constant_color_la::make_constant_color_la(raw_packet);

  EXPECT_FALSE(const_color_la_opt.has_value());
}
