#include "lp_proto.h"

#include <gtest/gtest.h>

using piwo::underlay_cast;

namespace
{

constexpr size_t LP_RELOAD_SHM_SIZE     = 0x1;
constexpr size_t LP_RELOAD_SHM_BAD_SIZE = 0x0;
constexpr size_t COMMON_PACKET_TYPE_POS = 0x0;
constexpr size_t LP_RELOAD_SHM_TYPE     = 0x3;

} // namespace

TEST(LpReloadShmTest, lp_reload_shm_packet_is_successfully_encoded_and_decoded)
{
  piwo::lp::pipe_byte_t buffer[LP_RELOAD_SHM_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_RELOAD_SHM_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_reload_shm_builder::make_lp_reload_shm_builder(raw_packet);

  ASSERT_TRUE(builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_RELOAD_SHM_TYPE });

  piwo::lp::lp_reload_shm reload_shm_packet(builder_opt.value());

  EXPECT_EQ(reload_shm_packet.size(), LP_RELOAD_SHM_SIZE);

  EXPECT_EQ(reload_shm_packet.cdata(), buffer);
  EXPECT_EQ(reload_shm_packet.data(), buffer);
  EXPECT_EQ(reload_shm_packet.begin(), buffer);
  EXPECT_EQ(reload_shm_packet.cbegin(), buffer);
  EXPECT_EQ(reload_shm_packet.end(), buffer + LP_RELOAD_SHM_SIZE);
  EXPECT_EQ(reload_shm_packet.cend(), buffer + LP_RELOAD_SHM_SIZE);

  EXPECT_EQ(reload_shm_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_RELOAD_SHM_TYPE });

  ASSERT_TRUE(reload_shm_packet.get_type().has_value());
  EXPECT_EQ(underlay_cast(reload_shm_packet.get_type().value()),
            LP_RELOAD_SHM_TYPE);
}

TEST(LpReloadShmTest,
     lp_reload_shm_packet_creation_fails_with_insufficient_packet_buffer)
{
  piwo::lp::pipe_byte_t buffer[LP_RELOAD_SHM_BAD_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_RELOAD_SHM_BAD_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_reload_shm_builder::make_lp_reload_shm_builder(raw_packet);

  ASSERT_FALSE(builder_opt.has_value());
}
