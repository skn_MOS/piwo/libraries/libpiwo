#include "proto.h"

#include <gtest/gtest.h>

namespace
{
constexpr int MEM_CMP_SUCCESS = 0;

constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  TX_SET_RADIO_STATE_SIZE      = 4;
constexpr size_t  UID_SIZE                     = 12;
constexpr uint8_t TX_SET_RADIO_STATE_TYPE      = 0x0D;
constexpr uint8_t TX_SET_RADIO_STATE_SEQ_POS   = 2;
constexpr uint8_t TX_SET_RADIO_STATE_STATE_POS = 3;

constexpr uint8_t              TX_SET_RADIO_STATE_SEQ_TEST_VALUE = 16;
constexpr piwo::tx_radio_state TX_SET_RADIO_STATE_STATE_TEST_VALUE =
  piwo::tx_radio_state::ENABLED;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(TxSetRadioStateTest, build_and_make_tx_set_radio_state_success)
{
  piwo::net_byte_t buffer[TX_SET_RADIO_STATE_SIZE];

  piwo::raw_packet raw_packet(buffer, TX_SET_RADIO_STATE_SIZE);

  auto tx_set_radio_state_builder_opt =
    piwo::tx_set_radio_state_builder::make_tx_set_radio_state_builder(
      raw_packet);

  ASSERT_TRUE(tx_set_radio_state_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ TX_SET_RADIO_STATE_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ TX_SET_RADIO_STATE_TYPE });

  auto tx_set_radio_state_builder = *tx_set_radio_state_builder_opt;
  tx_set_radio_state_builder.set_seq(TX_SET_RADIO_STATE_SEQ_TEST_VALUE);
  tx_set_radio_state_builder.set_state(TX_SET_RADIO_STATE_STATE_TEST_VALUE);

  piwo::tx_set_radio_state tx_set_radio_state_packet(
    tx_set_radio_state_builder);

  // Pointers must be the same
  EXPECT_EQ(tx_set_radio_state_packet.cdata(), buffer);
  EXPECT_EQ(tx_set_radio_state_packet.data(), buffer);
  EXPECT_EQ(tx_set_radio_state_packet.cdata(), buffer);
  EXPECT_EQ(tx_set_radio_state_packet.begin(), buffer);
  EXPECT_EQ(tx_set_radio_state_packet.cbegin(), buffer);
  EXPECT_EQ(tx_set_radio_state_packet.end(), buffer + TX_SET_RADIO_STATE_SIZE);
  EXPECT_EQ(tx_set_radio_state_packet.cend(), buffer + TX_SET_RADIO_STATE_SIZE);

  EXPECT_EQ(tx_set_radio_state_packet.size(), TX_SET_RADIO_STATE_SIZE);
  EXPECT_EQ(tx_set_radio_state_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ TX_SET_RADIO_STATE_SIZE });

  ASSERT_TRUE(tx_set_radio_state_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(tx_set_radio_state_packet.get_type().value()),
            TX_SET_RADIO_STATE_TYPE);
  EXPECT_EQ(tx_set_radio_state_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ TX_SET_RADIO_STATE_TYPE });

  EXPECT_EQ(tx_set_radio_state_packet.get_seq(),
            TX_SET_RADIO_STATE_SEQ_TEST_VALUE);
  EXPECT_EQ(tx_set_radio_state_packet.data()[TX_SET_RADIO_STATE_SEQ_POS],
            piwo::net_byte_t{ TX_SET_RADIO_STATE_SEQ_TEST_VALUE });

  EXPECT_EQ(tx_set_radio_state_packet.get_state(),
            TX_SET_RADIO_STATE_STATE_TEST_VALUE);
  EXPECT_EQ(tx_set_radio_state_packet.data()[TX_SET_RADIO_STATE_STATE_POS],
            static_cast<piwo::net_byte_t>(TX_SET_RADIO_STATE_STATE_TEST_VALUE));
}

TEST(TxSetRadioStateTest, make_tx_set_radio_state_success)
{
  piwo::net_byte_t buffer[TX_SET_RADIO_STATE_SIZE];

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ TX_SET_RADIO_STATE_SIZE };
  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ TX_SET_RADIO_STATE_TYPE };
  buffer[TX_SET_RADIO_STATE_SEQ_POS] =
    piwo::net_byte_t{ TX_SET_RADIO_STATE_SEQ_TEST_VALUE };
  buffer[TX_SET_RADIO_STATE_STATE_POS] =
    static_cast<piwo::net_byte_t>(TX_SET_RADIO_STATE_STATE_TEST_VALUE);

  piwo::raw_packet raw_packet(buffer, TX_SET_RADIO_STATE_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), TX_SET_RADIO_STATE_TYPE);

  auto tx_set_radio_state_packet_opt =
    piwo::tx_set_radio_state::make_tx_set_radio_state(raw_packet);

  ASSERT_TRUE(tx_set_radio_state_packet_opt.has_value());

  auto tx_set_radio_state_packet = tx_set_radio_state_packet_opt.value();

  // Pointers must be the same
  EXPECT_EQ(tx_set_radio_state_packet.cdata(), buffer);
  EXPECT_EQ(tx_set_radio_state_packet.data(), buffer);
  EXPECT_EQ(tx_set_radio_state_packet.begin(), buffer);
  EXPECT_EQ(tx_set_radio_state_packet.cbegin(), buffer);
  EXPECT_EQ(tx_set_radio_state_packet.end(), buffer + TX_SET_RADIO_STATE_SIZE);
  EXPECT_EQ(tx_set_radio_state_packet.cend(), buffer + TX_SET_RADIO_STATE_SIZE);

  EXPECT_EQ(tx_set_radio_state_packet.size(), TX_SET_RADIO_STATE_SIZE);
  EXPECT_EQ(tx_set_radio_state_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ TX_SET_RADIO_STATE_SIZE });

  ASSERT_TRUE(tx_set_radio_state_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(tx_set_radio_state_packet.get_type().value()),
            TX_SET_RADIO_STATE_TYPE);
  EXPECT_EQ(tx_set_radio_state_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ TX_SET_RADIO_STATE_TYPE });

  EXPECT_EQ(tx_set_radio_state_packet.get_seq(),
            TX_SET_RADIO_STATE_SEQ_TEST_VALUE);
  EXPECT_EQ(tx_set_radio_state_packet.data()[TX_SET_RADIO_STATE_SEQ_POS],
            piwo::net_byte_t{ TX_SET_RADIO_STATE_SEQ_TEST_VALUE });

  EXPECT_EQ(tx_set_radio_state_packet.get_state(),
            TX_SET_RADIO_STATE_STATE_TEST_VALUE);
  EXPECT_EQ(tx_set_radio_state_packet.data()[TX_SET_RADIO_STATE_STATE_POS],
            static_cast<piwo::net_byte_t>(TX_SET_RADIO_STATE_STATE_TEST_VALUE));
}

TEST(TxSetRadioStateTest, make_tx_set_radio_state_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = TX_SET_RADIO_STATE_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto tx_set_radio_state_builder_opt =
    piwo::tx_set_radio_state_builder::make_tx_set_radio_state_builder(
      raw_packet);

  EXPECT_FALSE(tx_set_radio_state_builder_opt.has_value());
}

TEST(TxSetRadioStateTest, make_tx_set_radio_state_fail_buffer_too_small)
{
  auto             too_small_buffer_size = TX_SET_RADIO_STATE_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto tx_set_radio_state_opt =
    piwo::tx_set_radio_state::make_tx_set_radio_state(raw_packet);

  EXPECT_FALSE(tx_set_radio_state_opt.has_value());
}

TEST(TxSetRadioStateTest,
     make_tx_set_radio_state_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = TX_SET_RADIO_STATE_SIZE + 1;
  piwo::net_byte_t buffer[TX_SET_RADIO_STATE_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, TX_SET_RADIO_STATE_SIZE);

  auto tx_set_radio_state_opt =
    piwo::tx_set_radio_state::make_tx_set_radio_state(raw_packet);

  EXPECT_FALSE(tx_set_radio_state_opt.has_value());

  inconsistent_frame_length = TX_SET_RADIO_STATE_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  tx_set_radio_state_opt =
    piwo::tx_set_radio_state::make_tx_set_radio_state(raw_packet);

  EXPECT_FALSE(tx_set_radio_state_opt.has_value());
}

TEST(TxSetRadioStateTest, make_tx_set_radio_state_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[TX_SET_RADIO_STATE_SIZE];
  buffer[COMMON_PACKET_TYPE_POS] = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ TX_SET_RADIO_STATE_SIZE };

  piwo::raw_packet raw_packet(buffer, TX_SET_RADIO_STATE_SIZE);

  auto tx_set_radio_state_opt =
    piwo::tx_set_radio_state::make_tx_set_radio_state(raw_packet);

  EXPECT_FALSE(tx_set_radio_state_opt.has_value());
}
