#include "proto.h"

#include <gtest/gtest.h>

namespace
{
constexpr int MEM_CMP_SUCCESS = 0;

constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  TX_ASSIGN_CHANNEL_SIZE        = 4;
constexpr size_t  UID_SIZE                      = 12;
constexpr uint8_t TX_ASSIGN_CHANNEL_TYPE        = 0x0E;
constexpr uint8_t TX_ASSIGN_CHANNEL_SEQ_POS     = 2;
constexpr uint8_t TX_ASSIGN_CHANNEL_CHANNEL_POS = 3;

constexpr uint8_t TX_ASSIGN_CHANNEL_SEQ_TEST_VALUE     = 16;
constexpr uint8_t TX_ASSIGN_CHANNEL_CHANNEL_TEST_VALUE = 17;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(TxAssignChannelTest, build_and_make_tx_assign_channel_success)
{
  piwo::net_byte_t buffer[TX_ASSIGN_CHANNEL_SIZE];

  piwo::raw_packet raw_packet(buffer, TX_ASSIGN_CHANNEL_SIZE);

  auto tx_assign_channel_builder_opt =
    piwo::tx_assign_channel_builder::make_tx_assign_channel_builder(raw_packet);

  ASSERT_TRUE(tx_assign_channel_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_TYPE });

  auto tx_assign_channel_builder = *tx_assign_channel_builder_opt;
  tx_assign_channel_builder.set_seq(TX_ASSIGN_CHANNEL_SEQ_TEST_VALUE);
  tx_assign_channel_builder.set_channel(TX_ASSIGN_CHANNEL_CHANNEL_TEST_VALUE);

  piwo::tx_assign_channel tx_assign_channel_packet(tx_assign_channel_builder);

  // Pointers must be the same
  EXPECT_EQ(tx_assign_channel_packet.cdata(), buffer);
  EXPECT_EQ(tx_assign_channel_packet.data(), buffer);
  EXPECT_EQ(tx_assign_channel_packet.cdata(), buffer);
  EXPECT_EQ(tx_assign_channel_packet.begin(), buffer);
  EXPECT_EQ(tx_assign_channel_packet.cbegin(), buffer);
  EXPECT_EQ(tx_assign_channel_packet.end(), buffer + TX_ASSIGN_CHANNEL_SIZE);
  EXPECT_EQ(tx_assign_channel_packet.cend(), buffer + TX_ASSIGN_CHANNEL_SIZE);

  EXPECT_EQ(tx_assign_channel_packet.size(), TX_ASSIGN_CHANNEL_SIZE);
  EXPECT_EQ(tx_assign_channel_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_SIZE });

  ASSERT_TRUE(tx_assign_channel_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(tx_assign_channel_packet.get_type().value()),
            TX_ASSIGN_CHANNEL_TYPE);
  EXPECT_EQ(tx_assign_channel_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_TYPE });

  EXPECT_EQ(tx_assign_channel_packet.get_seq(),
            TX_ASSIGN_CHANNEL_SEQ_TEST_VALUE);
  EXPECT_EQ(tx_assign_channel_packet.data()[TX_ASSIGN_CHANNEL_SEQ_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_SEQ_TEST_VALUE });

  EXPECT_EQ(tx_assign_channel_packet.get_channel(),
            TX_ASSIGN_CHANNEL_CHANNEL_TEST_VALUE);
  EXPECT_EQ(tx_assign_channel_packet.data()[TX_ASSIGN_CHANNEL_CHANNEL_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_CHANNEL_TEST_VALUE });
}

TEST(TxAssignChannelTest, make_tx_assign_channel_success)
{
  piwo::net_byte_t buffer[TX_ASSIGN_CHANNEL_SIZE];

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ TX_ASSIGN_CHANNEL_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ TX_ASSIGN_CHANNEL_TYPE };
  buffer[TX_ASSIGN_CHANNEL_SEQ_POS] =
    piwo::net_byte_t{ TX_ASSIGN_CHANNEL_SEQ_TEST_VALUE };
  buffer[TX_ASSIGN_CHANNEL_CHANNEL_POS] =
    piwo::net_byte_t{ TX_ASSIGN_CHANNEL_CHANNEL_TEST_VALUE };

  piwo::raw_packet raw_packet(buffer, TX_ASSIGN_CHANNEL_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), TX_ASSIGN_CHANNEL_TYPE);

  auto tx_assign_channel_packet_opt =
    piwo::tx_assign_channel::make_tx_assign_channel(raw_packet);

  ASSERT_TRUE(tx_assign_channel_packet_opt.has_value());

  auto tx_assign_channel_packet = tx_assign_channel_packet_opt.value();

  // Pointers must be the same
  EXPECT_EQ(tx_assign_channel_packet.cdata(), buffer);
  EXPECT_EQ(tx_assign_channel_packet.data(), buffer);
  EXPECT_EQ(tx_assign_channel_packet.begin(), buffer);
  EXPECT_EQ(tx_assign_channel_packet.cbegin(), buffer);
  EXPECT_EQ(tx_assign_channel_packet.end(), buffer + TX_ASSIGN_CHANNEL_SIZE);
  EXPECT_EQ(tx_assign_channel_packet.cend(), buffer + TX_ASSIGN_CHANNEL_SIZE);

  EXPECT_EQ(tx_assign_channel_packet.size(), TX_ASSIGN_CHANNEL_SIZE);
  EXPECT_EQ(tx_assign_channel_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_SIZE });

  ASSERT_TRUE(tx_assign_channel_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(tx_assign_channel_packet.get_type().value()),
            TX_ASSIGN_CHANNEL_TYPE);
  EXPECT_EQ(tx_assign_channel_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_TYPE });

  EXPECT_EQ(tx_assign_channel_packet.get_seq(),
            TX_ASSIGN_CHANNEL_SEQ_TEST_VALUE);
  EXPECT_EQ(tx_assign_channel_packet.data()[TX_ASSIGN_CHANNEL_SEQ_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_SEQ_TEST_VALUE });

  EXPECT_EQ(tx_assign_channel_packet.get_channel(),
            TX_ASSIGN_CHANNEL_CHANNEL_TEST_VALUE);
  EXPECT_EQ(tx_assign_channel_packet.data()[TX_ASSIGN_CHANNEL_CHANNEL_POS],
            piwo::net_byte_t{ TX_ASSIGN_CHANNEL_CHANNEL_TEST_VALUE });
}

TEST(TxAssignChannelTest, make_tx_assign_channel_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = TX_ASSIGN_CHANNEL_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto tx_assign_channel_builder_opt =
    piwo::tx_assign_channel_builder::make_tx_assign_channel_builder(raw_packet);

  EXPECT_FALSE(tx_assign_channel_builder_opt.has_value());
}

TEST(TxAssignChannelTest, make_tx_assign_channel_fail_buffer_too_small)
{
  auto             too_small_buffer_size = TX_ASSIGN_CHANNEL_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto tx_assign_channel_opt =
    piwo::tx_assign_channel::make_tx_assign_channel(raw_packet);

  EXPECT_FALSE(tx_assign_channel_opt.has_value());
}

TEST(TxAssignChannelTest, make_tx_assign_channel_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = TX_ASSIGN_CHANNEL_SIZE + 1;
  piwo::net_byte_t buffer[TX_ASSIGN_CHANNEL_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, TX_ASSIGN_CHANNEL_SIZE);

  auto tx_assign_channel_opt =
    piwo::tx_assign_channel::make_tx_assign_channel(raw_packet);

  EXPECT_FALSE(tx_assign_channel_opt.has_value());

  inconsistent_frame_length = TX_ASSIGN_CHANNEL_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  tx_assign_channel_opt =
    piwo::tx_assign_channel::make_tx_assign_channel(raw_packet);

  EXPECT_FALSE(tx_assign_channel_opt.has_value());
}

TEST(TxAssignChannelTest, make_tx_assign_channel_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[TX_ASSIGN_CHANNEL_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ TX_ASSIGN_CHANNEL_SIZE };

  piwo::raw_packet raw_packet(buffer, TX_ASSIGN_CHANNEL_SIZE);

  auto tx_assign_channel_opt =
    piwo::tx_assign_channel::make_tx_assign_channel(raw_packet);

  EXPECT_FALSE(tx_assign_channel_opt.has_value());
}
