#include "proto.h"

#include <gtest/gtest.h>

namespace
{
constexpr int MEM_CMP_SUCCESS = 0;

constexpr uint8_t COMMON_PACKET_LENGTH_POS = 1;
constexpr uint8_t COMMON_PACKET_TYPE_POS   = 0;

constexpr size_t  NEW_PALETTE_SIZE              = 5;
constexpr uint8_t NEW_PALETTE_TYPE              = 0x0A;
constexpr uint8_t NEW_PALETTE_PALETTE_INDEX_POS = 2;
constexpr uint8_t NEW_PALETTE_PALETTE_SIZE_POS  = 3;

constexpr uint8_t  NEW_PALETTE_PALETTE_INDEX_TEST_VALUE = 17;
constexpr uint16_t NEW_PALETTE_PALETTE_SIZE_TEST_VALUE  = 1234;

constexpr uint8_t UNKNOWN_FRAME = 255;
} // namespace

TEST(NewPaletteTest, build_and_make_new_palette_success)
{
  piwo::net_byte_t buffer[NEW_PALETTE_SIZE];

  piwo::raw_packet raw_packet(buffer, NEW_PALETTE_SIZE);

  auto new_palette_builder_opt =
    piwo::new_palette_builder::make_new_palette_builder(raw_packet);

  ASSERT_TRUE(new_palette_builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ NEW_PALETTE_SIZE });
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ NEW_PALETTE_TYPE });

  auto new_palette_builder = *new_palette_builder_opt;
  new_palette_builder.set_palette_index(NEW_PALETTE_PALETTE_INDEX_TEST_VALUE);
  new_palette_builder.set_palette_size(NEW_PALETTE_PALETTE_SIZE_TEST_VALUE);

  piwo::new_palette new_palette_packet(new_palette_builder);

  // Pointers must be the same
  EXPECT_EQ(new_palette_packet.cdata(), buffer);
  EXPECT_EQ(new_palette_packet.data(), buffer);
  EXPECT_EQ(new_palette_packet.cdata(), buffer);
  EXPECT_EQ(new_palette_packet.begin(), buffer);
  EXPECT_EQ(new_palette_packet.cbegin(), buffer);
  EXPECT_EQ(new_palette_packet.end(), buffer + NEW_PALETTE_SIZE);
  EXPECT_EQ(new_palette_packet.cend(), buffer + NEW_PALETTE_SIZE);

  EXPECT_EQ(new_palette_packet.size(), NEW_PALETTE_SIZE);
  EXPECT_EQ(new_palette_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ NEW_PALETTE_SIZE });

  ASSERT_TRUE(new_palette_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(new_palette_packet.get_type().value()),
            NEW_PALETTE_TYPE);
  EXPECT_EQ(new_palette_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ NEW_PALETTE_TYPE });

  EXPECT_EQ(new_palette_packet.get_palette_index(),
            NEW_PALETTE_PALETTE_INDEX_TEST_VALUE);
  EXPECT_EQ(new_palette_packet.data()[NEW_PALETTE_PALETTE_INDEX_POS],
            piwo::net_byte_t{ NEW_PALETTE_PALETTE_INDEX_TEST_VALUE });

  EXPECT_EQ(new_palette_packet.get_palette_size(),
            NEW_PALETTE_PALETTE_SIZE_TEST_VALUE);
}

TEST(NewPaletteTest, make_new_palette_success)
{
  piwo::net_byte_t buffer[NEW_PALETTE_SIZE];
  uint16_t         palette_size = htole16(NEW_PALETTE_PALETTE_SIZE_TEST_VALUE);

  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ NEW_PALETTE_SIZE };
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ NEW_PALETTE_TYPE };
  buffer[NEW_PALETTE_PALETTE_INDEX_POS] =
    piwo::net_byte_t{ NEW_PALETTE_PALETTE_INDEX_TEST_VALUE };

  std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&palette_size),
              sizeof(palette_size),
              buffer + NEW_PALETTE_PALETTE_SIZE_POS);

  piwo::raw_packet raw_packet(buffer, NEW_PALETTE_SIZE);

  auto readed_frame_type_opt = raw_packet.get_type();

  ASSERT_TRUE(readed_frame_type_opt.has_value());

  auto readed_frame_type = readed_frame_type_opt.value();

  EXPECT_EQ(underlay_cast(readed_frame_type), NEW_PALETTE_TYPE);

  auto new_palette_packet_opt = piwo::new_palette::make_new_palette(raw_packet);

  ASSERT_TRUE(new_palette_packet_opt.has_value());

  auto new_palette_packet = new_palette_packet_opt.value();

  // Pointers must be the same
  EXPECT_EQ(new_palette_packet.cdata(), buffer);
  EXPECT_EQ(new_palette_packet.data(), buffer);
  EXPECT_EQ(new_palette_packet.begin(), buffer);
  EXPECT_EQ(new_palette_packet.cbegin(), buffer);
  EXPECT_EQ(new_palette_packet.end(), buffer + NEW_PALETTE_SIZE);
  EXPECT_EQ(new_palette_packet.cend(), buffer + NEW_PALETTE_SIZE);

  EXPECT_EQ(new_palette_packet.size(), NEW_PALETTE_SIZE);
  EXPECT_EQ(new_palette_packet.data()[COMMON_PACKET_LENGTH_POS],
            piwo::net_byte_t{ NEW_PALETTE_SIZE });

  ASSERT_TRUE(new_palette_packet.get_type().has_value());

  EXPECT_EQ(underlay_cast(new_palette_packet.get_type().value()),
            NEW_PALETTE_TYPE);

  EXPECT_EQ(new_palette_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::net_byte_t{ NEW_PALETTE_TYPE });

  EXPECT_EQ(new_palette_packet.get_palette_index(),
            NEW_PALETTE_PALETTE_INDEX_TEST_VALUE);
  EXPECT_EQ(new_palette_packet.data()[NEW_PALETTE_PALETTE_INDEX_POS],
            piwo::net_byte_t{ NEW_PALETTE_PALETTE_INDEX_TEST_VALUE });

  EXPECT_EQ(new_palette_packet.get_palette_size(),
            NEW_PALETTE_PALETTE_SIZE_TEST_VALUE);
}

TEST(NewPaletteTest, make_new_palette_builder_fail_buffer_too_small)
{
  auto             too_small_buffer_size = NEW_PALETTE_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto new_palette_builder_opt =
    piwo::new_palette_builder::make_new_palette_builder(raw_packet);

  EXPECT_FALSE(new_palette_builder_opt.has_value());
}

TEST(NewPaletteTest, make_new_palette_fail_buffer_too_small)
{
  auto             too_small_buffer_size = NEW_PALETTE_SIZE - 1;
  piwo::net_byte_t buffer[too_small_buffer_size];

  piwo::raw_packet raw_packet(buffer, too_small_buffer_size);

  auto new_palette_opt = piwo::new_palette::make_new_palette(raw_packet);

  EXPECT_FALSE(new_palette_opt.has_value());
}

TEST(NewPaletteTest, make_new_palette_fail_inconsistent_frame_length)
{
  auto             inconsistent_frame_length = NEW_PALETTE_SIZE + 1;
  piwo::net_byte_t buffer[NEW_PALETTE_SIZE];
  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  piwo::raw_packet raw_packet(buffer, NEW_PALETTE_SIZE);

  auto new_palette_opt = piwo::new_palette::make_new_palette(raw_packet);

  EXPECT_FALSE(new_palette_opt.has_value());

  inconsistent_frame_length = NEW_PALETTE_SIZE - 1;

  buffer[COMMON_PACKET_LENGTH_POS] =
    piwo::net_byte_t{ static_cast<uint8_t>(inconsistent_frame_length) };

  new_palette_opt = piwo::new_palette::make_new_palette(raw_packet);

  EXPECT_FALSE(new_palette_opt.has_value());
}

TEST(NewPaletteTest, make_new_palette_fail_unknown_frame_type)
{
  piwo::net_byte_t buffer[NEW_PALETTE_SIZE];
  buffer[COMMON_PACKET_TYPE_POS]   = piwo::net_byte_t{ UNKNOWN_FRAME };
  buffer[COMMON_PACKET_LENGTH_POS] = piwo::net_byte_t{ NEW_PALETTE_SIZE };

  piwo::raw_packet raw_packet(buffer, NEW_PALETTE_SIZE);

  auto new_palette_opt = piwo::new_palette::make_new_palette(raw_packet);

  EXPECT_FALSE(new_palette_opt.has_value());
}
