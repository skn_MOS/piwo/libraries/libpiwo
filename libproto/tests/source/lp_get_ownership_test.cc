#include "lp_proto.h"

#include <gtest/gtest.h>

using piwo::underlay_cast;

namespace
{

constexpr size_t LP_GET_OWNERSHIP_SIZE      = 0x1;
constexpr size_t LP_GET_OWNERSHIP_HUGE_SIZE = 0x829;
constexpr size_t LP_GET_OWNERSHIP_BAD_SIZE  = 0x0;
constexpr size_t COMMON_PACKET_TYPE_POS     = 0x0;
constexpr size_t LP_GET_OWNERSHIP_TYPE      = 0x2;

} // namespace

TEST(LpGetOwnershipTest,
     lp_get_ownership_packet_is_successfully_encoded_and_decoded)
{
  piwo::lp::pipe_byte_t buffer[LP_GET_OWNERSHIP_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_GET_OWNERSHIP_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_get_ownership_builder::make_lp_get_ownership_builder(
      raw_packet);

  ASSERT_TRUE(builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_GET_OWNERSHIP_TYPE });

  piwo::lp::lp_get_ownership get_ownership_packet(builder_opt.value());

  EXPECT_EQ(get_ownership_packet.size(), LP_GET_OWNERSHIP_SIZE);

  EXPECT_EQ(get_ownership_packet.cdata(), buffer);
  EXPECT_EQ(get_ownership_packet.data(), buffer);
  EXPECT_EQ(get_ownership_packet.begin(), buffer);
  EXPECT_EQ(get_ownership_packet.cbegin(), buffer);
  EXPECT_EQ(get_ownership_packet.end(), buffer + LP_GET_OWNERSHIP_SIZE);
  EXPECT_EQ(get_ownership_packet.cend(), buffer + LP_GET_OWNERSHIP_SIZE);

  EXPECT_EQ(get_ownership_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_GET_OWNERSHIP_TYPE });

  ASSERT_TRUE(get_ownership_packet.get_type().has_value());
  EXPECT_EQ(underlay_cast(get_ownership_packet.get_type().value()),
            LP_GET_OWNERSHIP_TYPE);
}

TEST(
  LpGetOwnershipTest,
  lp_get_ownership_packet_is_successfully_encoded_and_decoded_with_buffer_bigger_than_necessary)
{
  static_assert(LP_GET_OWNERSHIP_HUGE_SIZE > LP_GET_OWNERSHIP_SIZE);

  piwo::lp::pipe_byte_t buffer[LP_GET_OWNERSHIP_HUGE_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_GET_OWNERSHIP_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_get_ownership_builder::make_lp_get_ownership_builder(
      raw_packet);

  ASSERT_TRUE(builder_opt.has_value());
  EXPECT_EQ(buffer[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_GET_OWNERSHIP_TYPE });

  piwo::lp::lp_get_ownership get_ownership_packet(builder_opt.value());

  EXPECT_EQ(get_ownership_packet.size(), LP_GET_OWNERSHIP_SIZE);

  EXPECT_EQ(get_ownership_packet.cdata(), buffer);
  EXPECT_EQ(get_ownership_packet.data(), buffer);
  EXPECT_EQ(get_ownership_packet.begin(), buffer);
  EXPECT_EQ(get_ownership_packet.cbegin(), buffer);
  EXPECT_EQ(get_ownership_packet.end(), buffer + LP_GET_OWNERSHIP_SIZE);
  EXPECT_EQ(get_ownership_packet.cend(), buffer + LP_GET_OWNERSHIP_SIZE);

  EXPECT_EQ(get_ownership_packet.data()[COMMON_PACKET_TYPE_POS],
            piwo::lp::pipe_byte_t{ LP_GET_OWNERSHIP_TYPE });

  ASSERT_TRUE(get_ownership_packet.get_type().has_value());
  EXPECT_EQ(underlay_cast(get_ownership_packet.get_type().value()),
            LP_GET_OWNERSHIP_TYPE);
}

TEST(LpGetOwnershipTest,
     lp_get_ownership_packet_creation_fails_with_insufficient_packet_buffer)
{
  piwo::lp::pipe_byte_t buffer[LP_GET_OWNERSHIP_BAD_SIZE];

  piwo::lp::raw_packet raw_packet(buffer, LP_GET_OWNERSHIP_BAD_SIZE);

  std::expected builder_opt =
    piwo::lp::lp_get_ownership_builder::make_lp_get_ownership_builder(
      raw_packet);

  ASSERT_FALSE(builder_opt.has_value());
}
