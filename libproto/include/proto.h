#pragma once

#include "protodef.h"

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <optional>
#include <span>
#include <type_traits>

#include "byteorder.h"
#include "compiler.h"

namespace piwo
{

class raw_packet
{
public:
  explicit raw_packet(net_byte_t* packet, const size_t& size)
    : _packet(packet)
    , _size(size)
  {
  }

  raw_packet(const raw_packet& packet)
    : _packet(packet._packet)
    , _size(packet._size)
  {
  }

  size_t
  size() const
  {
    return _size;
  }

  net_byte_t*
  data() const
  {
    return _packet;
  }

  net_byte_t*
  begin() const
  {
    return _packet;
  }

  net_byte_t*
  end() const
  {
    return _packet + _size;
  }

  net_byte_t*
  cdata() const
  {
    return _packet;
  }

  const net_byte_t*
  cbegin() const
  {
    return _packet;
  }

  const net_byte_t*
  cend() const
  {
    return _packet + _size;
  }

  std::optional<packet_type>
  get_type() const noexcept
  {
    switch (static_cast<packet_type>(_packet[common_type_pos]))
    {
      case packet_type::PING:
      case packet_type::ACK:
      case packet_type::STATUS_REQ:
      case packet_type::STATUS_REPLY:
      case packet_type::ASSIGN_LOGIC_ADDRESS:
      case packet_type::LIGHT_SHOW:
      case packet_type::CONST_COLOR:
      case packet_type::CONST_COLOR_LA:
      case packet_type::ASSIGN_PANID:
      case packet_type::ASSIGN_CHANNEL:
      case packet_type::NEW_PALETTE:
      case packet_type::FILL_PALETTE:
      case packet_type::LIGHT_SHOW_W_PALETTE:
      case packet_type::TX_SET_RADIO_STATE:
      case packet_type::TX_ASSIGN_CHANNEL:
      case packet_type::TX_ASSIGN_PANID:
      case packet_type::FORWARD_W_CID:
      case packet_type::BLINK:
        return static_cast<packet_type>(_packet[common_type_pos]);
      default:
        return std::nullopt;
        break;
    }
    return std::nullopt;
  }

protected:
  net_byte_t* _packet;
  size_t      _size;
};

template<class T>
concept specific_packet_c = requires(T a)
{
  std::is_base_of<raw_packet, T>::value && !std::is_same<raw_packet, T>::value;
};

template<class T>
concept redirectable_c = specific_packet_c<T> && requires(T a)
{
  std::is_base_of<packet_has_8_bit_size_tag, T>::value;
};

struct forward_w_cid_encapsulated_packet_t
{
  raw_packet packet;
  uint8_t    cid;
};

class ping_builder
{
public:
  friend class ping;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[ping_seq_pos] = net_byte_t{ seq };
  }

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.data()),
                uid::length,
                this->_raw_packet.begin() + ping_uid_pos);
  }

  static std::optional<ping_builder>
  make_ping_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < ping_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ ping_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::PING) };

    return ping_builder(packet);
  }

private:
  explicit ping_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class ping
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit ping(ping_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  size_t
  size() const
  {
    return ping_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + ping_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + ping_length;
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[ping_seq_pos]);
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  ping_uid_pos) };
  }

  static std::optional<ping>
  make_ping(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < ping_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          ping_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::PING)
    {
      return std::nullopt;
    }
    return ping(packet);
  }

private:
  explicit ping(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class blink_builder
{
public:
  friend class blink;

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.data()),
                uid::length,
                this->_raw_packet.begin() + blink_uid_pos);
  }

  static std::optional<blink_builder>
  make_blink_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < blink_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ blink_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::BLINK) };

    return blink_builder(packet);
  }

private:
  explicit blink_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class blink
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit blink(blink_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  size_t
  size() const
  {
    return blink_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + blink_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + blink_length;
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  blink_uid_pos) };
  }

  static std::optional<blink>
  make_blink(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < blink_length || !packet.get_type().has_value() ||
        packet.get_type() != packet_type::BLINK)
    {
      return std::nullopt;
    }
    return blink(packet);
  }

private:
  explicit blink(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class ack_builder
{
public:
  friend class ack;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[ack_seq_pos] = net_byte_t{ seq };
  }

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.begin()),
                uid::length,
                this->_raw_packet.begin() + ack_uid_pos);
  }

  void
  set_packet_id(const uint8_t packet_id)
  {
    this->_raw_packet.data()[ack_packet_id_pos] = net_byte_t{ packet_id };
  }

  static std::optional<ack_builder>
  make_ack_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < ack_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ ack_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::ACK) };

    return ack_builder(packet);
  }

private:
  explicit ack_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class ack
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit ack(ack_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  size_t
  get_lenght() const noexcept
  {
    return static_cast<size_t>(this->cdata()[common_length_pos]);
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[ack_seq_pos]);
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  ack_uid_pos) };
  }

  uint8_t
  get_packet_id() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[ack_packet_id_pos]);
  }

  size_t
  size() const
  {
    return ack_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + ack_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + ack_length;
  }

  static std::optional<ack>
  make_ack(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < ack_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) != ack_length ||
        !packet.get_type().has_value() || packet.get_type() != packet_type::ACK)
    {
      return std::nullopt;
    }
    return ack(packet);
  }

private:
  explicit ack(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class status_request_builder
{
public:
  friend class status_request;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[status_req_seq_pos] = net_byte_t{ seq };
  }

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.begin()),
                uid::length,
                this->_raw_packet.begin() + status_req_uid_pos);
  }

  static std::optional<status_request_builder>
  make_status_request_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < status_req_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ status_req_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::STATUS_REQ) };

    return status_request_builder(packet);
  }

private:
  explicit status_request_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class status_request
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit status_request(status_request_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[ack_seq_pos]);
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  status_req_uid_pos) };
  }

  size_t
  size() const
  {
    return status_req_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + status_req_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + status_req_length;
  }

  static std::optional<status_request>
  make_status_request(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < status_req_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          status_req_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::STATUS_REQ)
    {
      return std::nullopt;
    }
    return status_request(packet);
  }

private:
  explicit status_request(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class status_reply_builder
{
public:
  friend class status_reply;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[status_reply_seq_pos] = net_byte_t{ seq };
  }

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.begin()),
                uid::length,
                this->_raw_packet.begin() + status_reply_uid_pos);
  }

  void
  set_la(uint8_t la) noexcept
  {
    this->_raw_packet.data()[status_reply_logic_address_pos] = net_byte_t{ la };
  }

  void
  set_rssi(uint8_t rssi) noexcept
  {
    this->_raw_packet.data()[status_reply_rssi_pos] = net_byte_t{ rssi };
  }

  void
  set_up_time_h(uint8_t hours) noexcept
  {
    this->_raw_packet.data()[status_reply_uptime_h_pos] = net_byte_t{ hours };
  }

  void
  set_up_time_m(uint8_t minutes) noexcept
  {
    this->_raw_packet.data()[status_reply_uptime_m_pos] = net_byte_t{ minutes };
  }

  void
  set_up_time_s(uint8_t seconds) noexcept
  {
    this->_raw_packet.data()[status_reply_uptime_s_pos] = net_byte_t{ seconds };
  }

  static std::optional<status_reply_builder>
  make_status_reply_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < status_reply_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ status_reply_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::STATUS_REPLY) };

    return status_reply_builder(packet);
  }

private:
  explicit status_reply_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class status_reply
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit status_reply(status_reply_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[status_reply_seq_pos]);
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  status_reply_uid_pos) };
  }

  uint8_t
  get_la() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[status_reply_logic_address_pos]);
  }

  uint8_t
  get_rssi() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[status_reply_rssi_pos]);
  }

  uint8_t
  get_uptime_h() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[status_reply_uptime_h_pos]);
  }

  uint8_t
  get_uptime_m() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[status_reply_uptime_m_pos]);
  }

  uint8_t
  get_uptime_s() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[status_reply_uptime_s_pos]);
  }

  size_t
  size() const
  {
    return status_reply_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + status_reply_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + status_reply_length;
  }

  static std::optional<status_reply>
  make_status_reply(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < status_reply_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          status_reply_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::STATUS_REPLY)
    {
      return std::nullopt;
    }

    return status_reply(packet);
  }

private:
  explicit status_reply(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class assign_la_builder
{
public:
  friend class assign_la;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[assign_la_seq_pos] = net_byte_t{ seq };
  }

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.begin()),
                uid::length,
                this->_raw_packet.begin() + assign_la_uid_pos);
  }

  void
  set_la(const uint8_t la) noexcept
  {
    this->_raw_packet.data()[assign_la_logic_address_pos] = net_byte_t{ la };
  }

  static std::optional<assign_la_builder>
  make_assign_la_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < assign_la_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ assign_la_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::ASSIGN_LOGIC_ADDRESS) };

    return assign_la_builder(packet);
  }

private:
  explicit assign_la_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class assign_la
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit assign_la(assign_la_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[assign_la_seq_pos]);
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  assign_la_uid_pos) };
  }

  uint8_t
  get_la() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[assign_la_logic_address_pos]);
  }

  size_t
  size() const
  {
    return assign_la_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + assign_la_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + assign_la_length;
  }

  static std::optional<assign_la>
  make_assign_la(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < assign_la_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          assign_la_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::ASSIGN_LOGIC_ADDRESS)
    {
      return std::nullopt;
    }

    return assign_la(packet);
  }

private:
  explicit assign_la(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class lightshow_builder
{
public:
  friend class lightshow;

  void
  set_ttf(const uint8_t ttf) noexcept
  {
    this->_raw_packet.data()[lightshow_ttf_pos] = net_byte_t{ ttf };
  }

  void
  set_first_la(const uint8_t la) noexcept
  {
    this->_raw_packet.data()[lightshow_first_la_pos] = net_byte_t{ la };
  }

  bool
  add_color(packed_color& color) noexcept
  {
    if (_raw_packet.size() < _current_packet_size + packed_color::size)
    {
      return false;
    }

    std::copy_n(reinterpret_cast<piwo::net_byte_t*>(color.data()),
                packed_color::size,
                this->_raw_packet.begin() + _current_packet_size);

    _current_packet_size += packed_color::size;

    return true;
  }

  static std::optional<lightshow_builder>
  make_lightshow_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lightshow_buffer_minimum_length ||
        packet_buffer_size > lightshow_buffer_maximum_length)
    {
      return std::nullopt;
    }

    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::LIGHT_SHOW) };

    return lightshow_builder(packet);
  }

private:
  explicit lightshow_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  uint8_t    _current_packet_size = lightshow_first_color_offset;
  raw_packet _raw_packet;
};

class lightshow
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit lightshow(lightshow_builder builder)
    : raw_packet(builder._raw_packet)
    , _size(builder._current_packet_size)
  {
    this->data()[common_length_pos] = net_byte_t{ static_cast<uint8_t>(_size) };
  }

  uint8_t
  get_ttf() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[lightshow_ttf_pos]);
  }

  uint8_t
  get_first_la() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[lightshow_first_la_pos]);
  }

  size_t
  get_colors_count() const noexcept
  {
    return static_cast<size_t>((_size - lightshow_without_any_color_lenght) /
                               packed_color::size);
  }

  std::optional<packed_color>
  get_color(const uint8_t index)
  {
    packed_color color;
    size_t       offset =
      lightshow_without_any_color_lenght + (index * packed_color::size);

    if (offset >= _size)
    {
      return std::nullopt;
    }

    std::copy_n(this->cdata() + offset,
                packed_color::size,
                reinterpret_cast<piwo::net_byte_t*>(color.data()));

    return color;
  }

  size_t
  size() const
  {
    return this->_size;
  }

  net_byte_t*
  end() const
  {
    return this->data() + _size;
  }

  const net_byte_t*
  cend() const
  {
    return this->data() + _size;
  }

  static std::optional<lightshow>
  make_lightshow(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lightshow_buffer_minimum_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::LIGHT_SHOW ||
        packet_buffer_size > lightshow_buffer_maximum_length)
    {
      return std::nullopt;
    }
    auto packet_size = static_cast<size_t>(packet.cdata()[common_length_pos]);

    if (packet_buffer_size < packet_size)
    {
      return std::nullopt;
    }

    return lightshow(packet, packet_size);
  }

private:
  explicit lightshow(raw_packet packet, size_t size) noexcept
    : raw_packet(packet)
    , _size(size)
  {
  }

  const size_t _size;
};

class constant_color_builder
{
public:
  friend class constant_color;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[constant_color_seq_pos] = net_byte_t{ seq };
  }

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.begin()),
                uid::length,
                this->_raw_packet.begin() + constant_color_uid_pos);
  }

  void
  set_color(const packed_color& color) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(color.cdata()),
                packed_color::size,
                this->_raw_packet.begin() + constant_color_color_pos);
  }

  static std::optional<constant_color_builder>
  make_constant_color_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < constant_color_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ constant_color_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::CONST_COLOR) };

    return constant_color_builder(packet);
  }

private:
  explicit constant_color_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class constant_color
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit constant_color(constant_color_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[constant_color_seq_pos]);
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  constant_color_uid_pos) };
  }

  packed_color
  get_color()
  {
    packed_color color;
    std::copy_n(this->cdata() + constant_color_color_pos,
                packed_color::size,
                reinterpret_cast<piwo::net_byte_t*>(color.data()));

    return color;
  }

  size_t
  size() const
  {
    return constant_color_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + constant_color_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + constant_color_length;
  }

  static std::optional<constant_color>
  make_constant_color(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < constant_color_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          constant_color_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::CONST_COLOR)
    {
      return std::nullopt;
    }

    return constant_color(packet);
  }

private:
  explicit constant_color(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class constant_color_la_builder
{
public:
  friend class constant_color_la;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[constant_color_la_seq_pos] = net_byte_t{ seq };
  }

  void
  set_la(const uint8_t la) noexcept
  {
    this->_raw_packet.data()[constant_color_la_la_pos] = net_byte_t{ la };
  }

  void
  set_color(const packed_color& color) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(color.cdata()),
                packed_color::size,
                this->_raw_packet.begin() + constant_color_la_color_pos);
  }

  static std::optional<constant_color_la_builder>
  make_constant_color_la_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < constant_color_la_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ constant_color_la_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::CONST_COLOR_LA) };

    return constant_color_la_builder(packet);
  }

private:
  explicit constant_color_la_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class constant_color_la
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit constant_color_la(constant_color_la_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[constant_color_la_seq_pos]);
  }

  uint8_t
  get_la() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[constant_color_la_la_pos]);
  }

  packed_color
  get_color()
  {
    packed_color color;
    std::copy_n(this->cdata() + constant_color_la_color_pos,
                packed_color::size,
                reinterpret_cast<piwo::net_byte_t*>(color.data()));

    return color;
  }

  size_t
  size() const
  {
    return constant_color_la_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + constant_color_la_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + constant_color_la_length;
  }

  static std::optional<constant_color_la>
  make_constant_color_la(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < constant_color_la_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          constant_color_la_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::CONST_COLOR_LA)
    {
      return std::nullopt;
    }

    return constant_color_la(packet);
  }

private:
  explicit constant_color_la(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class assign_panid_builder
{
public:
  friend class assign_panid;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[assign_panid_seq_pos] = net_byte_t{ seq };
  }

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.begin()),
                uid::length,
                this->_raw_packet.begin() + assign_panid_uid_pos);
  }

  void
  set_panid(const uint16_t panid) noexcept
  {
    uint16_t littleEndian = htole(panid);
    std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&littleEndian),
                sizeof(panid),
                this->_raw_packet.data() + assign_panid_panid_pos);
  }

  static std::optional<assign_panid_builder>
  make_assign_panid_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < assign_panid_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ assign_panid_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::ASSIGN_PANID) };

    return assign_panid_builder(packet);
  }

private:
  explicit assign_panid_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class assign_panid
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit assign_panid(assign_panid_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[assign_panid_seq_pos]);
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  assign_panid_uid_pos) };
  }

  uint16_t
  get_panid() const noexcept
  {
    uint16_t panid = 0;
    std::copy_n(reinterpret_cast<piwo::net_byte_t*>(this->data() +
                                                    assign_panid_panid_pos),
                sizeof(panid),
                reinterpret_cast<piwo::net_byte_t*>(&panid));
    return letoh(panid);
  }

  size_t
  size() const
  {
    return assign_panid_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + assign_panid_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + assign_panid_length;
  }

  static std::optional<assign_panid>
  make_assign_panid(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < assign_panid_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          assign_panid_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::ASSIGN_PANID)
    {
      return std::nullopt;
    }

    return assign_panid(packet);
  }

private:
  explicit assign_panid(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class assign_channel_builder
{
public:
  friend class assign_channel;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[assign_channel_seq_pos] = net_byte_t{ seq };
  }

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.begin()),
                uid::length,
                this->_raw_packet.begin() + assign_channel_uid_pos);
  }

  void
  set_channel(const uint8_t channel) noexcept
  {
    this->_raw_packet.data()[assign_channel_channel_pos] =
      net_byte_t{ channel };
  }

  static std::optional<assign_channel_builder>
  make_assign_channel_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < assign_channel_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ assign_channel_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::ASSIGN_CHANNEL) };

    return assign_channel_builder(packet);
  }

private:
  explicit assign_channel_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class assign_channel
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit assign_channel(assign_channel_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[assign_channel_seq_pos]);
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  assign_channel_uid_pos) };
  }

  uint8_t
  get_channel() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[assign_channel_channel_pos]);
  }

  size_t
  size() const
  {
    return assign_channel_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + assign_channel_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + assign_channel_length;
  }

  static std::optional<assign_channel>
  make_assign_channel(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < assign_channel_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          assign_channel_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::ASSIGN_CHANNEL)
    {
      return std::nullopt;
    }

    return assign_channel(packet);
  }

private:
  explicit assign_channel(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class new_palette_builder
{
public:
  friend class new_palette;

  void
  set_palette_index(const uint8_t index) noexcept
  {
    this->_raw_packet.data()[new_palette_palette_index_pos] =
      net_byte_t{ index };
  }

  void
  set_palette_size(const uint16_t size) noexcept
  {
    uint16_t littleEndian = htole(size);
    std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&littleEndian),
                sizeof(size),
                this->_raw_packet.data() + new_palette_palette_size_pos);
  }

  static std::optional<new_palette_builder>
  make_new_palette_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < new_palette_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ new_palette_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::NEW_PALETTE) };

    return new_palette_builder(packet);
  }

private:
  explicit new_palette_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class new_palette
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit new_palette(new_palette_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  uint8_t
  get_palette_index() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[new_palette_palette_index_pos]);
  }

  uint16_t
  get_palette_size() const noexcept
  {
    uint16_t palette_size = 0;
    std::copy_n(reinterpret_cast<piwo::net_byte_t*>(
                  this->data() + new_palette_palette_size_pos),
                sizeof(palette_size),
                reinterpret_cast<piwo::net_byte_t*>(&palette_size));

    return letoh(palette_size);
  }

  size_t
  size() const
  {
    return new_palette_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + new_palette_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + new_palette_length;
  }

  static std::optional<new_palette>
  make_new_palette(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < new_palette_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          new_palette_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::NEW_PALETTE)
    {
      return std::nullopt;
    }

    return new_palette(packet);
  }

private:
  explicit new_palette(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class fill_palette_builder
{
public:
  friend class fill_palette;

  void
  set_palette_index(const uint8_t index) noexcept
  {
    this->_raw_packet.data()[new_palette_palette_index_pos] =
      net_byte_t{ index };
  }

  void
  set_palette_offset(const uint16_t offset) noexcept
  {
    uint16_t littleEndian = htole(offset);
    std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&littleEndian),
                sizeof(offset),
                this->_raw_packet.data() + new_palette_palette_size_pos);
  }

  bool
  append_data(piwo::net_byte_t* data, size_t size)
  {
    size_t new_package_size = _current_packet_size + size;
    if (new_package_size > this->_raw_packet.size())
    {
      return false;
    }

    std::copy(
      data, data + size, this->_raw_packet.data() + _current_packet_size);

    _current_packet_size = new_package_size;
    return true;
  }

  static std::optional<fill_palette_builder>
  make_fill_palette_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lightshow_buffer_minimum_length)
    {
      return std::nullopt;
    }

    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::FILL_PALETTE) };

    return fill_palette_builder(packet);
  }

private:
  explicit fill_palette_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }
  uint8_t    _current_packet_size = fill_palette_without_any_data_length;
  raw_packet _raw_packet;
};

class fill_palette
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit fill_palette(fill_palette_builder builder)
    : raw_packet(builder._raw_packet)
    , _size(builder._current_packet_size)
  {
    this->data()[common_length_pos] = net_byte_t{ static_cast<uint8_t>(_size) };
  }

  uint8_t
  get_palette_index() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[fill_palette_palette_index_pos]);
  }

  uint16_t
  get_palette_offset() const noexcept
  {
    uint16_t palette_offset = 0;
    std::copy_n(reinterpret_cast<piwo::net_byte_t*>(
                  this->data() + fill_palette_palette_offset_pos),
                sizeof(palette_offset),
                reinterpret_cast<piwo::net_byte_t*>(&palette_offset));

    return letoh(palette_offset);
  }

  std::span<const piwo::net_byte_t>
  get_payload()
  {
    size_t data_length = this->_size - fill_palette_without_any_data_length;
    return std::span<const piwo::net_byte_t>(
      this->data() + fill_palette_palette_data_pos, data_length);
  }

  size_t
  size() const
  {
    return _size;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + _size;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + _size;
  }

  static std::optional<fill_palette>
  make_fill_palette(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < fill_palette_buffer_minimum_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::FILL_PALETTE)
    {
      return std::nullopt;
    }

    auto packet_size = static_cast<size_t>(packet.cdata()[common_length_pos]);
    return fill_palette(packet, packet_size);
  }

private:
  explicit fill_palette(raw_packet packet, size_t size) noexcept
    : raw_packet(packet)
    , _size(size)
  {
  }

  const size_t _size;
};

class tx_set_radio_state_builder
{
public:
  friend class tx_set_radio_state;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[tx_set_radio_state_seq_pos] = net_byte_t{ seq };
  }

  void
  set_state(const tx_radio_state state) noexcept
  {
    this->_raw_packet.data()[tx_set_radio_state_state_pos] =
      net_byte_t{ underlay_cast(state) };
  }

  static std::optional<tx_set_radio_state_builder>
  make_tx_set_radio_state_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < tx_set_radio_state_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ tx_set_radio_state_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::TX_SET_RADIO_STATE) };

    return tx_set_radio_state_builder(packet);
  }

private:
  explicit tx_set_radio_state_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class tx_set_radio_state
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit tx_set_radio_state(tx_set_radio_state_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  size_t
  size() const
  {
    return tx_set_radio_state_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + tx_set_radio_state_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + tx_set_radio_state_length;
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[tx_set_radio_state_seq_pos]);
  }

  tx_radio_state
  get_state() const noexcept
  {
    return tx_radio_state{ static_cast<uint8_t>(
      this->cdata()[tx_set_radio_state_state_pos]) };
  }

  static std::optional<tx_set_radio_state>
  make_tx_set_radio_state(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < tx_set_radio_state_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          tx_set_radio_state_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::TX_SET_RADIO_STATE)
    {
      return std::nullopt;
    }
    return tx_set_radio_state(packet);
  }

private:
  explicit tx_set_radio_state(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class tx_assign_channel_builder
{
public:
  friend class tx_assign_channel;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[tx_assign_channel_seq_pos] = net_byte_t{ seq };
  }

  void
  set_channel(const uint8_t channel) noexcept
  {
    this->_raw_packet.data()[tx_assign_channel_channel_pos] =
      net_byte_t{ channel };
  }

  static std::optional<tx_assign_channel_builder>
  make_tx_assign_channel_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < tx_assign_channel_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ tx_assign_channel_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::TX_ASSIGN_CHANNEL) };

    return tx_assign_channel_builder(packet);
  }

private:
  explicit tx_assign_channel_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class tx_assign_channel
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit tx_assign_channel(tx_assign_channel_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  size_t
  size() const
  {
    return tx_assign_channel_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + tx_assign_channel_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + tx_assign_channel_length;
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[tx_assign_channel_seq_pos]);
  }

  uint8_t
  get_channel() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[tx_assign_channel_channel_pos]);
  }

  static std::optional<tx_assign_channel>
  make_tx_assign_channel(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < tx_assign_channel_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          tx_assign_channel_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::TX_ASSIGN_CHANNEL)
    {
      return std::nullopt;
    }
    return tx_assign_channel(packet);
  }

private:
  explicit tx_assign_channel(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class tx_assign_panid_builder
{
public:
  friend class tx_assign_panid;

  void
  set_seq(const uint8_t seq) noexcept
  {
    this->_raw_packet.data()[tx_assign_panid_seq_pos] = net_byte_t{ seq };
  }

  void
  set_uid(const uid& uid) noexcept
  {
    std::copy_n(reinterpret_cast<const piwo::net_byte_t*>(uid.data.begin()),
                uid::length,
                this->_raw_packet.begin() + tx_assign_panid_uid_pos);
  }

  void
  set_panid(const uint16_t panid) noexcept
  {
    uint16_t littleEndian = htole(panid);
    std::copy_n(reinterpret_cast<piwo::net_byte_t*>(&littleEndian),
                sizeof(panid),
                this->_raw_packet.data() + tx_assign_panid_panid_pos);
  }

  static std::optional<tx_assign_panid_builder>
  make_tx_assign_panid_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < tx_assign_panid_length)
    {
      return std::nullopt;
    }

    packet.data()[common_length_pos] = net_byte_t{ tx_assign_panid_length };
    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::TX_ASSIGN_PANID) };

    return tx_assign_panid_builder(packet);
  }

private:
  explicit tx_assign_panid_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class tx_assign_panid
  : public raw_packet
  , packet_has_8_bit_size_tag
{
public:
  explicit tx_assign_panid(tx_assign_panid_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  size_t
  size() const
  {
    return tx_assign_panid_length;
  }

  net_byte_t*
  end() const
  {
    return this->cdata() + tx_assign_panid_length;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + tx_assign_panid_length;
  }

  uint8_t
  get_seq() const noexcept
  {
    return static_cast<uint8_t>(this->cdata()[tx_assign_panid_seq_pos]);
  }

  uid
  get_uid() const noexcept
  {
    return uid{ reinterpret_cast<uid::data_type*>(this->cdata() +
                                                  tx_assign_panid_uid_pos) };
  }

  uint16_t
  get_panid() const noexcept
  {
    uint16_t panid = 0;
    std::copy_n(reinterpret_cast<piwo::net_byte_t*>(this->data() +
                                                    tx_assign_panid_panid_pos),
                sizeof(panid),
                reinterpret_cast<piwo::net_byte_t*>(&panid));
    return letoh(panid);
  }

  static std::optional<tx_assign_panid>
  make_tx_assign_panid(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < tx_assign_panid_length ||
        static_cast<uint8_t>(packet.cdata()[common_length_pos]) !=
          tx_assign_panid_length ||
        !packet.get_type().has_value() ||
        packet.get_type() != packet_type::TX_ASSIGN_PANID)
    {
      return std::nullopt;
    }
    return tx_assign_panid(packet);
  }

private:
  explicit tx_assign_panid(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class forward_w_cid_builder
{
public:
  friend class forward_w_cid;

  std::optional<raw_packet>
  get_next_slot()
  {
    if (_current_packet_size + forward_w_cid_cid_size >= _raw_packet.size())
    {
      return std::nullopt;
    }

    return raw_packet(
      this->_raw_packet.data() + _current_packet_size + forward_w_cid_cid_size,
      this->_raw_packet.size() - _current_packet_size - forward_w_cid_cid_size);
  }

  template<redirectable_c T>
  bool
  commit(T packet, uint8_t cid)
  {
    // Compare pointers to check if this packet was obtained via get_next_slot()
    // call
    if (packet.cbegin() != this->_raw_packet.cbegin() + _current_packet_size +
                             forward_w_cid_cid_size)
    {
      return false;
    }

    this->_raw_packet.data()[_current_packet_size] = piwo::net_byte_t{ cid };
    this->_current_packet_size += packet.size() + forward_w_cid_cid_size;

    return true;
  }

  static std::optional<forward_w_cid_builder>
  make_forward_w_cid_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < forward_w_cid_without_any_packet_length)
    {
      return std::nullopt;
    }

    packet.data()[common_type_pos] =
      net_byte_t{ piwo::underlay_cast(packet_type::FORWARD_W_CID) };

    return forward_w_cid_builder(packet);
  }

private:
  explicit forward_w_cid_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
  uint16_t   _current_packet_size = forward_w_cid_without_any_packet_length;
};

class forward_w_cid : public raw_packet
{
public:
  explicit forward_w_cid(forward_w_cid_builder builder)
    : raw_packet(builder._raw_packet)
    , _size(builder._current_packet_size)
  {
    uint16_t little_endian_packet_length =
      htole(static_cast<uint16_t>(this->_size));
    std::copy_n(
      reinterpret_cast<piwo::net_byte_t*>(&little_endian_packet_length),
      sizeof(little_endian_packet_length),
      this->begin() + common_length_pos);
  }

  net_byte_t*
  end() const
  {
    return this->data() + this->_size;
  }

  const net_byte_t*
  cend() const
  {
    return this->cdata() + this->_size;
  }

  size_t
  size() const
  {
    return this->_size;
  }

  std::optional<forward_w_cid_encapsulated_packet_t>
  get_next_packet()
  {
    if (this->_current_position + forward_w_cid_cid_size + common_length_pos >=
        this->_size)
    {
      return std::nullopt;
    }

    auto packet_length = static_cast<uint8_t>(
      this->begin()[this->_current_position + forward_w_cid_cid_size +
                    common_length_pos]);
    auto cid = static_cast<uint8_t>(this->data()[this->_current_position]);

    if (this->_current_position + packet_length >= this->_size)
    {
      return std::nullopt;
    }

    auto frame_address =
      this->data() + this->_current_position + forward_w_cid_cid_size;

    this->_current_position += (packet_length + forward_w_cid_cid_size);

    return forward_w_cid_encapsulated_packet_t{
      .packet = raw_packet(frame_address, packet_length), .cid = cid
    };
  }

  static std::optional<forward_w_cid>
  make_forward_w_cid(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    std::optional packet_type_opt = packet.get_type();
    if (packet_buffer_size < forward_w_cid_without_any_packet_length ||
        !packet_type_opt.has_value() ||
        packet_type_opt != packet_type::FORWARD_W_CID)
    {
      return std::nullopt;
    }
    uint16_t packet_size;
    memcpy(
      &packet_size, packet.cdata() + common_length_pos, sizeof(packet_size));
    htole(packet_size);

    if (packet_buffer_size < packet_size)
    {
      return std::nullopt;
    }

    return forward_w_cid(packet, packet_size);
  }

  void
  reset()
  {
    this->_current_position = forward_w_cid_without_any_packet_length;
  }

private:
  explicit forward_w_cid(raw_packet packet, size_t size) noexcept
    : raw_packet(packet)
    , _size(size)
  {
  }

  size_t       _current_position = forward_w_cid_without_any_packet_length;
  const size_t _size;
};

#define OPTIONAL_RET_VALUE_OR(opt, field, orval)                               \
  if (opt.has_value())                                                         \
    return opt->field();                                                       \
  return orval;

inline std::optional<size_t>
get_wrapped_packet_size(raw_packet rp)
{
  const auto type_opt = rp.get_type();
  if (!type_opt.has_value())
    return std::nullopt;

  switch (*type_opt)
  {
    case packet_type::PING:
      OPTIONAL_RET_VALUE_OR(ping::make_ping(rp), size, std::nullopt);
    case packet_type::ACK:
      OPTIONAL_RET_VALUE_OR(ack::make_ack(rp), size, std::nullopt);
    case packet_type::STATUS_REQ:
      OPTIONAL_RET_VALUE_OR(
        status_request::make_status_request(rp), size, std::nullopt);
    case packet_type::STATUS_REPLY:
      OPTIONAL_RET_VALUE_OR(
        status_reply::make_status_reply(rp), size, std::nullopt);
    case packet_type::ASSIGN_LOGIC_ADDRESS:
      OPTIONAL_RET_VALUE_OR(assign_la::make_assign_la(rp), size, std::nullopt);
    case packet_type::LIGHT_SHOW:
      OPTIONAL_RET_VALUE_OR(lightshow::make_lightshow(rp), size, std::nullopt);
    case packet_type::CONST_COLOR:
      OPTIONAL_RET_VALUE_OR(
        constant_color::make_constant_color(rp), size, std::nullopt);
    case packet_type::CONST_COLOR_LA:
      OPTIONAL_RET_VALUE_OR(
        constant_color_la::make_constant_color_la(rp), size, std::nullopt);
    case packet_type::ASSIGN_PANID:
      OPTIONAL_RET_VALUE_OR(
        assign_panid::make_assign_panid(rp), size, std::nullopt);
    case packet_type::ASSIGN_CHANNEL:
      OPTIONAL_RET_VALUE_OR(
        assign_channel::make_assign_channel(rp), size, std::nullopt);
    case packet_type::NEW_PALETTE:
      OPTIONAL_RET_VALUE_OR(
        new_palette::make_new_palette(rp), size, std::nullopt);
    case packet_type::FILL_PALETTE:
      OPTIONAL_RET_VALUE_OR(
        fill_palette::make_fill_palette(rp), size, std::nullopt);
    case packet_type::LIGHT_SHOW_W_PALETTE:
      return std::nullopt;
    case packet_type::TX_SET_RADIO_STATE:
      OPTIONAL_RET_VALUE_OR(
        tx_set_radio_state::make_tx_set_radio_state(rp), size, std::nullopt);
    case packet_type::TX_ASSIGN_CHANNEL:
      OPTIONAL_RET_VALUE_OR(
        tx_assign_channel::make_tx_assign_channel(rp), size, std::nullopt);
    case packet_type::TX_ASSIGN_PANID:
      OPTIONAL_RET_VALUE_OR(
        tx_assign_panid::make_tx_assign_panid(rp), size, std::nullopt);
    case packet_type::FORWARD_W_CID:
      OPTIONAL_RET_VALUE_OR(
        forward_w_cid::make_forward_w_cid(rp), size, std::nullopt);
    default:
      return std::nullopt;
  }

  return std::nullopt;
}

} // namespace piwo
