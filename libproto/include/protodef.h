#pragma once

#include <algorithm>
#include <array>
#include <charconv>
#include <cstddef>
#include <cstdlib>
#include <functional>
#include <optional>
#include <stdint.h>
#include <string_view>
#include <string>

#include "color.h"

namespace piwo
{

struct uid;

class fast_uid
{
public:
  static constexpr size_t MSB_WIDTH = 32;
  static constexpr size_t LSB_WIDTH = 64;

  fast_uid() = delete;

  fast_uid(const uid& uid);

  fast_uid(const fast_uid& other);
  bool
  operator==(const fast_uid& rhs) const;

  bool
  operator!=(const fast_uid& rhs) const;

  fast_uid&
  operator=(const fast_uid& other);

  fast_uid
  operator&(const fast_uid& other) const;

  fast_uid&
  operator&=(const fast_uid& other);

  fast_uid
  operator|(const fast_uid& other) const;

  fast_uid&
  operator|=(const fast_uid& other);

  fast_uid
  operator<<(const uint32_t shift) const;

  fast_uid&
  operator<<=(const uint32_t shift);

  fast_uid
  operator>>(const uint32_t shift) const;

  fast_uid&
  operator>>=(const uint32_t shift);

  bool
  operator<(const fast_uid& rhs) const;

  bool
  operator<=(const fast_uid& rhs) const;

  bool
  operator>(const fast_uid& rhs) const;

  bool
  operator>=(const fast_uid& rhs) const;

protected:
  fast_uid(uint32_t msb, uint64_t lsb)
    : _lsb(lsb)
    , _msb(msb)
  {
  }

  uint64_t _lsb = 0;
  uint32_t _msb = 0;
};

struct uid
{
  using data_type   = uint8_t;
  using packed_type = fast_uid;

  static constexpr size_t           length        = 12;
  static constexpr std::string_view uid_separator = ":";

  static constexpr size_t UID_ASCII_MAX_LEN = 12 * 3;

  uid() = default;
  uid(data_type* from) { std::copy_n(from, length, data.data()); }

  static std::optional<uid>
  from_ascii(std::string_view sv)
  {
    const char*       begin = sv.data();
    const char* const end   = sv.data() + sv.size();

    std::optional<uid> ret             = uid();
    constexpr size_t   bytes_to_decode = uid::length;
    size_t             bytes_decoded   = 0;

    for (;;)
    {
      uint8_t b8;
      auto    cres = std::from_chars(begin, end, b8, 16);

      if (cres.ec != std::errc())
      {
        ret = std::nullopt;
        break;
      }

      ret->data[bytes_decoded] = b8;

      ++bytes_decoded;

      if (bytes_decoded >= bytes_to_decode)
        break;

      static_assert(uid_separator.size() == 1);
      if (cres.ptr >= end || static_cast<char>(*cres.ptr) != uid_separator[0])
      {
        ret = std::nullopt;
        break;
      }

      begin = cres.ptr + 1;

      if (begin >= end)
      {
        ret = std::nullopt;
        break;
      }
    }

    return ret;
  }

  std::string
  to_ascii() const
  {
      std::string formatted_data;
      formatted_data.resize(UID_ASCII_MAX_LEN - 1); // -1 because we get null terminator for free (that is requirement for .resize())

      const char hex_digits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
      char *out = formatted_data.data();
      for (int i = 0; i < 12; ++i) {
          const auto di = data[i];
          out[0] = hex_digits[(di & 0xf0) >> 4];
          out[1] = hex_digits[(di & 0x0f)];
          out[2] = ':';

          out += 3;
      }
      *(out - 1) = '\0';

      return formatted_data;
  }

  packed_type
  packed() const
  {
    return fast_uid(*this);
  }

  // TODO(all): change type to u128?
  std::array<data_type, length> data{};
};

inline fast_uid::fast_uid(const fast_uid& other)
{
  this->_msb = other._msb;
  this->_lsb = other._lsb;
}

inline fast_uid&
fast_uid::operator=(const fast_uid& other)
{
  this->_msb = other._msb;
  this->_lsb = other._lsb;

  return *this;
}

inline fast_uid
fast_uid::operator&(const fast_uid& other) const
{
  return fast_uid(this->_msb & other._msb, this->_lsb & other._lsb);
}

inline fast_uid&
fast_uid::operator&=(const fast_uid& other)
{
  this->_msb &= other._msb;
  this->_lsb &= other._lsb;
  return *this;
}

inline fast_uid
fast_uid::operator|(const fast_uid& other) const
{
  return fast_uid(this->_msb | other._msb, this->_lsb | other._lsb);
}

inline fast_uid&
fast_uid::operator|=(const fast_uid& other)
{
  this->_msb |= other._msb;
  this->_lsb |= other._lsb;
  return *this;
}

inline fast_uid
fast_uid::operator<<(const uint32_t shift) const
{
  static_assert(std::is_same<decltype(this->_lsb), uint64_t>::value,
                "Unsupported LSB data type");

  auto new_msb = this->_msb << shift;
  new_msb |= (this->_lsb >> (this->LSB_WIDTH - shift));

  return fast_uid(new_msb, this->_lsb << shift);
}

inline fast_uid&
fast_uid::operator<<=(const uint32_t shift)
{
  static_assert(std::is_same<decltype(this->_lsb), uint64_t>::value,
                "Unsupported LSB data type");

  this->_msb <<= shift;
  this->_msb |= (this->_lsb >> (this->LSB_WIDTH - shift));

  this->_lsb <<= shift;

  return *this;
}

inline fast_uid
fast_uid::operator>>(const uint32_t shift) const
{
  static_assert(std::is_same<decltype(this->_lsb), uint64_t>::value,
                "Unsupported LSB data type");

  uint64_t wider_msb = this->_msb;

  auto new_lsb = this->_lsb >> shift;
  new_lsb |= wider_msb << (this->LSB_WIDTH - shift);

  return fast_uid(this->_msb >> shift, new_lsb);
}

inline fast_uid&
fast_uid::operator>>=(const uint32_t shift)
{
  static_assert(std::is_same<decltype(this->_lsb), uint64_t>::value,
                "Unsupported LSB data type");

  uint64_t wider_msb = this->_msb;

  this->_lsb >>= shift;
  this->_lsb |= wider_msb << (this->LSB_WIDTH - shift);

  this->_msb >>= shift;

  return *this;
}

inline bool
fast_uid::operator<(const fast_uid& rhs) const
{
  if (this->_msb > rhs._msb)
  {
    return false;
  }

  if (this->_lsb >= rhs._lsb && this->_msb == rhs._msb)
  {
    return false;
  }

  return true;
}

inline bool
fast_uid::operator<=(const fast_uid& rhs) const
{
  if (this->_msb > rhs._msb)
  {
    return false;
  }

  if (this->_lsb > rhs._lsb && this->_msb == rhs._msb)
  {
    return false;
  }

  return true;
}

inline bool
fast_uid::operator>(const fast_uid& rhs) const
{
  if (this->_msb < rhs._msb)
  {
    return false;
  }

  if (this->_lsb <= rhs._lsb && this->_msb == rhs._msb)
  {
    return false;
  }

  return true;
}

inline bool
fast_uid::operator>=(const fast_uid& rhs) const
{
  if (this->_msb < rhs._msb)
  {
    return false;
  }

  if (this->_lsb < rhs._lsb && this->_msb == rhs._msb)
  {
    return false;
  }

  return true;
}

inline bool
fast_uid::operator==(const fast_uid& rhs) const
{
  if (this->_msb == rhs._msb && rhs._lsb == this->_lsb)
  {
    return true;
  }
  return false;
}

inline bool
fast_uid::operator!=(const fast_uid& rhs) const
{
  if (this->_msb != rhs._msb || rhs._lsb != this->_lsb)
  {
    return true;
  }
  return false;
}

inline fast_uid::fast_uid(const uid& uid)
  : _lsb(0)
  , _msb(0)
{
  static_assert(std::is_same<uid::data_type, uint8_t>::value,
                "Unsupported UID data type");
  static_assert(uid::length == sizeof(uint32_t) + sizeof(uint64_t),
                "Unsupported UID size");

  constexpr size_t byte_width = 8;

  size_t i = 0;

  for (;;)
  {
    this->_msb |= uid.data.at(i);

    ++i;

    if (i == sizeof(uint32_t))
      break;

    this->_msb <<= byte_width;
  }

  for (;;)
  {
    this->_lsb |= uid.data.at(i);

    ++i;

    if (i == uid.data.size())
      break;

    this->_lsb <<= byte_width;
  }
}

using net_byte_t      = std::byte;
using logic_address_t = uint8_t;
using gateway_id_t    = uint8_t;
using radio_id_t      = uint8_t;

struct packet_has_8_bit_size_tag
{
};

// Common
constexpr size_t common_length_pos = 1;
constexpr size_t common_type_pos   = 0;

// Forward
constexpr uint8_t forward_w_cid_without_any_packet_length = 3;
constexpr size_t  forward_w_cid_cid_size                  = 1;
constexpr size_t  forward_w_cid_length_size               = 2;

// Ping
constexpr uint8_t ping_length = 3 + uid::length;

constexpr size_t ping_seq_pos = 2;
constexpr size_t ping_uid_pos = 3;

// Blink
constexpr uint8_t blink_length  = 2 + uid::length;
constexpr size_t  blink_uid_pos = 2;

// Ack
constexpr size_t ack_length = 4 + uid::length;

constexpr size_t ack_seq_pos       = 2;
constexpr size_t ack_uid_pos       = 3;
constexpr size_t ack_packet_id_pos = 3 + uid::length;

// Status request
constexpr size_t status_req_length = 3 + uid::length;

constexpr size_t status_req_seq_pos = 2;
constexpr size_t status_req_uid_pos = 3;

// Status reply
constexpr size_t status_reply_length = 8 + uid::length;

constexpr size_t status_reply_seq_pos           = 2;
constexpr size_t status_reply_uid_pos           = 3;
constexpr size_t status_reply_logic_address_pos = 3 + uid::length;
constexpr size_t status_reply_rssi_pos          = 4 + uid::length;
constexpr size_t status_reply_uptime_h_pos      = 5 + uid::length;
constexpr size_t status_reply_uptime_m_pos      = 6 + uid::length;
constexpr size_t status_reply_uptime_s_pos      = 7 + uid::length;

// Assign logic address
constexpr size_t assign_la_length = 4 + uid::length;

constexpr size_t assign_la_seq_pos           = 2;
constexpr size_t assign_la_uid_pos           = 3;
constexpr size_t assign_la_logic_address_pos = 3 + uid::length;

// Light show
constexpr size_t lightshow_buffer_minimum_length = 7;
// the size depends on the radio addressing configuration
// for 16-bit address configuration we can use 119
// paylod bytes, for 64-bit addressing we can use 113 payload bytes
// for more information visit:
// https://www.iith.ac.in/~tbr/teaching/docs/802.15.4-2003.pdf
// section 7.2.1 and
// https://ww1.microchip.com/downloads/en/DeviceDoc/39776C.pdf
// current radio implementation uses 16 bits addressing mode
// so we need only two bytes for PANID and two bytes
// for destination address.
constexpr size_t  lightshow_buffer_maximum_length    = 119;
constexpr uint8_t lightshow_first_color_offset       = 4;
constexpr uint8_t lightshow_without_any_color_lenght = 4;

constexpr size_t lightshow_ttf_pos      = 2;
constexpr size_t lightshow_first_la_pos = 3;

// Constant color
constexpr size_t constant_color_length = 3 + packed_color::size + uid::length;

constexpr size_t constant_color_seq_pos   = 2;
constexpr size_t constant_color_uid_pos   = 3;
constexpr size_t constant_color_color_pos = 3 + uid::length;

// Constant color la
constexpr size_t constant_color_la_length = 4 + packed_color::size;

constexpr size_t constant_color_la_seq_pos   = 2;
constexpr size_t constant_color_la_la_pos    = 3;
constexpr size_t constant_color_la_color_pos = 4;

// Assign panid
constexpr size_t assign_panid_length = 5 + uid::length;

constexpr size_t assign_panid_seq_pos   = 2;
constexpr size_t assign_panid_uid_pos   = 3;
constexpr size_t assign_panid_panid_pos = 3 + uid::length;

// Assign channel
constexpr size_t assign_channel_length = 4 + uid::length;

constexpr size_t assign_channel_seq_pos     = 2;
constexpr size_t assign_channel_uid_pos     = 3;
constexpr size_t assign_channel_channel_pos = 3 + uid::length;

// New palette
constexpr size_t new_palette_length = 5;

constexpr size_t new_palette_palette_index_pos = 2;
constexpr size_t new_palette_palette_size_pos  = 3;

// Fill palette
constexpr size_t fill_palette_buffer_minimum_length   = 6;
constexpr size_t fill_palette_without_any_data_length = 5;

constexpr size_t fill_palette_palette_index_pos  = 2;
constexpr size_t fill_palette_palette_offset_pos = 3;
constexpr size_t fill_palette_palette_data_pos   = 5;

// TX Set radio state
constexpr size_t tx_set_radio_state_length = 4;

constexpr size_t tx_set_radio_state_seq_pos   = 2;
constexpr size_t tx_set_radio_state_state_pos = 3;

// TX Set assign channel
constexpr size_t tx_assign_channel_length = 4;

constexpr size_t tx_assign_channel_seq_pos     = 2;
constexpr size_t tx_assign_channel_channel_pos = 3;

// TX Set assign panid
constexpr size_t tx_assign_panid_length = 5 + uid::length;

constexpr size_t tx_assign_panid_seq_pos   = 2;
constexpr size_t tx_assign_panid_uid_pos   = 3;
constexpr size_t tx_assign_panid_panid_pos = 3 + uid::length;

enum class tx_radio_state : uint8_t
{
  DISABLED = 0,
  ENABLED  = 1,
};

enum class packet_type : uint8_t
{
  // tcp packets
  PING                 = 0x0,
  ACK                  = 0x1,
  STATUS_REQ           = 0x2,
  STATUS_REPLY         = 0x3,
  ASSIGN_LOGIC_ADDRESS = 0x4,
  BLINK                = 0x5,
  CONST_COLOR          = 0x6,
  CONST_COLOR_LA       = 0x7,
  ASSIGN_PANID         = 0x8,
  ASSIGN_CHANNEL       = 0x9,
  NEW_PALETTE          = 0x0A,
  FILL_PALETTE         = 0x0B,
  LIGHT_SHOW_W_PALETTE = 0x0C,
  TX_SET_RADIO_STATE   = 0x0D,
  TX_ASSIGN_CHANNEL    = 0x0E,
  TX_ASSIGN_PANID      = 0x0F,
  FORWARD_W_CID        = 0x10,
  // udp packets
  LIGHT_SHOW = 0x40,
};
} // namespace piwo
