#pragma once

#include <algorithm>
#include <cstdint>

namespace piwo::lp
{
constexpr const char* fifo_name  = "lpd_fifo";
constexpr const char* shmem_name = "lpd_display_buffer";

using pipe_byte_t  = std::byte;
using lp_data_type = uint32_t;

enum class lp_packet_error : uint8_t
{
  BAD_PACKET,
  MORE_DATA,
};

enum class lp_packet_type : uint8_t
{

  // Usually reject or accept request alleged from previously received command.
  // Usage is context dependent.
  REJECT = 0x0,
  ACCEPT = 0x1,

  // Gets ownership of the pipe, used to execute synchronized flow.
  // Some of the commands needs synchronization of both of the parties. Party
  // that owns the pipe is called the owner. There can be only one owner at a
  // time. Ownership of the pipe has to be negotiated and the process for it is
  // described later, below. When the ownership is granted, the owner can
  // assume that no other commands (the ones that require ownership as well as
  // those that does not) have been written after the ownership was granted.
  //
  // Note: It means that if owner of the pipe has read the command from the
  // other party, this command must have been written **before** the ownership
  // was granted. Which also means that LPD should wait for negotitaion to end
  // before writing any commands that assume the ownership has been granted.
  // This is not required for Player, becuase owenership will always be
  // granted, regardless of LPD response.
  //
  // # Synchronized flow
  // Synchronized flow is the sequence of commands that are sent by both
  // parties after the ownership is granted to one of them. Each synchronized
  // flow defines when (after what sequence of commands) it ends. After the
  // synchronized flow ends none of the parties are owners.
  //
  // # Ownership negotiation process
  // Negotiation process begins by one of the parties sending GET_OWNERSHIP
  // command and when negotiation is finished, one of the parties becomes the
  // owner. Ownership is revoked when the last command of the synchronized flow
  // is sent. The exact command that results in that is context dependent and
  // is described in the documentation of the synchronized flow. When one of
  // the parties read GET_OWNERSHIP from the pipe it should respond with ACCEPT
  // command which ends the negotiation and thus make the requesting party an
  // owner. If both parties issued GET_OWNERSHIP command "at the same time"
  // none of them should send ACCEPT in the response. The negotiation in this
  // case ends immediately and the player becomes the owner. LPD may try to
  // become the owner again in the future, when just began synchronized flow
  // ends, but it is not required to do so.
  //
  // ## Ownership negotiation examples
  // Example #1:
  //     *** No pipe owner ***
  //     ... Sequence of commands that does not result in ownership negotiation.
  //     *** No pipe owner ***
  //     LPD   : Writes GET_OWNERSHIP
  //     Player: Reads GET_OWNERSHIP
  //     Player: Writes ACCEPT
  //     *** LPD becomes the pipe owner ***
  //     LPD   : Reads ACCEPT and begins synchronized flow
  //     LPD   : Writes sequence of commands
  //     LPD   : Writes command that ends the synchronized flow
  //     *** No pipe owner ***
  //
  // Example #2:
  //     *** No pipe owner ***
  //     ... Sequence of commands that does not result in ownership negotiation.
  //     *** No pipe owner ***
  //     Player: Writes GET_OWNERSHIP
  //     LPD   : Writes GET_OWNERSHIP
  //     *** Player becomes the pipe owner ***
  //     Player: Reads GET_OWNERSHIP
  //     LPD   : Reads GET_OWNERSHIP
  //     *** LPD issued the GET_OWNERSHIP, but knows that Player did too,
  //         so it must back off ***
  //     *** Player becomes the pipe owner ***
  //     Player: Writes sequence of commands
  //     Player: Writes command that ends the synchronized flow
  //     *** No pipe owner ***
  //     *** LPD may now try again to become the owner,
  //         but is not required to ***
  //
  GET_OWNERSHIP = 0x2,

  // Sent by LPD. Is used to inform Player that the previous shared memory is
  // no longer up-to-date and it should reopen it. No response is sent from the
  // other party.
  RELOAD_SHM = 0x3,

  // ! Part of the synchronized flow.
  // Sent by both parties when they want to resize the grid of the display. The
  // other party responds to this command with either ACCEPT or REJECT. The
  // respond ends synchronized flow.
  RESIZE = 0x4,
};

constexpr size_t lp_common_type_pos = 0x0;

constexpr size_t lp_reject_length        = 0x1;
constexpr size_t lp_accept_length        = 0x1;
constexpr size_t lp_get_ownership_length = 0x1;
constexpr size_t lp_reload_shm_length    = 0x1;
constexpr size_t lp_resize_length        = 0x9;

constexpr size_t lp_resize_width_pos  = 0x1;
constexpr size_t lp_resize_height_pos = 0x5;
constexpr size_t lp_resize_field_size = sizeof(uint32_t);

constexpr size_t lp_max_packet_size = std::max({
  lp_accept_length,
  lp_reject_length,
  lp_get_ownership_length,
  lp_reload_shm_length,
  lp_resize_length,
});

} // namespace piwo
