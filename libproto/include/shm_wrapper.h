#pragma once

#include <cassert>
#include <mipc/base.h>
#include <optional>

#include "color.h"
#include "lp_protodef.h"

namespace piwo
{
class lp_scene
{

  lp_scene(lp_data_type* scene, size_t rows, size_t cols, bool mem_owned)
    : _rows(rows)
    , _cols(cols)
  {
    if (mem_owned)
    {
      _scene = new (scene) color[_rows * _cols];
    }
    else
    {
      _scene = reinterpret_cast<piwo::color*>(scene);
    }
    _mem_owned = mem_owned;
  }

public:
  lp_scene() = default;

  lp_scene&
  operator=(const lp_scene&) = delete;

  lp_scene&
  operator=(lp_scene&& s)
  {
    _rows      = s._rows;
    _cols      = s._cols;
    _scene     = s._scene;
    _mem_owned = s._mem_owned;

    s._rows      = 0;
    s._cols      = 0;
    s._mem_owned = false;
    s._scene     = nullptr;
    return *this;
  }

  static lp_scene
  make_with_ownership(lp_data_type* scene, size_t rows, size_t cols)
  {
    return lp_scene(scene, rows, cols, true);
  }

  static lp_scene
  make_view(lp_data_type* scene, size_t rows, size_t cols)
  {
    return lp_scene(scene, rows, cols, false);
  }

  std::optional<color>
  get_pixel(size_t row, size_t col)
  {
    if (!(row < _rows and col < _cols))
    {
      return std::nullopt;
    }
    return _scene[row * _rows + col];
  }
  size_t
  get_rows()
  {
    return _rows;
  }
  size_t
  get_cols()
  {
    return _cols;
  }

  ~lp_scene()
  {
    if (!_mem_owned || _scene == nullptr)
    {
      return;
    }
#ifdef LIBPROTO_TEST
    assert(!destroyed);
    destroyed = true;
#endif
    for (size_t p = 0; p < _cols * _rows; p++)
    {
      _scene[p].~color();
    }
  }

private:
  size_t _rows = 0, _cols = 0;
  color* _scene     = nullptr;
  bool   _mem_owned = false;

#ifdef LIBPROTO_TEST
  bool destroyed = false;
#endif
};

} // namespace piwo
