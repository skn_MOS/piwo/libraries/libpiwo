#pragma once

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <span>

#include "compiler.h"
#include "lp_protodef.h"

#include <expected>

namespace piwo::lp
{

class raw_packet
{
public:
  explicit raw_packet(pipe_byte_t* packet, const size_t& size)
    : _packet(packet)
    , _size(size)
  {
  }

  [[nodiscard]] size_t
  size() const
  {
    return _size;
  }

  [[nodiscard]] pipe_byte_t*
  data() const
  {
    return _packet;
  }

  [[nodiscard]] pipe_byte_t*
  begin() const
  {
    return _packet;
  }

  [[nodiscard]] pipe_byte_t*
  end() const
  {
    return _packet + _size;
  }

  [[nodiscard]] pipe_byte_t*
  cdata() const
  {
    return _packet;
  }

  [[nodiscard]] const pipe_byte_t*
  cbegin() const
  {
    return _packet;
  }

  [[nodiscard]] const pipe_byte_t*
  cend() const
  {
    return _packet + _size;
  }

  std::expected<lp_packet_type, lp_packet_error>
  get_type() const noexcept
  {
    switch (static_cast<lp_packet_type>(_packet[lp_common_type_pos]))
    {
      case lp_packet_type::REJECT:
      case lp_packet_type::ACCEPT:
      case lp_packet_type::GET_OWNERSHIP:
      case lp_packet_type::RELOAD_SHM:
      case lp_packet_type::RESIZE:
        return static_cast<lp_packet_type>(_packet[lp_common_type_pos]);

      default:
        return std::unexpected(lp_packet_error::BAD_PACKET);
        break;
    }

    return std::unexpected(lp_packet_error::BAD_PACKET);
  }

private:
  pipe_byte_t* _packet;
  size_t       _size;
};

class lp_accept_builder
{
public:
  friend class lp_accept;

  static std::expected<lp_accept_builder, lp_packet_error>
  make_lp_accept_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_accept_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    packet.data()[lp_common_type_pos] =
      pipe_byte_t{ piwo::underlay_cast(lp_packet_type::ACCEPT) };

    return lp_accept_builder(packet);
  }

private:
  explicit lp_accept_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class lp_accept : public raw_packet
{
public:
  explicit lp_accept(lp_accept_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  [[nodiscard]] size_t
  size() const
  {
    return lp_accept_length;
  }

  [[nodiscard]] pipe_byte_t*
  end() const
  {
    return this->cdata() + lp_accept_length;
  }

  [[nodiscard]] const pipe_byte_t*
  cend() const
  {
    return this->cdata() + lp_accept_length;
  }

  static std::expected<lp_accept, lp_packet_error>
  make_lp_accept(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_accept_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    if (!packet.get_type().has_value() ||
        packet.get_type() != lp_packet_type::ACCEPT)
    {
      return std::unexpected(lp_packet_error::BAD_PACKET);
    }

    return lp_accept(packet);
  }

private:
  explicit lp_accept(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class lp_reject_builder
{
public:
  friend class lp_reject;

  static std::expected<lp_reject_builder, lp_packet_error>
  make_lp_reject_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_reject_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    packet.data()[lp_common_type_pos] =
      pipe_byte_t{ piwo::underlay_cast(lp_packet_type::REJECT) };

    return lp_reject_builder(packet);
  }

private:
  explicit lp_reject_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class lp_reject : public raw_packet
{
public:
  explicit lp_reject(lp_reject_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  [[nodiscard]] size_t
  size() const
  {
    return lp_reject_length;
  }

  [[nodiscard]] pipe_byte_t*
  end() const
  {
    return this->cdata() + lp_reject_length;
  }

  [[nodiscard]] const pipe_byte_t*
  cend() const
  {
    return this->cdata() + lp_reject_length;
  }

  static std::expected<lp_reject, lp_packet_error>
  make_lp_reject(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_reject_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    if (!packet.get_type().has_value() ||
        packet.get_type() != lp_packet_type::REJECT)
    {
      return std::unexpected(lp_packet_error::BAD_PACKET);
    }

    return lp_reject(packet);
  }

private:
  explicit lp_reject(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class lp_get_ownership_builder
{
public:
  friend class lp_get_ownership;

  static std::expected<lp_get_ownership_builder, lp_packet_error>
  make_lp_get_ownership_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_get_ownership_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    packet.data()[lp_common_type_pos] =
      pipe_byte_t{ piwo::underlay_cast(lp_packet_type::GET_OWNERSHIP) };

    return lp_get_ownership_builder(packet);
  }

private:
  explicit lp_get_ownership_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class lp_get_ownership : public raw_packet
{
public:
  explicit lp_get_ownership(lp_get_ownership_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  [[nodiscard]] size_t
  size() const
  {
    return lp_get_ownership_length;
  }

  [[nodiscard]] pipe_byte_t*
  end() const
  {
    return this->cdata() + lp_get_ownership_length;
  }

  [[nodiscard]] const pipe_byte_t*
  cend() const
  {
    return this->cdata() + lp_get_ownership_length;
  }

  static std::expected<lp_get_ownership, lp_packet_error>
  make_lp_get_ownership(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_get_ownership_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    if (!packet.get_type().has_value() ||
        packet.get_type() != lp_packet_type::GET_OWNERSHIP)
    {
      return std::unexpected(lp_packet_error::BAD_PACKET);
    }

    return lp_get_ownership(packet);
  }

private:
  explicit lp_get_ownership(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class lp_reload_shm_builder
{
public:
  friend class lp_reload_shm;

  static std::expected<lp_reload_shm_builder, lp_packet_error>
  make_lp_reload_shm_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_reload_shm_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    packet.data()[lp_common_type_pos] =
      pipe_byte_t{ piwo::underlay_cast(lp_packet_type::RELOAD_SHM) };

    return lp_reload_shm_builder(packet);
  }

private:
  explicit lp_reload_shm_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class lp_reload_shm : public raw_packet
{
public:
  explicit lp_reload_shm(lp_reload_shm_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  [[nodiscard]] size_t
  size() const
  {
    return lp_reload_shm_length;
  }

  [[nodiscard]] pipe_byte_t*
  end() const
  {
    return this->cdata() + lp_reload_shm_length;
  }

  [[nodiscard]] const pipe_byte_t*
  cend() const
  {
    return this->cdata() + lp_reload_shm_length;
  }

  static std::expected<lp_reload_shm, lp_packet_error>
  make_lp_reload_shm(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_reload_shm_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    if (!packet.get_type().has_value() ||
        packet.get_type() != lp_packet_type::RELOAD_SHM)
    {
      return std::unexpected(lp_packet_error::BAD_PACKET);
    }

    return lp_reload_shm(packet);
  }

private:
  explicit lp_reload_shm(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

class lp_resize_builder
{
public:
  friend class lp_resize;

  void
  set_width(uint32_t width)
  {
    std::memcpy(this->_raw_packet.data() + lp_resize_width_pos,
                &width,
                lp_resize_field_size);
  }

  void
  set_height(uint32_t height)
  {
    std::memcpy(this->_raw_packet.data() + lp_resize_height_pos,
                &height,
                lp_resize_field_size);
  }

  static std::expected<lp_resize_builder, lp_packet_error>
  make_lp_resize_builder(raw_packet packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_resize_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    packet.data()[lp_common_type_pos] =
      pipe_byte_t{ piwo::underlay_cast(lp_packet_type::RESIZE) };

    return lp_resize_builder(packet);
  }

private:
  explicit lp_resize_builder(raw_packet packet) noexcept
    : _raw_packet(packet)
  {
  }

  raw_packet _raw_packet;
};

class lp_resize : public raw_packet
{
public:
  explicit lp_resize(lp_resize_builder builder)
    : raw_packet(builder._raw_packet)
  {
  }

  [[nodiscard]] size_t
  size() const
  {
    return lp_resize_length;
  }

  [[nodiscard]] pipe_byte_t*
  end() const
  {
    return this->cdata() + lp_resize_length;
  }

  [[nodiscard]] const pipe_byte_t*
  cend() const
  {
    return this->cdata() + lp_resize_length;
  }

  [[nodiscard]] uint32_t
  get_width() const
  {
    uint32_t width{};

    std::memcpy(
      &width, this->cdata() + lp_resize_width_pos, lp_resize_field_size);

    return width;
  }

  [[nodiscard]] uint32_t
  get_height() const
  {
    uint32_t height{};

    std::memcpy(
      &height, this->cdata() + lp_resize_height_pos, lp_resize_field_size);

    return height;
  }

  static std::expected<lp_resize, lp_packet_error>
  make_lp_resize(raw_packet& packet)
  {
    auto packet_buffer_size = static_cast<size_t>(packet.size());

    if (packet_buffer_size < lp_resize_length)
    {
      return std::unexpected(lp_packet_error::MORE_DATA);
    }

    if (!packet.get_type().has_value() ||
        packet.get_type() != lp_packet_type::RESIZE)
    {
      return std::unexpected(lp_packet_error::BAD_PACKET);
    }

    return lp_resize(packet);
  }

private:
  explicit lp_resize(raw_packet packet) noexcept
    : raw_packet(packet)
  {
  }
};

} // namespace piwo
